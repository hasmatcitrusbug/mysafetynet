
$('#testimonials_team_slider').owlCarousel({
    loop: false,
    autoplay:false,
    autoplayHoverPause:false,
    margin:20,
    dots:false,
    nav: true,
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="db-btn__icon db-btn__icon--after nc-icon nc-tail-right left_icon custom_icon"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="db-btn__icon db-btn__icon--after nc-icon nc-tail-right right_icon custom_icon"></i></span>'],
    responsiveClass: true,
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1
        },
        1000:{
            items:2,
        }
    }
});

$(document).ready(function(){

    $("#toggle-read").click(function() {
        var elem = $("#toggle-read").text();
        if (elem == "Read More") {
        $("#toggle-read").text("Read Less");
        $("#text_hide_show").show();
        } else {
        $("#toggle-read").text("Read More");
        $("#text_hide_show").hide();
        }
    });

    $(window).on('load',function(){
        $('#resources-modal').modal('show');
    });

    

});