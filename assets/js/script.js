


! function (t, e) {
	"use strict";
	"object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
		if (!t.document) throw new Error("jQuery requires a window with a document");
		return e(t)
	} : e(t)
}("undefined" != typeof window ? window : this, function (k, t) {
	"use strict";
	var e = [],
		E = k.document,
		o = Object.getPrototypeOf,
		a = e.slice,
		m = e.concat,
		l = e.push,
		n = e.indexOf,
		i = {},
		s = i.toString,
		g = i.hasOwnProperty,
		r = g.toString,
		c = r.call(Object),
		v = {},
		b = function (t) {
			return "function" == typeof t && "number" != typeof t.nodeType
		},
		y = function (t) {
			return null != t && t === t.window
		},
		d = {
			type: !0,
			src: !0,
			noModule: !0
		};

	function w(t, e, i) {
		var o, n = (e = e || E).createElement("script");
		if (n.text = t, i)
			for (o in d) i[o] && (n[o] = i[o]);
		e.head.appendChild(n).parentNode.removeChild(n)
	}

	function x(t) {
		return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? i[s.call(t)] || "object" : typeof t
	}
	var _ = function (t, e) {
			return new _.fn.init(t, e)
		},
		u = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

	function p(t) {
		var e = !!t && "length" in t && t.length,
			i = x(t);
		return !b(t) && !y(t) && ("array" === i || 0 === e || "number" == typeof e && 0 < e && e - 1 in t)
	}
	_.fn = _.prototype = {
		jquery: "3.3.1",
		constructor: _,
		length: 0,
		toArray: function () {
			return a.call(this)
		},
		get: function (t) {
			return null == t ? a.call(this) : t < 0 ? this[t + this.length] : this[t]
		},
		pushStack: function (t) {
			var e = _.merge(this.constructor(), t);
			return e.prevObject = this, e
		},
		each: function (t) {
			return _.each(this, t)
		},
		map: function (i) {
			return this.pushStack(_.map(this, function (t, e) {
				return i.call(t, e, t)
			}))
		},
		slice: function () {
			return this.pushStack(a.apply(this, arguments))
		},
		first: function () {
			return this.eq(0)
		},
		last: function () {
			return this.eq(-1)
		},
		eq: function (t) {
			var e = this.length,
				i = +t + (t < 0 ? e : 0);
			return this.pushStack(0 <= i && i < e ? [this[i]] : [])
		},
		end: function () {
			return this.prevObject || this.constructor()
		},
		push: l,
		sort: e.sort,
		splice: e.splice
	}, _.extend = _.fn.extend = function () {
		var t, e, i, o, n, s, r = arguments[0] || {},
			a = 1,
			l = arguments.length,
			c = !1;
		for ("boolean" == typeof r && (c = r, r = arguments[a] || {}, a++), "object" == typeof r || b(r) || (r = {}), a === l && (r = this, a--); a < l; a++)
			if (null != (t = arguments[a]))
				for (e in t) i = r[e], r !== (o = t[e]) && (c && o && (_.isPlainObject(o) || (n = Array.isArray(o))) ? (n ? (n = !1, s = i && Array.isArray(i) ? i : []) : s = i && _.isPlainObject(i) ? i : {}, r[e] = _.extend(c, s, o)) : void 0 !== o && (r[e] = o));
		return r
	}, _.extend({
		expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
		isReady: !0,
		error: function (t) {
			throw new Error(t)
		},
		noop: function () {},
		isPlainObject: function (t) {
			var e, i;
			return !(!t || "[object Object]" !== s.call(t) || (e = o(t)) && ("function" != typeof (i = g.call(e, "constructor") && e.constructor) || r.call(i) !== c))
		},
		isEmptyObject: function (t) {
			var e;
			for (e in t) return !1;
			return !0
		},
		globalEval: function (t) {
			w(t)
		},
		each: function (t, e) {
			var i, o = 0;
			if (p(t))
				for (i = t.length; o < i && !1 !== e.call(t[o], o, t[o]); o++);
			else
				for (o in t)
					if (!1 === e.call(t[o], o, t[o])) break;
			return t
		},
		trim: function (t) {
			return null == t ? "" : (t + "").replace(u, "")
		},
		makeArray: function (t, e) {
			var i = e || [];
			return null != t && (p(Object(t)) ? _.merge(i, "string" == typeof t ? [t] : t) : l.call(i, t)), i
		},
		inArray: function (t, e, i) {
			return null == e ? -1 : n.call(e, t, i)
		},
		merge: function (t, e) {
			for (var i = +e.length, o = 0, n = t.length; o < i; o++) t[n++] = e[o];
			return t.length = n, t
		},
		grep: function (t, e, i) {
			for (var o = [], n = 0, s = t.length, r = !i; n < s; n++) !e(t[n], n) !== r && o.push(t[n]);
			return o
		},
		map: function (t, e, i) {
			var o, n, s = 0,
				r = [];
			if (p(t))
				for (o = t.length; s < o; s++) null != (n = e(t[s], s, i)) && r.push(n);
			else
				for (s in t) null != (n = e(t[s], s, i)) && r.push(n);
			return m.apply([], r)
		},
		guid: 1,
		support: v
	}), "function" == typeof Symbol && (_.fn[Symbol.iterator] = e[Symbol.iterator]), _.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
		i["[object " + e + "]"] = e.toLowerCase()
	});
	var h = function (i) {
		var t, h, w, s, n, f, u, m, x, l, c, T, k, r, E, g, a, d, v, _ = "sizzle" + 1 * new Date,
			b = i.document,
			C = 0,
			o = 0,
			p = rt(),
			y = rt(),
			S = rt(),
			A = function (t, e) {
				return t === e && (c = !0), 0
			},
			$ = {}.hasOwnProperty,
			e = [],
			D = e.pop,
			I = e.push,
			O = e.push,
			N = e.slice,
			L = function (t, e) {
				for (var i = 0, o = t.length; i < o; i++)
					if (t[i] === e) return i;
				return -1
			},
			j = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
			P = "[\\x20\\t\\r\\n\\f]",
			H = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
			M = "\\[" + P + "*(" + H + ")(?:" + P + "*([*^$|!~]?=)" + P + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + H + "))|)" + P + "*\\]",
			z = ":(" + H + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
			R = new RegExp(P + "+", "g"),
			q = new RegExp("^" + P + "+|((?:^|[^\\\\])(?:\\\\.)*)" + P + "+$", "g"),
			B = new RegExp("^" + P + "*," + P + "*"),
			W = new RegExp("^" + P + "*([>+~]|" + P + ")" + P + "*"),
			F = new RegExp("=" + P + "*([^\\]'\"]*?)" + P + "*\\]", "g"),
			U = new RegExp(z),
			V = new RegExp("^" + H + "$"),
			Q = {
				ID: new RegExp("^#(" + H + ")"),
				CLASS: new RegExp("^\\.(" + H + ")"),
				TAG: new RegExp("^(" + H + "|[*])"),
				ATTR: new RegExp("^" + M),
				PSEUDO: new RegExp("^" + z),
				CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + P + "*(even|odd|(([+-]|)(\\d*)n|)" + P + "*(?:([+-]|)" + P + "*(\\d+)|))" + P + "*\\)|)", "i"),
				bool: new RegExp("^(?:" + j + ")$", "i"),
				needsContext: new RegExp("^" + P + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + P + "*((?:-\\d)?\\d*)" + P + "*\\)|)(?=[^-]|$)", "i")
			},
			Y = /^(?:input|select|textarea|button)$/i,
			G = /^h\d$/i,
			K = /^[^{]+\{\s*\[native \w/,
			X = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
			J = /[+~]/,
			Z = new RegExp("\\\\([\\da-f]{1,6}" + P + "?|(" + P + ")|.)", "ig"),
			tt = function (t, e, i) {
				var o = "0x" + e - 65536;
				return o != o || i ? e : o < 0 ? String.fromCharCode(o + 65536) : String.fromCharCode(o >> 10 | 55296, 1023 & o | 56320)
			},
			et = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
			it = function (t, e) {
				return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t
			},
			ot = function () {
				T()
			},
			nt = bt(function (t) {
				return !0 === t.disabled && ("form" in t || "label" in t)
			}, {
				dir: "parentNode",
				next: "legend"
			});
		try {
			O.apply(e = N.call(b.childNodes), b.childNodes), e[b.childNodes.length].nodeType
		} catch (i) {
			O = {
				apply: e.length ? function (t, e) {
					I.apply(t, N.call(e))
				} : function (t, e) {
					for (var i = t.length, o = 0; t[i++] = e[o++];);
					t.length = i - 1
				}
			}
		}

		function st(t, e, i, o) {
			var n, s, r, a, l, c, d, u = e && e.ownerDocument,
				p = e ? e.nodeType : 9;
			if (i = i || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return i;
			if (!o && ((e ? e.ownerDocument || e : b) !== k && T(e), e = e || k, E)) {
				if (11 !== p && (l = X.exec(t)))
					if (n = l[1]) {
						if (9 === p) {
							if (!(r = e.getElementById(n))) return i;
							if (r.id === n) return i.push(r), i
						} else if (u && (r = u.getElementById(n)) && v(e, r) && r.id === n) return i.push(r), i
					} else {
						if (l[2]) return O.apply(i, e.getElementsByTagName(t)), i;
						if ((n = l[3]) && h.getElementsByClassName && e.getElementsByClassName) return O.apply(i, e.getElementsByClassName(n)), i
					}
				if (h.qsa && !S[t + " "] && (!g || !g.test(t))) {
					if (1 !== p) u = e, d = t;
					else if ("object" !== e.nodeName.toLowerCase()) {
						for ((a = e.getAttribute("id")) ? a = a.replace(et, it) : e.setAttribute("id", a = _), s = (c = f(t)).length; s--;) c[s] = "#" + a + " " + vt(c[s]);
						d = c.join(","), u = J.test(t) && mt(e.parentNode) || e
					}
					if (d) try {
						return O.apply(i, u.querySelectorAll(d)), i
					} catch (t) {} finally {
						a === _ && e.removeAttribute("id")
					}
				}
			}
			return m(t.replace(q, "$1"), e, i, o)
		}

		function rt() {
			var o = [];
			return function t(e, i) {
				return o.push(e + " ") > w.cacheLength && delete t[o.shift()], t[e + " "] = i
			}
		}

		function at(t) {
			return t[_] = !0, t
		}

		function lt(t) {
			var e = k.createElement("fieldset");
			try {
				return !!t(e)
			} catch (t) {
				return !1
			} finally {
				e.parentNode && e.parentNode.removeChild(e), e = null
			}
		}

		function ct(t, e) {
			for (var i = t.split("|"), o = i.length; o--;) w.attrHandle[i[o]] = e
		}

		function dt(t, e) {
			var i = e && t,
				o = i && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
			if (o) return o;
			if (i)
				for (; i = i.nextSibling;)
					if (i === e) return -1;
			return t ? 1 : -1
		}

		function ut(e) {
			return function (t) {
				return "input" === t.nodeName.toLowerCase() && t.type === e
			}
		}

		function pt(i) {
			return function (t) {
				var e = t.nodeName.toLowerCase();
				return ("input" === e || "button" === e) && t.type === i
			}
		}

		function ht(e) {
			return function (t) {
				return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && nt(t) === e : t.disabled === e : "label" in t && t.disabled === e
			}
		}

		function ft(r) {
			return at(function (s) {
				return s = +s, at(function (t, e) {
					for (var i, o = r([], t.length, s), n = o.length; n--;) t[i = o[n]] && (t[i] = !(e[i] = t[i]))
				})
			})
		}

		function mt(t) {
			return t && void 0 !== t.getElementsByTagName && t
		}
		for (t in h = st.support = {}, n = st.isXML = function (t) {
				var e = t && (t.ownerDocument || t).documentElement;
				return !!e && "HTML" !== e.nodeName
			}, T = st.setDocument = function (t) {
				var e, i, o = t ? t.ownerDocument || t : b;
				return o !== k && 9 === o.nodeType && o.documentElement && (r = (k = o).documentElement, E = !n(k), b !== k && (i = k.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", ot, !1) : i.attachEvent && i.attachEvent("onunload", ot)), h.attributes = lt(function (t) {
					return t.className = "i", !t.getAttribute("className")
				}), h.getElementsByTagName = lt(function (t) {
					return t.appendChild(k.createComment("")), !t.getElementsByTagName("*").length
				}), h.getElementsByClassName = K.test(k.getElementsByClassName), h.getById = lt(function (t) {
					return r.appendChild(t).id = _, !k.getElementsByName || !k.getElementsByName(_).length
				}), h.getById ? (w.filter.ID = function (t) {
					var e = t.replace(Z, tt);
					return function (t) {
						return t.getAttribute("id") === e
					}
				}, w.find.ID = function (t, e) {
					if (void 0 !== e.getElementById && E) {
						var i = e.getElementById(t);
						return i ? [i] : []
					}
				}) : (w.filter.ID = function (t) {
					var i = t.replace(Z, tt);
					return function (t) {
						var e = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
						return e && e.value === i
					}
				}, w.find.ID = function (t, e) {
					if (void 0 !== e.getElementById && E) {
						var i, o, n, s = e.getElementById(t);
						if (s) {
							if ((i = s.getAttributeNode("id")) && i.value === t) return [s];
							for (n = e.getElementsByName(t), o = 0; s = n[o++];)
								if ((i = s.getAttributeNode("id")) && i.value === t) return [s]
						}
						return []
					}
				}), w.find.TAG = h.getElementsByTagName ? function (t, e) {
					return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : h.qsa ? e.querySelectorAll(t) : void 0
				} : function (t, e) {
					var i, o = [],
						n = 0,
						s = e.getElementsByTagName(t);
					if ("*" === t) {
						for (; i = s[n++];) 1 === i.nodeType && o.push(i);
						return o
					}
					return s
				}, w.find.CLASS = h.getElementsByClassName && function (t, e) {
					if (void 0 !== e.getElementsByClassName && E) return e.getElementsByClassName(t)
				}, a = [], g = [], (h.qsa = K.test(k.querySelectorAll)) && (lt(function (t) {
					r.appendChild(t).innerHTML = "<a id='" + _ + "'></a><select id='" + _ + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && g.push("[*^$]=" + P + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || g.push("\\[" + P + "*(?:value|" + j + ")"), t.querySelectorAll("[id~=" + _ + "-]").length || g.push("~="), t.querySelectorAll(":checked").length || g.push(":checked"), t.querySelectorAll("a#" + _ + "+*").length || g.push(".#.+[+~]")
				}), lt(function (t) {
					t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
					var e = k.createElement("input");
					e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && g.push("name" + P + "*[*^$|!~]?="), 2 !== t.querySelectorAll(":enabled").length && g.push(":enabled", ":disabled"), r.appendChild(t).disabled = !0, 2 !== t.querySelectorAll(":disabled").length && g.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), g.push(",.*:")
				})), (h.matchesSelector = K.test(d = r.matches || r.webkitMatchesSelector || r.mozMatchesSelector || r.oMatchesSelector || r.msMatchesSelector)) && lt(function (t) {
					h.disconnectedMatch = d.call(t, "*"), d.call(t, "[s!='']:x"), a.push("!=", z)
				}), g = g.length && new RegExp(g.join("|")), a = a.length && new RegExp(a.join("|")), e = K.test(r.compareDocumentPosition), v = e || K.test(r.contains) ? function (t, e) {
					var i = 9 === t.nodeType ? t.documentElement : t,
						o = e && e.parentNode;
					return t === o || !(!o || 1 !== o.nodeType || !(i.contains ? i.contains(o) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(o)))
				} : function (t, e) {
					if (e)
						for (; e = e.parentNode;)
							if (e === t) return !0;
					return !1
				}, A = e ? function (t, e) {
					if (t === e) return c = !0, 0;
					var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
					return i || (1 & (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !h.sortDetached && e.compareDocumentPosition(t) === i ? t === k || t.ownerDocument === b && v(b, t) ? -1 : e === k || e.ownerDocument === b && v(b, e) ? 1 : l ? L(l, t) - L(l, e) : 0 : 4 & i ? -1 : 1)
				} : function (t, e) {
					if (t === e) return c = !0, 0;
					var i, o = 0,
						n = t.parentNode,
						s = e.parentNode,
						r = [t],
						a = [e];
					if (!n || !s) return t === k ? -1 : e === k ? 1 : n ? -1 : s ? 1 : l ? L(l, t) - L(l, e) : 0;
					if (n === s) return dt(t, e);
					for (i = t; i = i.parentNode;) r.unshift(i);
					for (i = e; i = i.parentNode;) a.unshift(i);
					for (; r[o] === a[o];) o++;
					return o ? dt(r[o], a[o]) : r[o] === b ? -1 : a[o] === b ? 1 : 0
				}), k
			}, st.matches = function (t, e) {
				return st(t, null, null, e)
			}, st.matchesSelector = function (t, e) {
				if ((t.ownerDocument || t) !== k && T(t), e = e.replace(F, "='$1']"), h.matchesSelector && E && !S[e + " "] && (!a || !a.test(e)) && (!g || !g.test(e))) try {
					var i = d.call(t, e);
					if (i || h.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
				} catch (t) {}
				return 0 < st(e, k, null, [t]).length
			}, st.contains = function (t, e) {
				return (t.ownerDocument || t) !== k && T(t), v(t, e)
			}, st.attr = function (t, e) {
				(t.ownerDocument || t) !== k && T(t);
				var i = w.attrHandle[e.toLowerCase()],
					o = i && $.call(w.attrHandle, e.toLowerCase()) ? i(t, e, !E) : void 0;
				return void 0 !== o ? o : h.attributes || !E ? t.getAttribute(e) : (o = t.getAttributeNode(e)) && o.specified ? o.value : null
			}, st.escape = function (t) {
				return (t + "").replace(et, it)
			}, st.error = function (t) {
				throw new Error("Syntax error, unrecognized expression: " + t)
			}, st.uniqueSort = function (t) {
				var e, i = [],
					o = 0,
					n = 0;
				if (c = !h.detectDuplicates, l = !h.sortStable && t.slice(0), t.sort(A), c) {
					for (; e = t[n++];) e === t[n] && (o = i.push(n));
					for (; o--;) t.splice(i[o], 1)
				}
				return l = null, t
			}, s = st.getText = function (t) {
				var e, i = "",
					o = 0,
					n = t.nodeType;
				if (n) {
					if (1 === n || 9 === n || 11 === n) {
						if ("string" == typeof t.textContent) return t.textContent;
						for (t = t.firstChild; t; t = t.nextSibling) i += s(t)
					} else if (3 === n || 4 === n) return t.nodeValue
				} else
					for (; e = t[o++];) i += s(e);
				return i
			}, (w = st.selectors = {
				cacheLength: 50,
				createPseudo: at,
				match: Q,
				attrHandle: {},
				find: {},
				relative: {
					">": {
						dir: "parentNode",
						first: !0
					},
					" ": {
						dir: "parentNode"
					},
					"+": {
						dir: "previousSibling",
						first: !0
					},
					"~": {
						dir: "previousSibling"
					}
				},
				preFilter: {
					ATTR: function (t) {
						return t[1] = t[1].replace(Z, tt), t[3] = (t[3] || t[4] || t[5] || "").replace(Z, tt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
					},
					CHILD: function (t) {
						return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || st.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && st.error(t[0]), t
					},
					PSEUDO: function (t) {
						var e, i = !t[6] && t[2];
						return Q.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && U.test(i) && (e = f(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
					}
				},
				filter: {
					TAG: function (t) {
						var e = t.replace(Z, tt).toLowerCase();
						return "*" === t ? function () {
							return !0
						} : function (t) {
							return t.nodeName && t.nodeName.toLowerCase() === e
						}
					},
					CLASS: function (t) {
						var e = p[t + " "];
						return e || (e = new RegExp("(^|" + P + ")" + t + "(" + P + "|$)")) && p(t, function (t) {
							return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "")
						})
					},
					ATTR: function (i, o, n) {
						return function (t) {
							var e = st.attr(t, i);
							return null == e ? "!=" === o : !o || (e += "", "=" === o ? e === n : "!=" === o ? e !== n : "^=" === o ? n && 0 === e.indexOf(n) : "*=" === o ? n && -1 < e.indexOf(n) : "$=" === o ? n && e.slice(-n.length) === n : "~=" === o ? -1 < (" " + e.replace(R, " ") + " ").indexOf(n) : "|=" === o && (e === n || e.slice(0, n.length + 1) === n + "-"))
						}
					},
					CHILD: function (f, t, e, m, g) {
						var v = "nth" !== f.slice(0, 3),
							b = "last" !== f.slice(-4),
							y = "of-type" === t;
						return 1 === m && 0 === g ? function (t) {
							return !!t.parentNode
						} : function (t, e, i) {
							var o, n, s, r, a, l, c = v !== b ? "nextSibling" : "previousSibling",
								d = t.parentNode,
								u = y && t.nodeName.toLowerCase(),
								p = !i && !y,
								h = !1;
							if (d) {
								if (v) {
									for (; c;) {
										for (r = t; r = r[c];)
											if (y ? r.nodeName.toLowerCase() === u : 1 === r.nodeType) return !1;
										l = c = "only" === f && !l && "nextSibling"
									}
									return !0
								}
								if (l = [b ? d.firstChild : d.lastChild], b && p) {
									for (h = (a = (o = (n = (s = (r = d)[_] || (r[_] = {}))[r.uniqueID] || (s[r.uniqueID] = {}))[f] || [])[0] === C && o[1]) && o[2], r = a && d.childNodes[a]; r = ++a && r && r[c] || (h = a = 0) || l.pop();)
										if (1 === r.nodeType && ++h && r === t) {
											n[f] = [C, a, h];
											break
										}
								} else if (p && (h = a = (o = (n = (s = (r = t)[_] || (r[_] = {}))[r.uniqueID] || (s[r.uniqueID] = {}))[f] || [])[0] === C && o[1]), !1 === h)
									for (;
										(r = ++a && r && r[c] || (h = a = 0) || l.pop()) && ((y ? r.nodeName.toLowerCase() !== u : 1 !== r.nodeType) || !++h || (p && ((n = (s = r[_] || (r[_] = {}))[r.uniqueID] || (s[r.uniqueID] = {}))[f] = [C, h]), r !== t)););
								return (h -= g) === m || h % m == 0 && 0 <= h / m
							}
						}
					},
					PSEUDO: function (t, s) {
						var e, r = w.pseudos[t] || w.setFilters[t.toLowerCase()] || st.error("unsupported pseudo: " + t);
						return r[_] ? r(s) : 1 < r.length ? (e = [t, t, "", s], w.setFilters.hasOwnProperty(t.toLowerCase()) ? at(function (t, e) {
							for (var i, o = r(t, s), n = o.length; n--;) t[i = L(t, o[n])] = !(e[i] = o[n])
						}) : function (t) {
							return r(t, 0, e)
						}) : r
					}
				},
				pseudos: {
					not: at(function (t) {
						var o = [],
							n = [],
							a = u(t.replace(q, "$1"));
						return a[_] ? at(function (t, e, i, o) {
							for (var n, s = a(t, null, o, []), r = t.length; r--;)(n = s[r]) && (t[r] = !(e[r] = n))
						}) : function (t, e, i) {
							return o[0] = t, a(o, null, i, n), o[0] = null, !n.pop()
						}
					}),
					has: at(function (e) {
						return function (t) {
							return 0 < st(e, t).length
						}
					}),
					contains: at(function (e) {
						return e = e.replace(Z, tt),
							function (t) {
								return -1 < (t.textContent || t.innerText || s(t)).indexOf(e)
							}
					}),
					lang: at(function (i) {
						return V.test(i || "") || st.error("unsupported lang: " + i), i = i.replace(Z, tt).toLowerCase(),
							function (t) {
								var e;
								do {
									if (e = E ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (e = e.toLowerCase()) === i || 0 === e.indexOf(i + "-")
								} while ((t = t.parentNode) && 1 === t.nodeType);
								return !1
							}
					}),
					target: function (t) {
						var e = i.location && i.location.hash;
						return e && e.slice(1) === t.id
					},
					root: function (t) {
						return t === r
					},
					focus: function (t) {
						return t === k.activeElement && (!k.hasFocus || k.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
					},
					enabled: ht(!1),
					disabled: ht(!0),
					checked: function (t) {
						var e = t.nodeName.toLowerCase();
						return "input" === e && !!t.checked || "option" === e && !!t.selected
					},
					selected: function (t) {
						return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
					},
					empty: function (t) {
						for (t = t.firstChild; t; t = t.nextSibling)
							if (t.nodeType < 6) return !1;
						return !0
					},
					parent: function (t) {
						return !w.pseudos.empty(t)
					},
					header: function (t) {
						return G.test(t.nodeName)
					},
					input: function (t) {
						return Y.test(t.nodeName)
					},
					button: function (t) {
						var e = t.nodeName.toLowerCase();
						return "input" === e && "button" === t.type || "button" === e
					},
					text: function (t) {
						var e;
						return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
					},
					first: ft(function () {
						return [0]
					}),
					last: ft(function (t, e) {
						return [e - 1]
					}),
					eq: ft(function (t, e, i) {
						return [i < 0 ? i + e : i]
					}),
					even: ft(function (t, e) {
						for (var i = 0; i < e; i += 2) t.push(i);
						return t
					}),
					odd: ft(function (t, e) {
						for (var i = 1; i < e; i += 2) t.push(i);
						return t
					}),
					lt: ft(function (t, e, i) {
						for (var o = i < 0 ? i + e : i; 0 <= --o;) t.push(o);
						return t
					}),
					gt: ft(function (t, e, i) {
						for (var o = i < 0 ? i + e : i; ++o < e;) t.push(o);
						return t
					})
				}
			}).pseudos.nth = w.pseudos.eq, {
				radio: !0,
				checkbox: !0,
				file: !0,
				password: !0,
				image: !0
			}) w.pseudos[t] = ut(t);
		for (t in {
				submit: !0,
				reset: !0
			}) w.pseudos[t] = pt(t);

		function gt() {}

		function vt(t) {
			for (var e = 0, i = t.length, o = ""; e < i; e++) o += t[e].value;
			return o
		}

		function bt(a, t, e) {
			var l = t.dir,
				c = t.next,
				d = c || l,
				u = e && "parentNode" === d,
				p = o++;
			return t.first ? function (t, e, i) {
				for (; t = t[l];)
					if (1 === t.nodeType || u) return a(t, e, i);
				return !1
			} : function (t, e, i) {
				var o, n, s, r = [C, p];
				if (i) {
					for (; t = t[l];)
						if ((1 === t.nodeType || u) && a(t, e, i)) return !0
				} else
					for (; t = t[l];)
						if (1 === t.nodeType || u)
							if (n = (s = t[_] || (t[_] = {}))[t.uniqueID] || (s[t.uniqueID] = {}), c && c === t.nodeName.toLowerCase()) t = t[l] || t;
							else {
								if ((o = n[d]) && o[0] === C && o[1] === p) return r[2] = o[2];
								if ((n[d] = r)[2] = a(t, e, i)) return !0
							} return !1
			}
		}

		function yt(n) {
			return 1 < n.length ? function (t, e, i) {
				for (var o = n.length; o--;)
					if (!n[o](t, e, i)) return !1;
				return !0
			} : n[0]
		}

		function wt(t, e, i, o, n) {
			for (var s, r = [], a = 0, l = t.length, c = null != e; a < l; a++)(s = t[a]) && (i && !i(s, o, n) || (r.push(s), c && e.push(a)));
			return r
		}

		function xt(h, f, m, g, v, t) {
			return g && !g[_] && (g = xt(g)), v && !v[_] && (v = xt(v, t)), at(function (t, e, i, o) {
				var n, s, r, a = [],
					l = [],
					c = e.length,
					d = t || function (t, e, i) {
						for (var o = 0, n = e.length; o < n; o++) st(t, e[o], i);
						return i
					}(f || "*", i.nodeType ? [i] : i, []),
					u = !h || !t && f ? d : wt(d, a, h, i, o),
					p = m ? v || (t ? h : c || g) ? [] : e : u;
				if (m && m(u, p, i, o), g)
					for (n = wt(p, l), g(n, [], i, o), s = n.length; s--;)(r = n[s]) && (p[l[s]] = !(u[l[s]] = r));
				if (t) {
					if (v || h) {
						if (v) {
							for (n = [], s = p.length; s--;)(r = p[s]) && n.push(u[s] = r);
							v(null, p = [], n, o)
						}
						for (s = p.length; s--;)(r = p[s]) && -1 < (n = v ? L(t, r) : a[s]) && (t[n] = !(e[n] = r))
					}
				} else p = wt(p === e ? p.splice(c, p.length) : p), v ? v(null, e, p, o) : O.apply(e, p)
			})
		}

		function Tt(t) {
			for (var n, e, i, o = t.length, s = w.relative[t[0].type], r = s || w.relative[" "], a = s ? 1 : 0, l = bt(function (t) {
					return t === n
				}, r, !0), c = bt(function (t) {
					return -1 < L(n, t)
				}, r, !0), d = [function (t, e, i) {
					var o = !s && (i || e !== x) || ((n = e).nodeType ? l(t, e, i) : c(t, e, i));
					return n = null, o
				}]; a < o; a++)
				if (e = w.relative[t[a].type]) d = [bt(yt(d), e)];
				else {
					if ((e = w.filter[t[a].type].apply(null, t[a].matches))[_]) {
						for (i = ++a; i < o && !w.relative[t[i].type]; i++);
						return xt(1 < a && yt(d), 1 < a && vt(t.slice(0, a - 1).concat({
							value: " " === t[a - 2].type ? "*" : ""
						})).replace(q, "$1"), e, a < i && Tt(t.slice(a, i)), i < o && Tt(t = t.slice(i)), i < o && vt(t))
					}
					d.push(e)
				}
			return yt(d)
		}
		return gt.prototype = w.filters = w.pseudos, w.setFilters = new gt, f = st.tokenize = function (t, e) {
			var i, o, n, s, r, a, l, c = y[t + " "];
			if (c) return e ? 0 : c.slice(0);
			for (r = t, a = [], l = w.preFilter; r;) {
				for (s in i && !(o = B.exec(r)) || (o && (r = r.slice(o[0].length) || r), a.push(n = [])), i = !1, (o = W.exec(r)) && (i = o.shift(), n.push({
						value: i,
						type: o[0].replace(q, " ")
					}), r = r.slice(i.length)), w.filter) !(o = Q[s].exec(r)) || l[s] && !(o = l[s](o)) || (i = o.shift(), n.push({
					value: i,
					type: s,
					matches: o
				}), r = r.slice(i.length));
				if (!i) break
			}
			return e ? r.length : r ? st.error(t) : y(t, a).slice(0)
		}, u = st.compile = function (t, e) {
			var i, g, v, b, y, o, n = [],
				s = [],
				r = S[t + " "];
			if (!r) {
				for (e || (e = f(t)), i = e.length; i--;)(r = Tt(e[i]))[_] ? n.push(r) : s.push(r);
				(r = S(t, (g = s, v = n, b = 0 < v.length, y = 0 < g.length, o = function (t, e, i, o, n) {
					var s, r, a, l = 0,
						c = "0",
						d = t && [],
						u = [],
						p = x,
						h = t || y && w.find.TAG("*", n),
						f = C += null == p ? 1 : Math.random() || .1,
						m = h.length;
					for (n && (x = e === k || e || n); c !== m && null != (s = h[c]); c++) {
						if (y && s) {
							for (r = 0, e || s.ownerDocument === k || (T(s), i = !E); a = g[r++];)
								if (a(s, e || k, i)) {
									o.push(s);
									break
								}
							n && (C = f)
						}
						b && ((s = !a && s) && l--, t && d.push(s))
					}
					if (l += c, b && c !== l) {
						for (r = 0; a = v[r++];) a(d, u, e, i);
						if (t) {
							if (0 < l)
								for (; c--;) d[c] || u[c] || (u[c] = D.call(o));
							u = wt(u)
						}
						O.apply(o, u), n && !t && 0 < u.length && 1 < l + v.length && st.uniqueSort(o)
					}
					return n && (C = f, x = p), d
				}, b ? at(o) : o))).selector = t
			}
			return r
		}, m = st.select = function (t, e, i, o) {
			var n, s, r, a, l, c = "function" == typeof t && t,
				d = !o && f(t = c.selector || t);
			if (i = i || [], 1 === d.length) {
				if (2 < (s = d[0] = d[0].slice(0)).length && "ID" === (r = s[0]).type && 9 === e.nodeType && E && w.relative[s[1].type]) {
					if (!(e = (w.find.ID(r.matches[0].replace(Z, tt), e) || [])[0])) return i;
					c && (e = e.parentNode), t = t.slice(s.shift().value.length)
				}
				for (n = Q.needsContext.test(t) ? 0 : s.length; n-- && (r = s[n], !w.relative[a = r.type]);)
					if ((l = w.find[a]) && (o = l(r.matches[0].replace(Z, tt), J.test(s[0].type) && mt(e.parentNode) || e))) {
						if (s.splice(n, 1), !(t = o.length && vt(s))) return O.apply(i, o), i;
						break
					}
			}
			return (c || u(t, d))(o, e, !E, i, !e || J.test(t) && mt(e.parentNode) || e), i
		}, h.sortStable = _.split("").sort(A).join("") === _, h.detectDuplicates = !!c, T(), h.sortDetached = lt(function (t) {
			return 1 & t.compareDocumentPosition(k.createElement("fieldset"))
		}), lt(function (t) {
			return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
		}) || ct("type|href|height|width", function (t, e, i) {
			if (!i) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
		}), h.attributes && lt(function (t) {
			return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
		}) || ct("value", function (t, e, i) {
			if (!i && "input" === t.nodeName.toLowerCase()) return t.defaultValue
		}), lt(function (t) {
			return null == t.getAttribute("disabled")
		}) || ct(j, function (t, e, i) {
			var o;
			if (!i) return !0 === t[e] ? e.toLowerCase() : (o = t.getAttributeNode(e)) && o.specified ? o.value : null
		}), st
	}(k);
	_.find = h, _.expr = h.selectors, _.expr[":"] = _.expr.pseudos, _.uniqueSort = _.unique = h.uniqueSort, _.text = h.getText, _.isXMLDoc = h.isXML, _.contains = h.contains, _.escapeSelector = h.escape;
	var f = function (t, e, i) {
			for (var o = [], n = void 0 !== i;
				(t = t[e]) && 9 !== t.nodeType;)
				if (1 === t.nodeType) {
					if (n && _(t).is(i)) break;
					o.push(t)
				}
			return o
		},
		T = function (t, e) {
			for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
			return i
		},
		C = _.expr.match.needsContext;

	function S(t, e) {
		return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
	}
	var A = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

	function $(t, i, o) {
		return b(i) ? _.grep(t, function (t, e) {
			return !!i.call(t, e, t) !== o
		}) : i.nodeType ? _.grep(t, function (t) {
			return t === i !== o
		}) : "string" != typeof i ? _.grep(t, function (t) {
			return -1 < n.call(i, t) !== o
		}) : _.filter(i, t, o)
	}
	_.filter = function (t, e, i) {
		var o = e[0];
		return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === o.nodeType ? _.find.matchesSelector(o, t) ? [o] : [] : _.find.matches(t, _.grep(e, function (t) {
			return 1 === t.nodeType
		}))
	}, _.fn.extend({
		find: function (t) {
			var e, i, o = this.length,
				n = this;
			if ("string" != typeof t) return this.pushStack(_(t).filter(function () {
				for (e = 0; e < o; e++)
					if (_.contains(n[e], this)) return !0
			}));
			for (i = this.pushStack([]), e = 0; e < o; e++) _.find(t, n[e], i);
			return 1 < o ? _.uniqueSort(i) : i
		},
		filter: function (t) {
			return this.pushStack($(this, t || [], !1))
		},
		not: function (t) {
			return this.pushStack($(this, t || [], !0))
		},
		is: function (t) {
			return !!$(this, "string" == typeof t && C.test(t) ? _(t) : t || [], !1).length
		}
	});
	var D, I = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
	(_.fn.init = function (t, e, i) {
		var o, n;
		if (!t) return this;
		if (i = i || D, "string" == typeof t) {
			if (!(o = "<" === t[0] && ">" === t[t.length - 1] && 3 <= t.length ? [null, t, null] : I.exec(t)) || !o[1] && e) return !e || e.jquery ? (e || i).find(t) : this.constructor(e).find(t);
			if (o[1]) {
				if (e = e instanceof _ ? e[0] : e, _.merge(this, _.parseHTML(o[1], e && e.nodeType ? e.ownerDocument || e : E, !0)), A.test(o[1]) && _.isPlainObject(e))
					for (o in e) b(this[o]) ? this[o](e[o]) : this.attr(o, e[o]);
				return this
			}
			return (n = E.getElementById(o[2])) && (this[0] = n, this.length = 1), this
		}
		return t.nodeType ? (this[0] = t, this.length = 1, this) : b(t) ? void 0 !== i.ready ? i.ready(t) : t(_) : _.makeArray(t, this)
	}).prototype = _.fn, D = _(E);
	var O = /^(?:parents|prev(?:Until|All))/,
		N = {
			children: !0,
			contents: !0,
			next: !0,
			prev: !0
		};

	function L(t, e) {
		for (;
			(t = t[e]) && 1 !== t.nodeType;);
		return t
	}
	_.fn.extend({
		has: function (t) {
			var e = _(t, this),
				i = e.length;
			return this.filter(function () {
				for (var t = 0; t < i; t++)
					if (_.contains(this, e[t])) return !0
			})
		},
		closest: function (t, e) {
			var i, o = 0,
				n = this.length,
				s = [],
				r = "string" != typeof t && _(t);
			if (!C.test(t))
				for (; o < n; o++)
					for (i = this[o]; i && i !== e; i = i.parentNode)
						if (i.nodeType < 11 && (r ? -1 < r.index(i) : 1 === i.nodeType && _.find.matchesSelector(i, t))) {
							s.push(i);
							break
						}
			return this.pushStack(1 < s.length ? _.uniqueSort(s) : s)
		},
		index: function (t) {
			return t ? "string" == typeof t ? n.call(_(t), this[0]) : n.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
		},
		add: function (t, e) {
			return this.pushStack(_.uniqueSort(_.merge(this.get(), _(t, e))))
		},
		addBack: function (t) {
			return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
		}
	}), _.each({
		parent: function (t) {
			var e = t.parentNode;
			return e && 11 !== e.nodeType ? e : null
		},
		parents: function (t) {
			return f(t, "parentNode")
		},
		parentsUntil: function (t, e, i) {
			return f(t, "parentNode", i)
		},
		next: function (t) {
			return L(t, "nextSibling")
		},
		prev: function (t) {
			return L(t, "previousSibling")
		},
		nextAll: function (t) {
			return f(t, "nextSibling")
		},
		prevAll: function (t) {
			return f(t, "previousSibling")
		},
		nextUntil: function (t, e, i) {
			return f(t, "nextSibling", i)
		},
		prevUntil: function (t, e, i) {
			return f(t, "previousSibling", i)
		},
		siblings: function (t) {
			return T((t.parentNode || {}).firstChild, t)
		},
		children: function (t) {
			return T(t.firstChild)
		},
		contents: function (t) {
			return S(t, "iframe") ? t.contentDocument : (S(t, "template") && (t = t.content || t), _.merge([], t.childNodes))
		}
	}, function (o, n) {
		_.fn[o] = function (t, e) {
			var i = _.map(this, n, t);
			return "Until" !== o.slice(-5) && (e = t), e && "string" == typeof e && (i = _.filter(e, i)), 1 < this.length && (N[o] || _.uniqueSort(i), O.test(o) && i.reverse()), this.pushStack(i)
		}
	});
	var j = /[^\x20\t\r\n\f]+/g;

	function P(t) {
		return t
	}

	function H(t) {
		throw t
	}

	function M(t, e, i, o) {
		var n;
		try {
			t && b(n = t.promise) ? n.call(t).done(e).fail(i) : t && b(n = t.then) ? n.call(t, e, i) : e.apply(void 0, [t].slice(o))
		} catch (t) {
			i.apply(void 0, [t])
		}
	}
	_.Callbacks = function (o) {
		var t, i;
		o = "string" == typeof o ? (t = o, i = {}, _.each(t.match(j) || [], function (t, e) {
			i[e] = !0
		}), i) : _.extend({}, o);
		var n, e, s, r, a = [],
			l = [],
			c = -1,
			d = function () {
				for (r = r || o.once, s = n = !0; l.length; c = -1)
					for (e = l.shift(); ++c < a.length;) !1 === a[c].apply(e[0], e[1]) && o.stopOnFalse && (c = a.length, e = !1);
				o.memory || (e = !1), n = !1, r && (a = e ? [] : "")
			},
			u = {
				add: function () {
					return a && (e && !n && (c = a.length - 1, l.push(e)), function i(t) {
						_.each(t, function (t, e) {
							b(e) ? o.unique && u.has(e) || a.push(e) : e && e.length && "string" !== x(e) && i(e)
						})
					}(arguments), e && !n && d()), this
				},
				remove: function () {
					return _.each(arguments, function (t, e) {
						for (var i; - 1 < (i = _.inArray(e, a, i));) a.splice(i, 1), i <= c && c--
					}), this
				},
				has: function (t) {
					return t ? -1 < _.inArray(t, a) : 0 < a.length
				},
				empty: function () {
					return a && (a = []), this
				},
				disable: function () {
					return r = l = [], a = e = "", this
				},
				disabled: function () {
					return !a
				},
				lock: function () {
					return r = l = [], e || n || (a = e = ""), this
				},
				locked: function () {
					return !!r
				},
				fireWith: function (t, e) {
					return r || (e = [t, (e = e || []).slice ? e.slice() : e], l.push(e), n || d()), this
				},
				fire: function () {
					return u.fireWith(this, arguments), this
				},
				fired: function () {
					return !!s
				}
			};
		return u
	}, _.extend({
		Deferred: function (t) {
			var s = [
					["notify", "progress", _.Callbacks("memory"), _.Callbacks("memory"), 2],
					["resolve", "done", _.Callbacks("once memory"), _.Callbacks("once memory"), 0, "resolved"],
					["reject", "fail", _.Callbacks("once memory"), _.Callbacks("once memory"), 1, "rejected"]
				],
				n = "pending",
				r = {
					state: function () {
						return n
					},
					always: function () {
						return a.done(arguments).fail(arguments), this
					},
					catch: function (t) {
						return r.then(null, t)
					},
					pipe: function () {
						var n = arguments;
						return _.Deferred(function (o) {
							_.each(s, function (t, e) {
								var i = b(n[e[4]]) && n[e[4]];
								a[e[1]](function () {
									var t = i && i.apply(this, arguments);
									t && b(t.promise) ? t.promise().progress(o.notify).done(o.resolve).fail(o.reject) : o[e[0] + "With"](this, i ? [t] : arguments)
								})
							}), n = null
						}).promise()
					},
					then: function (e, i, o) {
						var l = 0;

						function c(n, s, r, a) {
							return function () {
								var i = this,
									o = arguments,
									t = function () {
										var t, e;
										if (!(n < l)) {
											if ((t = r.apply(i, o)) === s.promise()) throw new TypeError("Thenable self-resolution");
											e = t && ("object" == typeof t || "function" == typeof t) && t.then, b(e) ? a ? e.call(t, c(l, s, P, a), c(l, s, H, a)) : (l++, e.call(t, c(l, s, P, a), c(l, s, H, a), c(l, s, P, s.notifyWith))) : (r !== P && (i = void 0, o = [t]), (a || s.resolveWith)(i, o))
										}
									},
									e = a ? t : function () {
										try {
											t()
										} catch (t) {
											_.Deferred.exceptionHook && _.Deferred.exceptionHook(t, e.stackTrace), l <= n + 1 && (r !== H && (i = void 0, o = [t]), s.rejectWith(i, o))
										}
									};
								n ? e() : (_.Deferred.getStackHook && (e.stackTrace = _.Deferred.getStackHook()), k.setTimeout(e))
							}
						}
						return _.Deferred(function (t) {
							s[0][3].add(c(0, t, b(o) ? o : P, t.notifyWith)), s[1][3].add(c(0, t, b(e) ? e : P)), s[2][3].add(c(0, t, b(i) ? i : H))
						}).promise()
					},
					promise: function (t) {
						return null != t ? _.extend(t, r) : r
					}
				},
				a = {};
			return _.each(s, function (t, e) {
				var i = e[2],
					o = e[5];
				r[e[1]] = i.add, o && i.add(function () {
					n = o
				}, s[3 - t][2].disable, s[3 - t][3].disable, s[0][2].lock, s[0][3].lock), i.add(e[3].fire), a[e[0]] = function () {
					return a[e[0] + "With"](this === a ? void 0 : this, arguments), this
				}, a[e[0] + "With"] = i.fireWith
			}), r.promise(a), t && t.call(a, a), a
		},
		when: function (t) {
			var i = arguments.length,
				e = i,
				o = Array(e),
				n = a.call(arguments),
				s = _.Deferred(),
				r = function (e) {
					return function (t) {
						o[e] = this, n[e] = 1 < arguments.length ? a.call(arguments) : t, --i || s.resolveWith(o, n)
					}
				};
			if (i <= 1 && (M(t, s.done(r(e)).resolve, s.reject, !i), "pending" === s.state() || b(n[e] && n[e].then))) return s.then();
			for (; e--;) M(n[e], r(e), s.reject);
			return s.promise()
		}
	});
	var z = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
	_.Deferred.exceptionHook = function (t, e) {
		k.console && k.console.warn && t && z.test(t.name) && k.console.warn("jQuery.Deferred exception: " + t.message, t.stack, e)
	}, _.readyException = function (t) {
		k.setTimeout(function () {
			throw t
		})
	};
	var R = _.Deferred();

	function q() {
		E.removeEventListener("DOMContentLoaded", q), k.removeEventListener("load", q), _.ready()
	}
	_.fn.ready = function (t) {
		return R.then(t).catch(function (t) {
			_.readyException(t)
		}), this
	}, _.extend({
		isReady: !1,
		readyWait: 1,
		ready: function (t) {
			(!0 === t ? --_.readyWait : _.isReady) || ((_.isReady = !0) !== t && 0 < --_.readyWait || R.resolveWith(E, [_]))
		}
	}), _.ready.then = R.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? k.setTimeout(_.ready) : (E.addEventListener("DOMContentLoaded", q), k.addEventListener("load", q));
	var B = function (t, e, i, o, n, s, r) {
			var a = 0,
				l = t.length,
				c = null == i;
			if ("object" === x(i))
				for (a in n = !0, i) B(t, e, a, i[a], !0, s, r);
			else if (void 0 !== o && (n = !0, b(o) || (r = !0), c && (r ? (e.call(t, o), e = null) : (c = e, e = function (t, e, i) {
					return c.call(_(t), i)
				})), e))
				for (; a < l; a++) e(t[a], i, r ? o : o.call(t[a], a, e(t[a], i)));
			return n ? t : c ? e.call(t) : l ? e(t[0], i) : s
		},
		W = /^-ms-/,
		F = /-([a-z])/g;

	function U(t, e) {
		return e.toUpperCase()
	}

	function V(t) {
		return t.replace(W, "ms-").replace(F, U)
	}
	var Q = function (t) {
		return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
	};

	function Y() {
		this.expando = _.expando + Y.uid++
	}
	Y.uid = 1, Y.prototype = {
		cache: function (t) {
			var e = t[this.expando];
			return e || (e = {}, Q(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
				value: e,
				configurable: !0
			}))), e
		},
		set: function (t, e, i) {
			var o, n = this.cache(t);
			if ("string" == typeof e) n[V(e)] = i;
			else
				for (o in e) n[V(o)] = e[o];
			return n
		},
		get: function (t, e) {
			return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][V(e)]
		},
		access: function (t, e, i) {
			return void 0 === e || e && "string" == typeof e && void 0 === i ? this.get(t, e) : (this.set(t, e, i), void 0 !== i ? i : e)
		},
		remove: function (t, e) {
			var i, o = t[this.expando];
			if (void 0 !== o) {
				if (void 0 !== e) {
					i = (e = Array.isArray(e) ? e.map(V) : (e = V(e)) in o ? [e] : e.match(j) || []).length;
					for (; i--;) delete o[e[i]]
				}(void 0 === e || _.isEmptyObject(o)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
			}
		},
		hasData: function (t) {
			var e = t[this.expando];
			return void 0 !== e && !_.isEmptyObject(e)
		}
	};
	var G = new Y,
		K = new Y,
		X = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
		J = /[A-Z]/g;

	function Z(t, e, i) {
		var o, n;
		if (void 0 === i && 1 === t.nodeType)
			if (o = "data-" + e.replace(J, "-$&").toLowerCase(), "string" == typeof (i = t.getAttribute(o))) {
				try {
					i = "true" === (n = i) || "false" !== n && ("null" === n ? null : n === +n + "" ? +n : X.test(n) ? JSON.parse(n) : n)
				} catch (t) {}
				K.set(t, e, i)
			} else i = void 0;
		return i
	}
	_.extend({
		hasData: function (t) {
			return K.hasData(t) || G.hasData(t)
		},
		data: function (t, e, i) {
			return K.access(t, e, i)
		},
		removeData: function (t, e) {
			K.remove(t, e)
		},
		_data: function (t, e, i) {
			return G.access(t, e, i)
		},
		_removeData: function (t, e) {
			G.remove(t, e)
		}
	}), _.fn.extend({
		data: function (i, t) {
			var e, o, n, s = this[0],
				r = s && s.attributes;
			if (void 0 === i) {
				if (this.length && (n = K.get(s), 1 === s.nodeType && !G.get(s, "hasDataAttrs"))) {
					for (e = r.length; e--;) r[e] && 0 === (o = r[e].name).indexOf("data-") && (o = V(o.slice(5)), Z(s, o, n[o]));
					G.set(s, "hasDataAttrs", !0)
				}
				return n
			}
			return "object" == typeof i ? this.each(function () {
				K.set(this, i)
			}) : B(this, function (t) {
				var e;
				if (s && void 0 === t) {
					if (void 0 !== (e = K.get(s, i))) return e;
					if (void 0 !== (e = Z(s, i))) return e
				} else this.each(function () {
					K.set(this, i, t)
				})
			}, null, t, 1 < arguments.length, null, !0)
		},
		removeData: function (t) {
			return this.each(function () {
				K.remove(this, t)
			})
		}
	}), _.extend({
		queue: function (t, e, i) {
			var o;
			if (t) return e = (e || "fx") + "queue", o = G.get(t, e), i && (!o || Array.isArray(i) ? o = G.access(t, e, _.makeArray(i)) : o.push(i)), o || []
		},
		dequeue: function (t, e) {
			e = e || "fx";
			var i = _.queue(t, e),
				o = i.length,
				n = i.shift(),
				s = _._queueHooks(t, e);
			"inprogress" === n && (n = i.shift(), o--), n && ("fx" === e && i.unshift("inprogress"), delete s.stop, n.call(t, function () {
				_.dequeue(t, e)
			}, s)), !o && s && s.empty.fire()
		},
		_queueHooks: function (t, e) {
			var i = e + "queueHooks";
			return G.get(t, i) || G.access(t, i, {
				empty: _.Callbacks("once memory").add(function () {
					G.remove(t, [e + "queue", i])
				})
			})
		}
	}), _.fn.extend({
		queue: function (e, i) {
			var t = 2;
			return "string" != typeof e && (i = e, e = "fx", t--), arguments.length < t ? _.queue(this[0], e) : void 0 === i ? this : this.each(function () {
				var t = _.queue(this, e, i);
				_._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && _.dequeue(this, e)
			})
		},
		dequeue: function (t) {
			return this.each(function () {
				_.dequeue(this, t)
			})
		},
		clearQueue: function (t) {
			return this.queue(t || "fx", [])
		},
		promise: function (t, e) {
			var i, o = 1,
				n = _.Deferred(),
				s = this,
				r = this.length,
				a = function () {
					--o || n.resolveWith(s, [s])
				};
			for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; r--;)(i = G.get(s[r], t + "queueHooks")) && i.empty && (o++, i.empty.add(a));
			return a(), n.promise(e)
		}
	});
	var tt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
		et = new RegExp("^(?:([+-])=|)(" + tt + ")([a-z%]*)$", "i"),
		it = ["Top", "Right", "Bottom", "Left"],
		ot = function (t, e) {
			return "none" === (t = e || t).style.display || "" === t.style.display && _.contains(t.ownerDocument, t) && "none" === _.css(t, "display")
		},
		nt = function (t, e, i, o) {
			var n, s, r = {};
			for (s in e) r[s] = t.style[s], t.style[s] = e[s];
			for (s in n = i.apply(t, o || []), e) t.style[s] = r[s];
			return n
		};

	function st(t, e, i, o) {
		var n, s, r = 20,
			a = o ? function () {
				return o.cur()
			} : function () {
				return _.css(t, e, "")
			},
			l = a(),
			c = i && i[3] || (_.cssNumber[e] ? "" : "px"),
			d = (_.cssNumber[e] || "px" !== c && +l) && et.exec(_.css(t, e));
		if (d && d[3] !== c) {
			for (l /= 2, c = c || d[3], d = +l || 1; r--;) _.style(t, e, d + c), (1 - s) * (1 - (s = a() / l || .5)) <= 0 && (r = 0), d /= s;
			d *= 2, _.style(t, e, d + c), i = i || []
		}
		return i && (d = +d || +l || 0, n = i[1] ? d + (i[1] + 1) * i[2] : +i[2], o && (o.unit = c, o.start = d, o.end = n)), n
	}
	var rt = {};

	function at(t, e) {
		for (var i, o, n = [], s = 0, r = t.length; s < r; s++)(o = t[s]).style && (i = o.style.display, e ? ("none" === i && (n[s] = G.get(o, "display") || null, n[s] || (o.style.display = "")), "" === o.style.display && ot(o) && (n[s] = (u = c = l = void 0, c = (a = o).ownerDocument, d = a.nodeName, (u = rt[d]) || (l = c.body.appendChild(c.createElement(d)), u = _.css(l, "display"), l.parentNode.removeChild(l), "none" === u && (u = "block"), rt[d] = u)))) : "none" !== i && (n[s] = "none", G.set(o, "display", i)));
		var a, l, c, d, u;
		for (s = 0; s < r; s++) null != n[s] && (t[s].style.display = n[s]);
		return t
	}
	_.fn.extend({
		show: function () {
			return at(this, !0)
		},
		hide: function () {
			return at(this)
		},
		toggle: function (t) {
			return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
				ot(this) ? _(this).show() : _(this).hide()
			})
		}
	});
	var lt = /^(?:checkbox|radio)$/i,
		ct = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
		dt = /^$|^module$|\/(?:java|ecma)script/i,
		ut = {
			option: [1, "<select multiple='multiple'>", "</select>"],
			thead: [1, "<table>", "</table>"],
			col: [2, "<table><colgroup>", "</colgroup></table>"],
			tr: [2, "<table><tbody>", "</tbody></table>"],
			td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
			_default: [0, "", ""]
		};

	function pt(t, e) {
		var i;
		return i = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [], void 0 === e || e && S(t, e) ? _.merge([t], i) : i
	}

	function ht(t, e) {
		for (var i = 0, o = t.length; i < o; i++) G.set(t[i], "globalEval", !e || G.get(e[i], "globalEval"))
	}
	ut.optgroup = ut.option, ut.tbody = ut.tfoot = ut.colgroup = ut.caption = ut.thead, ut.th = ut.td;
	var ft, mt, gt = /<|&#?\w+;/;

	function vt(t, e, i, o, n) {
		for (var s, r, a, l, c, d, u = e.createDocumentFragment(), p = [], h = 0, f = t.length; h < f; h++)
			if ((s = t[h]) || 0 === s)
				if ("object" === x(s)) _.merge(p, s.nodeType ? [s] : s);
				else if (gt.test(s)) {
			for (r = r || u.appendChild(e.createElement("div")), a = (ct.exec(s) || ["", ""])[1].toLowerCase(), l = ut[a] || ut._default, r.innerHTML = l[1] + _.htmlPrefilter(s) + l[2], d = l[0]; d--;) r = r.lastChild;
			_.merge(p, r.childNodes), (r = u.firstChild).textContent = ""
		} else p.push(e.createTextNode(s));
		for (u.textContent = "", h = 0; s = p[h++];)
			if (o && -1 < _.inArray(s, o)) n && n.push(s);
			else if (c = _.contains(s.ownerDocument, s), r = pt(u.appendChild(s), "script"), c && ht(r), i)
			for (d = 0; s = r[d++];) dt.test(s.type || "") && i.push(s);
		return u
	}
	ft = E.createDocumentFragment().appendChild(E.createElement("div")), (mt = E.createElement("input")).setAttribute("type", "radio"), mt.setAttribute("checked", "checked"), mt.setAttribute("name", "t"), ft.appendChild(mt), v.checkClone = ft.cloneNode(!0).cloneNode(!0).lastChild.checked, ft.innerHTML = "<textarea>x</textarea>", v.noCloneChecked = !!ft.cloneNode(!0).lastChild.defaultValue;
	var bt = E.documentElement,
		yt = /^key/,
		wt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		xt = /^([^.]*)(?:\.(.+)|)/;

	function Tt() {
		return !0
	}

	function kt() {
		return !1
	}

	function Et() {
		try {
			return E.activeElement
		} catch (t) {}
	}

	function _t(t, e, i, o, n, s) {
		var r, a;
		if ("object" == typeof e) {
			for (a in "string" != typeof i && (o = o || i, i = void 0), e) _t(t, a, i, o, e[a], s);
			return t
		}
		if (null == o && null == n ? (n = i, o = i = void 0) : null == n && ("string" == typeof i ? (n = o, o = void 0) : (n = o, o = i, i = void 0)), !1 === n) n = kt;
		else if (!n) return t;
		return 1 === s && (r = n, (n = function (t) {
			return _().off(t), r.apply(this, arguments)
		}).guid = r.guid || (r.guid = _.guid++)), t.each(function () {
			_.event.add(this, e, n, o, i)
		})
	}
	_.event = {
		global: {},
		add: function (e, t, i, o, n) {
			var s, r, a, l, c, d, u, p, h, f, m, g = G.get(e);
			if (g)
				for (i.handler && (i = (s = i).handler, n = s.selector), n && _.find.matchesSelector(bt, n), i.guid || (i.guid = _.guid++), (l = g.events) || (l = g.events = {}), (r = g.handle) || (r = g.handle = function (t) {
						return void 0 !== _ && _.event.triggered !== t.type ? _.event.dispatch.apply(e, arguments) : void 0
					}), c = (t = (t || "").match(j) || [""]).length; c--;) h = m = (a = xt.exec(t[c]) || [])[1], f = (a[2] || "").split(".").sort(), h && (u = _.event.special[h] || {}, h = (n ? u.delegateType : u.bindType) || h, u = _.event.special[h] || {}, d = _.extend({
					type: h,
					origType: m,
					data: o,
					handler: i,
					guid: i.guid,
					selector: n,
					needsContext: n && _.expr.match.needsContext.test(n),
					namespace: f.join(".")
				}, s), (p = l[h]) || ((p = l[h] = []).delegateCount = 0, u.setup && !1 !== u.setup.call(e, o, f, r) || e.addEventListener && e.addEventListener(h, r)), u.add && (u.add.call(e, d), d.handler.guid || (d.handler.guid = i.guid)), n ? p.splice(p.delegateCount++, 0, d) : p.push(d), _.event.global[h] = !0)
		},
		remove: function (t, e, i, o, n) {
			var s, r, a, l, c, d, u, p, h, f, m, g = G.hasData(t) && G.get(t);
			if (g && (l = g.events)) {
				for (c = (e = (e || "").match(j) || [""]).length; c--;)
					if (h = m = (a = xt.exec(e[c]) || [])[1], f = (a[2] || "").split(".").sort(), h) {
						for (u = _.event.special[h] || {}, p = l[h = (o ? u.delegateType : u.bindType) || h] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), r = s = p.length; s--;) d = p[s], !n && m !== d.origType || i && i.guid !== d.guid || a && !a.test(d.namespace) || o && o !== d.selector && ("**" !== o || !d.selector) || (p.splice(s, 1), d.selector && p.delegateCount--, u.remove && u.remove.call(t, d));
						r && !p.length && (u.teardown && !1 !== u.teardown.call(t, f, g.handle) || _.removeEvent(t, h, g.handle), delete l[h])
					} else
						for (h in l) _.event.remove(t, h + e[c], i, o, !0);
				_.isEmptyObject(l) && G.remove(t, "handle events")
			}
		},
		dispatch: function (t) {
			var e, i, o, n, s, r, a = _.event.fix(t),
				l = new Array(arguments.length),
				c = (G.get(this, "events") || {})[a.type] || [],
				d = _.event.special[a.type] || {};
			for (l[0] = a, e = 1; e < arguments.length; e++) l[e] = arguments[e];
			if (a.delegateTarget = this, !d.preDispatch || !1 !== d.preDispatch.call(this, a)) {
				for (r = _.event.handlers.call(this, a, c), e = 0;
					(n = r[e++]) && !a.isPropagationStopped();)
					for (a.currentTarget = n.elem, i = 0;
						(s = n.handlers[i++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(s.namespace) || (a.handleObj = s, a.data = s.data, void 0 !== (o = ((_.event.special[s.origType] || {}).handle || s.handler).apply(n.elem, l)) && !1 === (a.result = o) && (a.preventDefault(), a.stopPropagation()));
				return d.postDispatch && d.postDispatch.call(this, a), a.result
			}
		},
		handlers: function (t, e) {
			var i, o, n, s, r, a = [],
				l = e.delegateCount,
				c = t.target;
			if (l && c.nodeType && !("click" === t.type && 1 <= t.button))
				for (; c !== this; c = c.parentNode || this)
					if (1 === c.nodeType && ("click" !== t.type || !0 !== c.disabled)) {
						for (s = [], r = {}, i = 0; i < l; i++) void 0 === r[n = (o = e[i]).selector + " "] && (r[n] = o.needsContext ? -1 < _(n, this).index(c) : _.find(n, this, null, [c]).length), r[n] && s.push(o);
						s.length && a.push({
							elem: c,
							handlers: s
						})
					}
			return c = this, l < e.length && a.push({
				elem: c,
				handlers: e.slice(l)
			}), a
		},
		addProp: function (e, t) {
			Object.defineProperty(_.Event.prototype, e, {
				enumerable: !0,
				configurable: !0,
				get: b(t) ? function () {
					if (this.originalEvent) return t(this.originalEvent)
				} : function () {
					if (this.originalEvent) return this.originalEvent[e]
				},
				set: function (t) {
					Object.defineProperty(this, e, {
						enumerable: !0,
						configurable: !0,
						writable: !0,
						value: t
					})
				}
			})
		},
		fix: function (t) {
			return t[_.expando] ? t : new _.Event(t)
		},
		special: {
			load: {
				noBubble: !0
			},
			focus: {
				trigger: function () {
					if (this !== Et() && this.focus) return this.focus(), !1
				},
				delegateType: "focusin"
			},
			blur: {
				trigger: function () {
					if (this === Et() && this.blur) return this.blur(), !1
				},
				delegateType: "focusout"
			},
			click: {
				trigger: function () {
					if ("checkbox" === this.type && this.click && S(this, "input")) return this.click(), !1
				},
				_default: function (t) {
					return S(t.target, "a")
				}
			},
			beforeunload: {
				postDispatch: function (t) {
					void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
				}
			}
		}
	}, _.removeEvent = function (t, e, i) {
		t.removeEventListener && t.removeEventListener(e, i)
	}, _.Event = function (t, e) {
		if (!(this instanceof _.Event)) return new _.Event(t, e);
		t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? Tt : kt, this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target, this.currentTarget = t.currentTarget, this.relatedTarget = t.relatedTarget) : this.type = t, e && _.extend(this, e), this.timeStamp = t && t.timeStamp || Date.now(), this[_.expando] = !0
	}, _.Event.prototype = {
		constructor: _.Event,
		isDefaultPrevented: kt,
		isPropagationStopped: kt,
		isImmediatePropagationStopped: kt,
		isSimulated: !1,
		preventDefault: function () {
			var t = this.originalEvent;
			this.isDefaultPrevented = Tt, t && !this.isSimulated && t.preventDefault()
		},
		stopPropagation: function () {
			var t = this.originalEvent;
			this.isPropagationStopped = Tt, t && !this.isSimulated && t.stopPropagation()
		},
		stopImmediatePropagation: function () {
			var t = this.originalEvent;
			this.isImmediatePropagationStopped = Tt, t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation()
		}
	}, _.each({
		altKey: !0,
		bubbles: !0,
		cancelable: !0,
		changedTouches: !0,
		ctrlKey: !0,
		detail: !0,
		eventPhase: !0,
		metaKey: !0,
		pageX: !0,
		pageY: !0,
		shiftKey: !0,
		view: !0,
		char: !0,
		charCode: !0,
		key: !0,
		keyCode: !0,
		button: !0,
		buttons: !0,
		clientX: !0,
		clientY: !0,
		offsetX: !0,
		offsetY: !0,
		pointerId: !0,
		pointerType: !0,
		screenX: !0,
		screenY: !0,
		targetTouches: !0,
		toElement: !0,
		touches: !0,
		which: function (t) {
			var e = t.button;
			return null == t.which && yt.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && wt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which
		}
	}, _.event.addProp), _.each({
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function (t, n) {
		_.event.special[t] = {
			delegateType: n,
			bindType: n,
			handle: function (t) {
				var e, i = t.relatedTarget,
					o = t.handleObj;
				return i && (i === this || _.contains(this, i)) || (t.type = o.origType, e = o.handler.apply(this, arguments), t.type = n), e
			}
		}
	}), _.fn.extend({
		on: function (t, e, i, o) {
			return _t(this, t, e, i, o)
		},
		one: function (t, e, i, o) {
			return _t(this, t, e, i, o, 1)
		},
		off: function (t, e, i) {
			var o, n;
			if (t && t.preventDefault && t.handleObj) return o = t.handleObj, _(t.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler), this;
			if ("object" == typeof t) {
				for (n in t) this.off(n, e, t[n]);
				return this
			}
			return !1 !== e && "function" != typeof e || (i = e, e = void 0), !1 === i && (i = kt), this.each(function () {
				_.event.remove(this, t, i, e)
			})
		}
	});
	var Ct = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
		St = /<script|<style|<link/i,
		At = /checked\s*(?:[^=]|=\s*.checked.)/i,
		$t = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

	function Dt(t, e) {
		return S(t, "table") && S(11 !== e.nodeType ? e : e.firstChild, "tr") && _(t).children("tbody")[0] || t
	}

	function It(t) {
		return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t
	}

	function Ot(t) {
		return "true/" === (t.type || "").slice(0, 5) ? t.type = t.type.slice(5) : t.removeAttribute("type"), t
	}

	function Nt(t, e) {
		var i, o, n, s, r, a, l, c;
		if (1 === e.nodeType) {
			if (G.hasData(t) && (s = G.access(t), r = G.set(e, s), c = s.events))
				for (n in delete r.handle, r.events = {}, c)
					for (i = 0, o = c[n].length; i < o; i++) _.event.add(e, n, c[n][i]);
			K.hasData(t) && (a = K.access(t), l = _.extend({}, a), K.set(e, l))
		}
	}

	function Lt(i, o, n, s) {
		o = m.apply([], o);
		var t, e, r, a, l, c, d = 0,
			u = i.length,
			p = u - 1,
			h = o[0],
			f = b(h);
		if (f || 1 < u && "string" == typeof h && !v.checkClone && At.test(h)) return i.each(function (t) {
			var e = i.eq(t);
			f && (o[0] = h.call(this, t, e.html())), Lt(e, o, n, s)
		});
		if (u && (e = (t = vt(o, i[0].ownerDocument, !1, i, s)).firstChild, 1 === t.childNodes.length && (t = e), e || s)) {
			for (a = (r = _.map(pt(t, "script"), It)).length; d < u; d++) l = t, d !== p && (l = _.clone(l, !0, !0), a && _.merge(r, pt(l, "script"))), n.call(i[d], l, d);
			if (a)
				for (c = r[r.length - 1].ownerDocument, _.map(r, Ot), d = 0; d < a; d++) l = r[d], dt.test(l.type || "") && !G.access(l, "globalEval") && _.contains(c, l) && (l.src && "module" !== (l.type || "").toLowerCase() ? _._evalUrl && _._evalUrl(l.src) : w(l.textContent.replace($t, ""), c, l))
		}
		return i
	}

	function jt(t, e, i) {
		for (var o, n = e ? _.filter(e, t) : t, s = 0; null != (o = n[s]); s++) i || 1 !== o.nodeType || _.cleanData(pt(o)), o.parentNode && (i && _.contains(o.ownerDocument, o) && ht(pt(o, "script")), o.parentNode.removeChild(o));
		return t
	}
	_.extend({
		htmlPrefilter: function (t) {
			return t.replace(Ct, "<$1></$2>")
		},
		clone: function (t, e, i) {
			var o, n, s, r, a, l, c, d = t.cloneNode(!0),
				u = _.contains(t.ownerDocument, t);
			if (!(v.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || _.isXMLDoc(t)))
				for (r = pt(d), o = 0, n = (s = pt(t)).length; o < n; o++) a = s[o], l = r[o], void 0, "input" === (c = l.nodeName.toLowerCase()) && lt.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
			if (e)
				if (i)
					for (s = s || pt(t), r = r || pt(d), o = 0, n = s.length; o < n; o++) Nt(s[o], r[o]);
				else Nt(t, d);
			return 0 < (r = pt(d, "script")).length && ht(r, !u && pt(t, "script")), d
		},
		cleanData: function (t) {
			for (var e, i, o, n = _.event.special, s = 0; void 0 !== (i = t[s]); s++)
				if (Q(i)) {
					if (e = i[G.expando]) {
						if (e.events)
							for (o in e.events) n[o] ? _.event.remove(i, o) : _.removeEvent(i, o, e.handle);
						i[G.expando] = void 0
					}
					i[K.expando] && (i[K.expando] = void 0)
				}
		}
	}), _.fn.extend({
		detach: function (t) {
			return jt(this, t, !0)
		},
		remove: function (t) {
			return jt(this, t)
		},
		text: function (t) {
			return B(this, function (t) {
				return void 0 === t ? _.text(this) : this.empty().each(function () {
					1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
				})
			}, null, t, arguments.length)
		},
		append: function () {
			return Lt(this, arguments, function (t) {
				1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Dt(this, t).appendChild(t)
			})
		},
		prepend: function () {
			return Lt(this, arguments, function (t) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var e = Dt(this, t);
					e.insertBefore(t, e.firstChild)
				}
			})
		},
		before: function () {
			return Lt(this, arguments, function (t) {
				this.parentNode && this.parentNode.insertBefore(t, this)
			})
		},
		after: function () {
			return Lt(this, arguments, function (t) {
				this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
			})
		},
		empty: function () {
			for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (_.cleanData(pt(t, !1)), t.textContent = "");
			return this
		},
		clone: function (t, e) {
			return t = null != t && t, e = null == e ? t : e, this.map(function () {
				return _.clone(this, t, e)
			})
		},
		html: function (t) {
			return B(this, function (t) {
				var e = this[0] || {},
					i = 0,
					o = this.length;
				if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
				if ("string" == typeof t && !St.test(t) && !ut[(ct.exec(t) || ["", ""])[1].toLowerCase()]) {
					t = _.htmlPrefilter(t);
					try {
						for (; i < o; i++) 1 === (e = this[i] || {}).nodeType && (_.cleanData(pt(e, !1)), e.innerHTML = t);
						e = 0
					} catch (t) {}
				}
				e && this.empty().append(t)
			}, null, t, arguments.length)
		},
		replaceWith: function () {
			var i = [];
			return Lt(this, arguments, function (t) {
				var e = this.parentNode;
				_.inArray(this, i) < 0 && (_.cleanData(pt(this)), e && e.replaceChild(t, this))
			}, i)
		}
	}), _.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function (t, r) {
		_.fn[t] = function (t) {
			for (var e, i = [], o = _(t), n = o.length - 1, s = 0; s <= n; s++) e = s === n ? this : this.clone(!0), _(o[s])[r](e), l.apply(i, e.get());
			return this.pushStack(i)
		}
	});
	var Pt = new RegExp("^(" + tt + ")(?!px)[a-z%]+$", "i"),
		Ht = function (t) {
			var e = t.ownerDocument.defaultView;
			return e && e.opener || (e = k), e.getComputedStyle(t)
		},
		Mt = new RegExp(it.join("|"), "i");

	function zt(t, e, i) {
		var o, n, s, r, a = t.style;
		return (i = i || Ht(t)) && ("" !== (r = i.getPropertyValue(e) || i[e]) || _.contains(t.ownerDocument, t) || (r = _.style(t, e)), !v.pixelBoxStyles() && Pt.test(r) && Mt.test(e) && (o = a.width, n = a.minWidth, s = a.maxWidth, a.minWidth = a.maxWidth = a.width = r, r = i.width, a.width = o, a.minWidth = n, a.maxWidth = s)), void 0 !== r ? r + "" : r
	}

	function Rt(t, e) {
		return {
			get: function () {
				if (!t()) return (this.get = e).apply(this, arguments);
				delete this.get
			}
		}
	}! function () {
		function t() {
			if (l) {
				a.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", bt.appendChild(a).appendChild(l);
				var t = k.getComputedStyle(l);
				i = "1%" !== t.top, r = 12 === e(t.marginLeft), l.style.right = "60%", s = 36 === e(t.right), o = 36 === e(t.width), l.style.position = "absolute", n = 36 === l.offsetWidth || "absolute", bt.removeChild(a), l = null
			}
		}

		function e(t) {
			return Math.round(parseFloat(t))
		}
		var i, o, n, s, r, a = E.createElement("div"),
			l = E.createElement("div");
		l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", v.clearCloneStyle = "content-box" === l.style.backgroundClip, _.extend(v, {
			boxSizingReliable: function () {
				return t(), o
			},
			pixelBoxStyles: function () {
				return t(), s
			},
			pixelPosition: function () {
				return t(), i
			},
			reliableMarginLeft: function () {
				return t(), r
			},
			scrollboxSize: function () {
				return t(), n
			}
		}))
	}();
	var qt = /^(none|table(?!-c[ea]).+)/,
		Bt = /^--/,
		Wt = {
			position: "absolute",
			visibility: "hidden",
			display: "block"
		},
		Ft = {
			letterSpacing: "0",
			fontWeight: "400"
		},
		Ut = ["Webkit", "Moz", "ms"],
		Vt = E.createElement("div").style;

	function Qt(t) {
		var e = _.cssProps[t];
		return e || (e = _.cssProps[t] = function (t) {
			if (t in Vt) return t;
			for (var e = t[0].toUpperCase() + t.slice(1), i = Ut.length; i--;)
				if ((t = Ut[i] + e) in Vt) return t
		}(t) || t), e
	}

	function Yt(t, e, i) {
		var o = et.exec(e);
		return o ? Math.max(0, o[2] - (i || 0)) + (o[3] || "px") : e
	}

	function Gt(t, e, i, o, n, s) {
		var r = "width" === e ? 1 : 0,
			a = 0,
			l = 0;
		if (i === (o ? "border" : "content")) return 0;
		for (; r < 4; r += 2) "margin" === i && (l += _.css(t, i + it[r], !0, n)), o ? ("content" === i && (l -= _.css(t, "padding" + it[r], !0, n)), "margin" !== i && (l -= _.css(t, "border" + it[r] + "Width", !0, n))) : (l += _.css(t, "padding" + it[r], !0, n), "padding" !== i ? l += _.css(t, "border" + it[r] + "Width", !0, n) : a += _.css(t, "border" + it[r] + "Width", !0, n));
		return !o && 0 <= s && (l += Math.max(0, Math.ceil(t["offset" + e[0].toUpperCase() + e.slice(1)] - s - l - a - .5))), l
	}

	function Kt(t, e, i) {
		var o = Ht(t),
			n = zt(t, e, o),
			s = "border-box" === _.css(t, "boxSizing", !1, o),
			r = s;
		if (Pt.test(n)) {
			if (!i) return n;
			n = "auto"
		}
		return r = r && (v.boxSizingReliable() || n === t.style[e]), ("auto" === n || !parseFloat(n) && "inline" === _.css(t, "display", !1, o)) && (n = t["offset" + e[0].toUpperCase() + e.slice(1)], r = !0), (n = parseFloat(n) || 0) + Gt(t, e, i || (s ? "border" : "content"), r, o, n) + "px"
	}

	function Xt(t, e, i, o, n) {
		return new Xt.prototype.init(t, e, i, o, n)
	}
	_.extend({
		cssHooks: {
			opacity: {
				get: function (t, e) {
					if (e) {
						var i = zt(t, "opacity");
						return "" === i ? "1" : i
					}
				}
			}
		},
		cssNumber: {
			animationIterationCount: !0,
			columnCount: !0,
			fillOpacity: !0,
			flexGrow: !0,
			flexShrink: !0,
			fontWeight: !0,
			lineHeight: !0,
			opacity: !0,
			order: !0,
			orphans: !0,
			widows: !0,
			zIndex: !0,
			zoom: !0
		},
		cssProps: {},
		style: function (t, e, i, o) {
			if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
				var n, s, r, a = V(e),
					l = Bt.test(e),
					c = t.style;
				if (l || (e = Qt(a)), r = _.cssHooks[e] || _.cssHooks[a], void 0 === i) return r && "get" in r && void 0 !== (n = r.get(t, !1, o)) ? n : c[e];
				"string" == (s = typeof i) && (n = et.exec(i)) && n[1] && (i = st(t, e, n), s = "number"), null != i && i == i && ("number" === s && (i += n && n[3] || (_.cssNumber[a] ? "" : "px")), v.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (c[e] = "inherit"), r && "set" in r && void 0 === (i = r.set(t, i, o)) || (l ? c.setProperty(e, i) : c[e] = i))
			}
		},
		css: function (t, e, i, o) {
			var n, s, r, a = V(e);
			return Bt.test(e) || (e = Qt(a)), (r = _.cssHooks[e] || _.cssHooks[a]) && "get" in r && (n = r.get(t, !0, i)), void 0 === n && (n = zt(t, e, o)), "normal" === n && e in Ft && (n = Ft[e]), "" === i || i ? (s = parseFloat(n), !0 === i || isFinite(s) ? s || 0 : n) : n
		}
	}), _.each(["height", "width"], function (t, a) {
		_.cssHooks[a] = {
			get: function (t, e, i) {
				if (e) return !qt.test(_.css(t, "display")) || t.getClientRects().length && t.getBoundingClientRect().width ? Kt(t, a, i) : nt(t, Wt, function () {
					return Kt(t, a, i)
				})
			},
			set: function (t, e, i) {
				var o, n = Ht(t),
					s = "border-box" === _.css(t, "boxSizing", !1, n),
					r = i && Gt(t, a, i, s, n);
				return s && v.scrollboxSize() === n.position && (r -= Math.ceil(t["offset" + a[0].toUpperCase() + a.slice(1)] - parseFloat(n[a]) - Gt(t, a, "border", !1, n) - .5)), r && (o = et.exec(e)) && "px" !== (o[3] || "px") && (t.style[a] = e, e = _.css(t, a)), Yt(0, e, r)
			}
		}
	}), _.cssHooks.marginLeft = Rt(v.reliableMarginLeft, function (t, e) {
		if (e) return (parseFloat(zt(t, "marginLeft")) || t.getBoundingClientRect().left - nt(t, {
			marginLeft: 0
		}, function () {
			return t.getBoundingClientRect().left
		})) + "px"
	}), _.each({
		margin: "",
		padding: "",
		border: "Width"
	}, function (n, s) {
		_.cssHooks[n + s] = {
			expand: function (t) {
				for (var e = 0, i = {}, o = "string" == typeof t ? t.split(" ") : [t]; e < 4; e++) i[n + it[e] + s] = o[e] || o[e - 2] || o[0];
				return i
			}
		}, "margin" !== n && (_.cssHooks[n + s].set = Yt)
	}), _.fn.extend({
		css: function (t, e) {
			return B(this, function (t, e, i) {
				var o, n, s = {},
					r = 0;
				if (Array.isArray(e)) {
					for (o = Ht(t), n = e.length; r < n; r++) s[e[r]] = _.css(t, e[r], !1, o);
					return s
				}
				return void 0 !== i ? _.style(t, e, i) : _.css(t, e)
			}, t, e, 1 < arguments.length)
		}
	}), ((_.Tween = Xt).prototype = {
		constructor: Xt,
		init: function (t, e, i, o, n, s) {
			this.elem = t, this.prop = i, this.easing = n || _.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = o, this.unit = s || (_.cssNumber[i] ? "" : "px")
		},
		cur: function () {
			var t = Xt.propHooks[this.prop];
			return t && t.get ? t.get(this) : Xt.propHooks._default.get(this)
		},
		run: function (t) {
			var e, i = Xt.propHooks[this.prop];
			return this.options.duration ? this.pos = e = _.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : Xt.propHooks._default.set(this), this
		}
	}).init.prototype = Xt.prototype, (Xt.propHooks = {
		_default: {
			get: function (t) {
				var e;
				return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = _.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0
			},
			set: function (t) {
				_.fx.step[t.prop] ? _.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[_.cssProps[t.prop]] && !_.cssHooks[t.prop] ? t.elem[t.prop] = t.now : _.style(t.elem, t.prop, t.now + t.unit)
			}
		}
	}).scrollTop = Xt.propHooks.scrollLeft = {
		set: function (t) {
			t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
		}
	}, _.easing = {
		linear: function (t) {
			return t
		},
		swing: function (t) {
			return .5 - Math.cos(t * Math.PI) / 2
		},
		_default: "swing"
	}, _.fx = Xt.prototype.init, _.fx.step = {};
	var Jt, Zt, te, ee, ie = /^(?:toggle|show|hide)$/,
		oe = /queueHooks$/;

	function ne() {
		Zt && (!1 === E.hidden && k.requestAnimationFrame ? k.requestAnimationFrame(ne) : k.setTimeout(ne, _.fx.interval), _.fx.tick())
	}

	function se() {
		return k.setTimeout(function () {
			Jt = void 0
		}), Jt = Date.now()
	}

	function re(t, e) {
		var i, o = 0,
			n = {
				height: t
			};
		for (e = e ? 1 : 0; o < 4; o += 2 - e) n["margin" + (i = it[o])] = n["padding" + i] = t;
		return e && (n.opacity = n.width = t), n
	}

	function ae(t, e, i) {
		for (var o, n = (le.tweeners[e] || []).concat(le.tweeners["*"]), s = 0, r = n.length; s < r; s++)
			if (o = n[s].call(i, e, t)) return o
	}

	function le(s, t, e) {
		var i, r, o = 0,
			n = le.prefilters.length,
			a = _.Deferred().always(function () {
				delete l.elem
			}),
			l = function () {
				if (r) return !1;
				for (var t = Jt || se(), e = Math.max(0, c.startTime + c.duration - t), i = 1 - (e / c.duration || 0), o = 0, n = c.tweens.length; o < n; o++) c.tweens[o].run(i);
				return a.notifyWith(s, [c, i, e]), i < 1 && n ? e : (n || a.notifyWith(s, [c, 1, 0]), a.resolveWith(s, [c]), !1)
			},
			c = a.promise({
				elem: s,
				props: _.extend({}, t),
				opts: _.extend(!0, {
					specialEasing: {},
					easing: _.easing._default
				}, e),
				originalProperties: t,
				originalOptions: e,
				startTime: Jt || se(),
				duration: e.duration,
				tweens: [],
				createTween: function (t, e) {
					var i = _.Tween(s, c.opts, t, e, c.opts.specialEasing[t] || c.opts.easing);
					return c.tweens.push(i), i
				},
				stop: function (t) {
					var e = 0,
						i = t ? c.tweens.length : 0;
					if (r) return this;
					for (r = !0; e < i; e++) c.tweens[e].run(1);
					return t ? (a.notifyWith(s, [c, 1, 0]), a.resolveWith(s, [c, t])) : a.rejectWith(s, [c, t]), this
				}
			}),
			d = c.props;
		for (function (t, e) {
				var i, o, n, s, r;
				for (i in t)
					if (n = e[o = V(i)], s = t[i], Array.isArray(s) && (n = s[1], s = t[i] = s[0]), i !== o && (t[o] = s, delete t[i]), (r = _.cssHooks[o]) && "expand" in r)
						for (i in s = r.expand(s), delete t[o], s) i in t || (t[i] = s[i], e[i] = n);
					else e[o] = n
			}(d, c.opts.specialEasing); o < n; o++)
			if (i = le.prefilters[o].call(c, s, d, c.opts)) return b(i.stop) && (_._queueHooks(c.elem, c.opts.queue).stop = i.stop.bind(i)), i;
		return _.map(d, ae, c), b(c.opts.start) && c.opts.start.call(s, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), _.fx.timer(_.extend(l, {
			elem: s,
			anim: c,
			queue: c.opts.queue
		})), c
	}
	_.Animation = _.extend(le, {
		tweeners: {
			"*": [function (t, e) {
				var i = this.createTween(t, e);
				return st(i.elem, t, et.exec(e), i), i
			}]
		},
		tweener: function (t, e) {
			b(t) ? (e = t, t = ["*"]) : t = t.match(j);
			for (var i, o = 0, n = t.length; o < n; o++) i = t[o], le.tweeners[i] = le.tweeners[i] || [], le.tweeners[i].unshift(e)
		},
		prefilters: [function (t, e, i) {
			var o, n, s, r, a, l, c, d, u = "width" in e || "height" in e,
				p = this,
				h = {},
				f = t.style,
				m = t.nodeType && ot(t),
				g = G.get(t, "fxshow");
			for (o in i.queue || (null == (r = _._queueHooks(t, "fx")).unqueued && (r.unqueued = 0, a = r.empty.fire, r.empty.fire = function () {
					r.unqueued || a()
				}), r.unqueued++, p.always(function () {
					p.always(function () {
						r.unqueued--, _.queue(t, "fx").length || r.empty.fire()
					})
				})), e)
				if (n = e[o], ie.test(n)) {
					if (delete e[o], s = s || "toggle" === n, n === (m ? "hide" : "show")) {
						if ("show" !== n || !g || void 0 === g[o]) continue;
						m = !0
					}
					h[o] = g && g[o] || _.style(t, o)
				}
			if ((l = !_.isEmptyObject(e)) || !_.isEmptyObject(h))
				for (o in u && 1 === t.nodeType && (i.overflow = [f.overflow, f.overflowX, f.overflowY], null == (c = g && g.display) && (c = G.get(t, "display")), "none" === (d = _.css(t, "display")) && (c ? d = c : (at([t], !0), c = t.style.display || c, d = _.css(t, "display"), at([t]))), ("inline" === d || "inline-block" === d && null != c) && "none" === _.css(t, "float") && (l || (p.done(function () {
						f.display = c
					}), null == c && (d = f.display, c = "none" === d ? "" : d)), f.display = "inline-block")), i.overflow && (f.overflow = "hidden", p.always(function () {
						f.overflow = i.overflow[0], f.overflowX = i.overflow[1], f.overflowY = i.overflow[2]
					})), l = !1, h) l || (g ? "hidden" in g && (m = g.hidden) : g = G.access(t, "fxshow", {
					display: c
				}), s && (g.hidden = !m), m && at([t], !0), p.done(function () {
					for (o in m || at([t]), G.remove(t, "fxshow"), h) _.style(t, o, h[o])
				})), l = ae(m ? g[o] : 0, o, p), o in g || (g[o] = l.start, m && (l.end = l.start, l.start = 0))
		}],
		prefilter: function (t, e) {
			e ? le.prefilters.unshift(t) : le.prefilters.push(t)
		}
	}), _.speed = function (t, e, i) {
		var o = t && "object" == typeof t ? _.extend({}, t) : {
			complete: i || !i && e || b(t) && t,
			duration: t,
			easing: i && e || e && !b(e) && e
		};
		return _.fx.off ? o.duration = 0 : "number" != typeof o.duration && (o.duration in _.fx.speeds ? o.duration = _.fx.speeds[o.duration] : o.duration = _.fx.speeds._default), null != o.queue && !0 !== o.queue || (o.queue = "fx"), o.old = o.complete, o.complete = function () {
			b(o.old) && o.old.call(this), o.queue && _.dequeue(this, o.queue)
		}, o
	}, _.fn.extend({
		fadeTo: function (t, e, i, o) {
			return this.filter(ot).css("opacity", 0).show().end().animate({
				opacity: e
			}, t, i, o)
		},
		animate: function (e, t, i, o) {
			var n = _.isEmptyObject(e),
				s = _.speed(t, i, o),
				r = function () {
					var t = le(this, _.extend({}, e), s);
					(n || G.get(this, "finish")) && t.stop(!0)
				};
			return r.finish = r, n || !1 === s.queue ? this.each(r) : this.queue(s.queue, r)
		},
		stop: function (n, t, s) {
			var r = function (t) {
				var e = t.stop;
				delete t.stop, e(s)
			};
			return "string" != typeof n && (s = t, t = n, n = void 0), t && !1 !== n && this.queue(n || "fx", []), this.each(function () {
				var t = !0,
					e = null != n && n + "queueHooks",
					i = _.timers,
					o = G.get(this);
				if (e) o[e] && o[e].stop && r(o[e]);
				else
					for (e in o) o[e] && o[e].stop && oe.test(e) && r(o[e]);
				for (e = i.length; e--;) i[e].elem !== this || null != n && i[e].queue !== n || (i[e].anim.stop(s), t = !1, i.splice(e, 1));
				!t && s || _.dequeue(this, n)
			})
		},
		finish: function (r) {
			return !1 !== r && (r = r || "fx"), this.each(function () {
				var t, e = G.get(this),
					i = e[r + "queue"],
					o = e[r + "queueHooks"],
					n = _.timers,
					s = i ? i.length : 0;
				for (e.finish = !0, _.queue(this, r, []), o && o.stop && o.stop.call(this, !0), t = n.length; t--;) n[t].elem === this && n[t].queue === r && (n[t].anim.stop(!0), n.splice(t, 1));
				for (t = 0; t < s; t++) i[t] && i[t].finish && i[t].finish.call(this);
				delete e.finish
			})
		}
	}), _.each(["toggle", "show", "hide"], function (t, o) {
		var n = _.fn[o];
		_.fn[o] = function (t, e, i) {
			return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(re(o, !0), t, e, i)
		}
	}), _.each({
		slideDown: re("show"),
		slideUp: re("hide"),
		slideToggle: re("toggle"),
		fadeIn: {
			opacity: "show"
		},
		fadeOut: {
			opacity: "hide"
		},
		fadeToggle: {
			opacity: "toggle"
		}
	}, function (t, o) {
		_.fn[t] = function (t, e, i) {
			return this.animate(o, t, e, i)
		}
	}), _.timers = [], _.fx.tick = function () {
		var t, e = 0,
			i = _.timers;
		for (Jt = Date.now(); e < i.length; e++)(t = i[e])() || i[e] !== t || i.splice(e--, 1);
		i.length || _.fx.stop(), Jt = void 0
	}, _.fx.timer = function (t) {
		_.timers.push(t), _.fx.start()
	}, _.fx.interval = 13, _.fx.start = function () {
		Zt || (Zt = !0, ne())
	}, _.fx.stop = function () {
		Zt = null
	}, _.fx.speeds = {
		slow: 600,
		fast: 200,
		_default: 400
	}, _.fn.delay = function (o, t) {
		return o = _.fx && _.fx.speeds[o] || o, t = t || "fx", this.queue(t, function (t, e) {
			var i = k.setTimeout(t, o);
			e.stop = function () {
				k.clearTimeout(i)
			}
		})
	}, te = E.createElement("input"), ee = E.createElement("select").appendChild(E.createElement("option")), te.type = "checkbox", v.checkOn = "" !== te.value, v.optSelected = ee.selected, (te = E.createElement("input")).value = "t", te.type = "radio", v.radioValue = "t" === te.value;
	var ce, de = _.expr.attrHandle;
	_.fn.extend({
		attr: function (t, e) {
			return B(this, _.attr, t, e, 1 < arguments.length)
		},
		removeAttr: function (t) {
			return this.each(function () {
				_.removeAttr(this, t)
			})
		}
	}), _.extend({
		attr: function (t, e, i) {
			var o, n, s = t.nodeType;
			if (3 !== s && 8 !== s && 2 !== s) return void 0 === t.getAttribute ? _.prop(t, e, i) : (1 === s && _.isXMLDoc(t) || (n = _.attrHooks[e.toLowerCase()] || (_.expr.match.bool.test(e) ? ce : void 0)), void 0 !== i ? null === i ? void _.removeAttr(t, e) : n && "set" in n && void 0 !== (o = n.set(t, i, e)) ? o : (t.setAttribute(e, i + ""), i) : n && "get" in n && null !== (o = n.get(t, e)) ? o : null == (o = _.find.attr(t, e)) ? void 0 : o)
		},
		attrHooks: {
			type: {
				set: function (t, e) {
					if (!v.radioValue && "radio" === e && S(t, "input")) {
						var i = t.value;
						return t.setAttribute("type", e), i && (t.value = i), e
					}
				}
			}
		},
		removeAttr: function (t, e) {
			var i, o = 0,
				n = e && e.match(j);
			if (n && 1 === t.nodeType)
				for (; i = n[o++];) t.removeAttribute(i)
		}
	}), ce = {
		set: function (t, e, i) {
			return !1 === e ? _.removeAttr(t, i) : t.setAttribute(i, i), i
		}
	}, _.each(_.expr.match.bool.source.match(/\w+/g), function (t, e) {
		var r = de[e] || _.find.attr;
		de[e] = function (t, e, i) {
			var o, n, s = e.toLowerCase();
			return i || (n = de[s], de[s] = o, o = null != r(t, e, i) ? s : null, de[s] = n), o
		}
	});
	var ue = /^(?:input|select|textarea|button)$/i,
		pe = /^(?:a|area)$/i;

	function he(t) {
		return (t.match(j) || []).join(" ")
	}

	function fe(t) {
		return t.getAttribute && t.getAttribute("class") || ""
	}

	function me(t) {
		return Array.isArray(t) ? t : "string" == typeof t && t.match(j) || []
	}
	_.fn.extend({
		prop: function (t, e) {
			return B(this, _.prop, t, e, 1 < arguments.length)
		},
		removeProp: function (t) {
			return this.each(function () {
				delete this[_.propFix[t] || t]
			})
		}
	}), _.extend({
		prop: function (t, e, i) {
			var o, n, s = t.nodeType;
			if (3 !== s && 8 !== s && 2 !== s) return 1 === s && _.isXMLDoc(t) || (e = _.propFix[e] || e, n = _.propHooks[e]), void 0 !== i ? n && "set" in n && void 0 !== (o = n.set(t, i, e)) ? o : t[e] = i : n && "get" in n && null !== (o = n.get(t, e)) ? o : t[e]
		},
		propHooks: {
			tabIndex: {
				get: function (t) {
					var e = _.find.attr(t, "tabindex");
					return e ? parseInt(e, 10) : ue.test(t.nodeName) || pe.test(t.nodeName) && t.href ? 0 : -1
				}
			}
		},
		propFix: {
			for: "htmlFor",
			class: "className"
		}
	}), v.optSelected || (_.propHooks.selected = {
		get: function (t) {
			var e = t.parentNode;
			return e && e.parentNode && e.parentNode.selectedIndex, null
		},
		set: function (t) {
			var e = t.parentNode;
			e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex)
		}
	}), _.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
		_.propFix[this.toLowerCase()] = this
	}), _.fn.extend({
		addClass: function (e) {
			var t, i, o, n, s, r, a, l = 0;
			if (b(e)) return this.each(function (t) {
				_(this).addClass(e.call(this, t, fe(this)))
			});
			if ((t = me(e)).length)
				for (; i = this[l++];)
					if (n = fe(i), o = 1 === i.nodeType && " " + he(n) + " ") {
						for (r = 0; s = t[r++];) o.indexOf(" " + s + " ") < 0 && (o += s + " ");
						n !== (a = he(o)) && i.setAttribute("class", a)
					}
			return this
		},
		removeClass: function (e) {
			var t, i, o, n, s, r, a, l = 0;
			if (b(e)) return this.each(function (t) {
				_(this).removeClass(e.call(this, t, fe(this)))
			});
			if (!arguments.length) return this.attr("class", "");
			if ((t = me(e)).length)
				for (; i = this[l++];)
					if (n = fe(i), o = 1 === i.nodeType && " " + he(n) + " ") {
						for (r = 0; s = t[r++];)
							for (; - 1 < o.indexOf(" " + s + " ");) o = o.replace(" " + s + " ", " ");
						n !== (a = he(o)) && i.setAttribute("class", a)
					}
			return this
		},
		toggleClass: function (n, e) {
			var s = typeof n,
				r = "string" === s || Array.isArray(n);
			return "boolean" == typeof e && r ? e ? this.addClass(n) : this.removeClass(n) : b(n) ? this.each(function (t) {
				_(this).toggleClass(n.call(this, t, fe(this), e), e)
			}) : this.each(function () {
				var t, e, i, o;
				if (r)
					for (e = 0, i = _(this), o = me(n); t = o[e++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t);
				else void 0 !== n && "boolean" !== s || ((t = fe(this)) && G.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === n ? "" : G.get(this, "__className__") || ""))
			})
		},
		hasClass: function (t) {
			var e, i, o = 0;
			for (e = " " + t + " "; i = this[o++];)
				if (1 === i.nodeType && -1 < (" " + he(fe(i)) + " ").indexOf(e)) return !0;
			return !1
		}
	});
	var ge = /\r/g;
	_.fn.extend({
		val: function (i) {
			var o, t, n, e = this[0];
			return arguments.length ? (n = b(i), this.each(function (t) {
				var e;
				1 === this.nodeType && (null == (e = n ? i.call(this, t, _(this).val()) : i) ? e = "" : "number" == typeof e ? e += "" : Array.isArray(e) && (e = _.map(e, function (t) {
					return null == t ? "" : t + ""
				})), (o = _.valHooks[this.type] || _.valHooks[this.nodeName.toLowerCase()]) && "set" in o && void 0 !== o.set(this, e, "value") || (this.value = e))
			})) : e ? (o = _.valHooks[e.type] || _.valHooks[e.nodeName.toLowerCase()]) && "get" in o && void 0 !== (t = o.get(e, "value")) ? t : "string" == typeof (t = e.value) ? t.replace(ge, "") : null == t ? "" : t : void 0
		}
	}), _.extend({
		valHooks: {
			option: {
				get: function (t) {
					var e = _.find.attr(t, "value");
					return null != e ? e : he(_.text(t))
				}
			},
			select: {
				get: function (t) {
					var e, i, o, n = t.options,
						s = t.selectedIndex,
						r = "select-one" === t.type,
						a = r ? null : [],
						l = r ? s + 1 : n.length;
					for (o = s < 0 ? l : r ? s : 0; o < l; o++)
						if (((i = n[o]).selected || o === s) && !i.disabled && (!i.parentNode.disabled || !S(i.parentNode, "optgroup"))) {
							if (e = _(i).val(), r) return e;
							a.push(e)
						}
					return a
				},
				set: function (t, e) {
					for (var i, o, n = t.options, s = _.makeArray(e), r = n.length; r--;)((o = n[r]).selected = -1 < _.inArray(_.valHooks.option.get(o), s)) && (i = !0);
					return i || (t.selectedIndex = -1), s
				}
			}
		}
	}), _.each(["radio", "checkbox"], function () {
		_.valHooks[this] = {
			set: function (t, e) {
				if (Array.isArray(e)) return t.checked = -1 < _.inArray(_(t).val(), e)
			}
		}, v.checkOn || (_.valHooks[this].get = function (t) {
			return null === t.getAttribute("value") ? "on" : t.value
		})
	}), v.focusin = "onfocusin" in k;
	var ve = /^(?:focusinfocus|focusoutblur)$/,
		be = function (t) {
			t.stopPropagation()
		};
	_.extend(_.event, {
		trigger: function (t, e, i, o) {
			var n, s, r, a, l, c, d, u, p = [i || E],
				h = g.call(t, "type") ? t.type : t,
				f = g.call(t, "namespace") ? t.namespace.split(".") : [];
			if (s = u = r = i = i || E, 3 !== i.nodeType && 8 !== i.nodeType && !ve.test(h + _.event.triggered) && (-1 < h.indexOf(".") && (h = (f = h.split(".")).shift(), f.sort()), l = h.indexOf(":") < 0 && "on" + h, (t = t[_.expando] ? t : new _.Event(h, "object" == typeof t && t)).isTrigger = o ? 2 : 3, t.namespace = f.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), e = null == e ? [t] : _.makeArray(e, [t]), d = _.event.special[h] || {}, o || !d.trigger || !1 !== d.trigger.apply(i, e))) {
				if (!o && !d.noBubble && !y(i)) {
					for (a = d.delegateType || h, ve.test(a + h) || (s = s.parentNode); s; s = s.parentNode) p.push(s), r = s;
					r === (i.ownerDocument || E) && p.push(r.defaultView || r.parentWindow || k)
				}
				for (n = 0;
					(s = p[n++]) && !t.isPropagationStopped();) u = s, t.type = 1 < n ? a : d.bindType || h, (c = (G.get(s, "events") || {})[t.type] && G.get(s, "handle")) && c.apply(s, e), (c = l && s[l]) && c.apply && Q(s) && (t.result = c.apply(s, e), !1 === t.result && t.preventDefault());
				return t.type = h, o || t.isDefaultPrevented() || d._default && !1 !== d._default.apply(p.pop(), e) || !Q(i) || l && b(i[h]) && !y(i) && ((r = i[l]) && (i[l] = null), _.event.triggered = h, t.isPropagationStopped() && u.addEventListener(h, be), i[h](), t.isPropagationStopped() && u.removeEventListener(h, be), _.event.triggered = void 0, r && (i[l] = r)), t.result
			}
		},
		simulate: function (t, e, i) {
			var o = _.extend(new _.Event, i, {
				type: t,
				isSimulated: !0
			});
			_.event.trigger(o, null, e)
		}
	}), _.fn.extend({
		trigger: function (t, e) {
			return this.each(function () {
				_.event.trigger(t, e, this)
			})
		},
		triggerHandler: function (t, e) {
			var i = this[0];
			if (i) return _.event.trigger(t, e, i, !0)
		}
	}), v.focusin || _.each({
		focus: "focusin",
		blur: "focusout"
	}, function (i, o) {
		var n = function (t) {
			_.event.simulate(o, t.target, _.event.fix(t))
		};
		_.event.special[o] = {
			setup: function () {
				var t = this.ownerDocument || this,
					e = G.access(t, o);
				e || t.addEventListener(i, n, !0), G.access(t, o, (e || 0) + 1)
			},
			teardown: function () {
				var t = this.ownerDocument || this,
					e = G.access(t, o) - 1;
				e ? G.access(t, o, e) : (t.removeEventListener(i, n, !0), G.remove(t, o))
			}
		}
	});
	var ye = k.location,
		we = Date.now(),
		xe = /\?/;
	_.parseXML = function (t) {
		var e;
		if (!t || "string" != typeof t) return null;
		try {
			e = (new k.DOMParser).parseFromString(t, "text/xml")
		} catch (t) {
			e = void 0
		}
		return e && !e.getElementsByTagName("parsererror").length || _.error("Invalid XML: " + t), e
	};
	var Te = /\[\]$/,
		ke = /\r?\n/g,
		Ee = /^(?:submit|button|image|reset|file)$/i,
		_e = /^(?:input|select|textarea|keygen)/i;

	function Ce(i, t, o, n) {
		var e;
		if (Array.isArray(t)) _.each(t, function (t, e) {
			o || Te.test(i) ? n(i, e) : Ce(i + "[" + ("object" == typeof e && null != e ? t : "") + "]", e, o, n)
		});
		else if (o || "object" !== x(t)) n(i, t);
		else
			for (e in t) Ce(i + "[" + e + "]", t[e], o, n)
	}
	_.param = function (t, e) {
		var i, o = [],
			n = function (t, e) {
				var i = b(e) ? e() : e;
				o[o.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == i ? "" : i)
			};
		if (Array.isArray(t) || t.jquery && !_.isPlainObject(t)) _.each(t, function () {
			n(this.name, this.value)
		});
		else
			for (i in t) Ce(i, t[i], e, n);
		return o.join("&")
	}, _.fn.extend({
		serialize: function () {
			return _.param(this.serializeArray())
		},
		serializeArray: function () {
			return this.map(function () {
				var t = _.prop(this, "elements");
				return t ? _.makeArray(t) : this
			}).filter(function () {
				var t = this.type;
				return this.name && !_(this).is(":disabled") && _e.test(this.nodeName) && !Ee.test(t) && (this.checked || !lt.test(t))
			}).map(function (t, e) {
				var i = _(this).val();
				return null == i ? null : Array.isArray(i) ? _.map(i, function (t) {
					return {
						name: e.name,
						value: t.replace(ke, "\r\n")
					}
				}) : {
					name: e.name,
					value: i.replace(ke, "\r\n")
				}
			}).get()
		}
	});
	var Se = /%20/g,
		Ae = /#.*$/,
		$e = /([?&])_=[^&]*/,
		De = /^(.*?):[ \t]*([^\r\n]*)$/gm,
		Ie = /^(?:GET|HEAD)$/,
		Oe = /^\/\//,
		Ne = {},
		Le = {},
		je = "*/".concat("*"),
		Pe = E.createElement("a");

	function He(s) {
		return function (t, e) {
			"string" != typeof t && (e = t, t = "*");
			var i, o = 0,
				n = t.toLowerCase().match(j) || [];
			if (b(e))
				for (; i = n[o++];) "+" === i[0] ? (i = i.slice(1) || "*", (s[i] = s[i] || []).unshift(e)) : (s[i] = s[i] || []).push(e)
		}
	}

	function Me(e, n, s, r) {
		var a = {},
			l = e === Le;

		function c(t) {
			var o;
			return a[t] = !0, _.each(e[t] || [], function (t, e) {
				var i = e(n, s, r);
				return "string" != typeof i || l || a[i] ? l ? !(o = i) : void 0 : (n.dataTypes.unshift(i), c(i), !1)
			}), o
		}
		return c(n.dataTypes[0]) || !a["*"] && c("*")
	}

	function ze(t, e) {
		var i, o, n = _.ajaxSettings.flatOptions || {};
		for (i in e) void 0 !== e[i] && ((n[i] ? t : o || (o = {}))[i] = e[i]);
		return o && _.extend(!0, t, o), t
	}
	Pe.href = ye.href, _.extend({
		active: 0,
		lastModified: {},
		etag: {},
		ajaxSettings: {
			url: ye.href,
			type: "GET",
			isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(ye.protocol),
			global: !0,
			processData: !0,
			async: !0,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			accepts: {
				"*": je,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
			contents: {
				xml: /\bxml\b/,
				html: /\bhtml/,
				json: /\bjson\b/
			},
			responseFields: {
				xml: "responseXML",
				text: "responseText",
				json: "responseJSON"
			},
			converters: {
				"* text": String,
				"text html": !0,
				"text json": JSON.parse,
				"text xml": _.parseXML
			},
			flatOptions: {
				url: !0,
				context: !0
			}
		},
		ajaxSetup: function (t, e) {
			return e ? ze(ze(t, _.ajaxSettings), e) : ze(_.ajaxSettings, t)
		},
		ajaxPrefilter: He(Ne),
		ajaxTransport: He(Le),
		ajax: function (t, e) {
			"object" == typeof t && (e = t, t = void 0), e = e || {};
			var d, u, p, i, h, o, f, m, n, s, g = _.ajaxSetup({}, e),
				v = g.context || g,
				b = g.context && (v.nodeType || v.jquery) ? _(v) : _.event,
				y = _.Deferred(),
				w = _.Callbacks("once memory"),
				x = g.statusCode || {},
				r = {},
				a = {},
				l = "canceled",
				T = {
					readyState: 0,
					getResponseHeader: function (t) {
						var e;
						if (f) {
							if (!i)
								for (i = {}; e = De.exec(p);) i[e[1].toLowerCase()] = e[2];
							e = i[t.toLowerCase()]
						}
						return null == e ? null : e
					},
					getAllResponseHeaders: function () {
						return f ? p : null
					},
					setRequestHeader: function (t, e) {
						return null == f && (t = a[t.toLowerCase()] = a[t.toLowerCase()] || t, r[t] = e), this
					},
					overrideMimeType: function (t) {
						return null == f && (g.mimeType = t), this
					},
					statusCode: function (t) {
						var e;
						if (t)
							if (f) T.always(t[T.status]);
							else
								for (e in t) x[e] = [x[e], t[e]];
						return this
					},
					abort: function (t) {
						var e = t || l;
						return d && d.abort(e), c(0, e), this
					}
				};
			if (y.promise(T), g.url = ((t || g.url || ye.href) + "").replace(Oe, ye.protocol + "//"), g.type = e.method || e.type || g.method || g.type, g.dataTypes = (g.dataType || "*").toLowerCase().match(j) || [""], null == g.crossDomain) {
				o = E.createElement("a");
				try {
					o.href = g.url, o.href = o.href, g.crossDomain = Pe.protocol + "//" + Pe.host != o.protocol + "//" + o.host
				} catch (t) {
					g.crossDomain = !0
				}
			}
			if (g.data && g.processData && "string" != typeof g.data && (g.data = _.param(g.data, g.traditional)), Me(Ne, g, e, T), f) return T;
			for (n in (m = _.event && g.global) && 0 == _.active++ && _.event.trigger("ajaxStart"), g.type = g.type.toUpperCase(), g.hasContent = !Ie.test(g.type), u = g.url.replace(Ae, ""), g.hasContent ? g.data && g.processData && 0 === (g.contentType || "").indexOf("application/x-www-form-urlencoded") && (g.data = g.data.replace(Se, "+")) : (s = g.url.slice(u.length), g.data && (g.processData || "string" == typeof g.data) && (u += (xe.test(u) ? "&" : "?") + g.data, delete g.data), !1 === g.cache && (u = u.replace($e, "$1"), s = (xe.test(u) ? "&" : "?") + "_=" + we++ + s), g.url = u + s), g.ifModified && (_.lastModified[u] && T.setRequestHeader("If-Modified-Since", _.lastModified[u]), _.etag[u] && T.setRequestHeader("If-None-Match", _.etag[u])), (g.data && g.hasContent && !1 !== g.contentType || e.contentType) && T.setRequestHeader("Content-Type", g.contentType), T.setRequestHeader("Accept", g.dataTypes[0] && g.accepts[g.dataTypes[0]] ? g.accepts[g.dataTypes[0]] + ("*" !== g.dataTypes[0] ? ", " + je + "; q=0.01" : "") : g.accepts["*"]), g.headers) T.setRequestHeader(n, g.headers[n]);
			if (g.beforeSend && (!1 === g.beforeSend.call(v, T, g) || f)) return T.abort();
			if (l = "abort", w.add(g.complete), T.done(g.success), T.fail(g.error), d = Me(Le, g, e, T)) {
				if (T.readyState = 1, m && b.trigger("ajaxSend", [T, g]), f) return T;
				g.async && 0 < g.timeout && (h = k.setTimeout(function () {
					T.abort("timeout")
				}, g.timeout));
				try {
					f = !1, d.send(r, c)
				} catch (t) {
					if (f) throw t;
					c(-1, t)
				}
			} else c(-1, "No Transport");

			function c(t, e, i, o) {
				var n, s, r, a, l, c = e;
				f || (f = !0, h && k.clearTimeout(h), d = void 0, p = o || "", T.readyState = 0 < t ? 4 : 0, n = 200 <= t && t < 300 || 304 === t, i && (a = function (t, e, i) {
					for (var o, n, s, r, a = t.contents, l = t.dataTypes;
						"*" === l[0];) l.shift(), void 0 === o && (o = t.mimeType || e.getResponseHeader("Content-Type"));
					if (o)
						for (n in a)
							if (a[n] && a[n].test(o)) {
								l.unshift(n);
								break
							}
					if (l[0] in i) s = l[0];
					else {
						for (n in i) {
							if (!l[0] || t.converters[n + " " + l[0]]) {
								s = n;
								break
							}
							r || (r = n)
						}
						s = s || r
					}
					if (s) return s !== l[0] && l.unshift(s), i[s]
				}(g, T, i)), a = function (t, e, i, o) {
					var n, s, r, a, l, c = {},
						d = t.dataTypes.slice();
					if (d[1])
						for (r in t.converters) c[r.toLowerCase()] = t.converters[r];
					for (s = d.shift(); s;)
						if (t.responseFields[s] && (i[t.responseFields[s]] = e), !l && o && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = s, s = d.shift())
							if ("*" === s) s = l;
							else if ("*" !== l && l !== s) {
						if (!(r = c[l + " " + s] || c["* " + s]))
							for (n in c)
								if ((a = n.split(" "))[1] === s && (r = c[l + " " + a[0]] || c["* " + a[0]])) {
									!0 === r ? r = c[n] : !0 !== c[n] && (s = a[0], d.unshift(a[1]));
									break
								}
						if (!0 !== r)
							if (r && t.throws) e = r(e);
							else try {
								e = r(e)
							} catch (t) {
								return {
									state: "parsererror",
									error: r ? t : "No conversion from " + l + " to " + s
								}
							}
					}
					return {
						state: "success",
						data: e
					}
				}(g, a, T, n), n ? (g.ifModified && ((l = T.getResponseHeader("Last-Modified")) && (_.lastModified[u] = l), (l = T.getResponseHeader("etag")) && (_.etag[u] = l)), 204 === t || "HEAD" === g.type ? c = "nocontent" : 304 === t ? c = "notmodified" : (c = a.state, s = a.data, n = !(r = a.error))) : (r = c, !t && c || (c = "error", t < 0 && (t = 0))), T.status = t, T.statusText = (e || c) + "", n ? y.resolveWith(v, [s, c, T]) : y.rejectWith(v, [T, c, r]), T.statusCode(x), x = void 0, m && b.trigger(n ? "ajaxSuccess" : "ajaxError", [T, g, n ? s : r]), w.fireWith(v, [T, c]), m && (b.trigger("ajaxComplete", [T, g]), --_.active || _.event.trigger("ajaxStop")))
			}
			return T
		},
		getJSON: function (t, e, i) {
			return _.get(t, e, i, "json")
		},
		getScript: function (t, e) {
			return _.get(t, void 0, e, "script")
		}
	}), _.each(["get", "post"], function (t, n) {
		_[n] = function (t, e, i, o) {
			return b(e) && (o = o || i, i = e, e = void 0), _.ajax(_.extend({
				url: t,
				type: n,
				dataType: o,
				data: e,
				success: i
			}, _.isPlainObject(t) && t))
		}
	}), _._evalUrl = function (t) {
		return _.ajax({
			url: t,
			type: "GET",
			dataType: "script",
			cache: !0,
			async: !1,
			global: !1,
			throws: !0
		})
	}, _.fn.extend({
		wrapAll: function (t) {
			var e;
			return this[0] && (b(t) && (t = t.call(this[0])), e = _(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
				for (var t = this; t.firstElementChild;) t = t.firstElementChild;
				return t
			}).append(this)), this
		},
		wrapInner: function (i) {
			return b(i) ? this.each(function (t) {
				_(this).wrapInner(i.call(this, t))
			}) : this.each(function () {
				var t = _(this),
					e = t.contents();
				e.length ? e.wrapAll(i) : t.append(i)
			})
		},
		wrap: function (e) {
			var i = b(e);
			return this.each(function (t) {
				_(this).wrapAll(i ? e.call(this, t) : e)
			})
		},
		unwrap: function (t) {
			return this.parent(t).not("body").each(function () {
				_(this).replaceWith(this.childNodes)
			}), this
		}
	}), _.expr.pseudos.hidden = function (t) {
		return !_.expr.pseudos.visible(t)
	}, _.expr.pseudos.visible = function (t) {
		return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length)
	}, _.ajaxSettings.xhr = function () {
		try {
			return new k.XMLHttpRequest
		} catch (t) {}
	};
	var Re = {
			0: 200,
			1223: 204
		},
		qe = _.ajaxSettings.xhr();
	v.cors = !!qe && "withCredentials" in qe, v.ajax = qe = !!qe, _.ajaxTransport(function (n) {
		var s, r;
		if (v.cors || qe && !n.crossDomain) return {
			send: function (t, e) {
				var i, o = n.xhr();
				if (o.open(n.type, n.url, n.async, n.username, n.password), n.xhrFields)
					for (i in n.xhrFields) o[i] = n.xhrFields[i];
				for (i in n.mimeType && o.overrideMimeType && o.overrideMimeType(n.mimeType), n.crossDomain || t["X-Requested-With"] || (t["X-Requested-With"] = "XMLHttpRequest"), t) o.setRequestHeader(i, t[i]);
				s = function (t) {
					return function () {
						s && (s = r = o.onload = o.onerror = o.onabort = o.ontimeout = o.onreadystatechange = null, "abort" === t ? o.abort() : "error" === t ? "number" != typeof o.status ? e(0, "error") : e(o.status, o.statusText) : e(Re[o.status] || o.status, o.statusText, "text" !== (o.responseType || "text") || "string" != typeof o.responseText ? {
							binary: o.response
						} : {
							text: o.responseText
						}, o.getAllResponseHeaders()))
					}
				}, o.onload = s(), r = o.onerror = o.ontimeout = s("error"), void 0 !== o.onabort ? o.onabort = r : o.onreadystatechange = function () {
					4 === o.readyState && k.setTimeout(function () {
						s && r()
					})
				}, s = s("abort");
				try {
					o.send(n.hasContent && n.data || null)
				} catch (t) {
					if (s) throw t
				}
			},
			abort: function () {
				s && s()
			}
		}
	}), _.ajaxPrefilter(function (t) {
		t.crossDomain && (t.contents.script = !1)
	}), _.ajaxSetup({
		accepts: {
			script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
		},
		contents: {
			script: /\b(?:java|ecma)script\b/
		},
		converters: {
			"text script": function (t) {
				return _.globalEval(t), t
			}
		}
	}), _.ajaxPrefilter("script", function (t) {
		void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
	}), _.ajaxTransport("script", function (i) {
		var o, n;
		if (i.crossDomain) return {
			send: function (t, e) {
				o = _("<script>").prop({
					charset: i.scriptCharset,
					src: i.url
				}).on("load error", n = function (t) {
					o.remove(), n = null, t && e("error" === t.type ? 404 : 200, t.type)
				}), E.head.appendChild(o[0])
			},
			abort: function () {
				n && n()
			}
		}
	});
	var Be, We = [],
		Fe = /(=)\?(?=&|$)|\?\?/;
	_.ajaxSetup({
		jsonp: "callback",
		jsonpCallback: function () {
			var t = We.pop() || _.expando + "_" + we++;
			return this[t] = !0, t
		}
	}), _.ajaxPrefilter("json jsonp", function (t, e, i) {
		var o, n, s, r = !1 !== t.jsonp && (Fe.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Fe.test(t.data) && "data");
		if (r || "jsonp" === t.dataTypes[0]) return o = t.jsonpCallback = b(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, r ? t[r] = t[r].replace(Fe, "$1" + o) : !1 !== t.jsonp && (t.url += (xe.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function () {
			return s || _.error(o + " was not called"), s[0]
		}, t.dataTypes[0] = "json", n = k[o], k[o] = function () {
			s = arguments
		}, i.always(function () {
			void 0 === n ? _(k).removeProp(o) : k[o] = n, t[o] && (t.jsonpCallback = e.jsonpCallback, We.push(o)), s && b(n) && n(s[0]), s = n = void 0
		}), "script"
	}), v.createHTMLDocument = ((Be = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Be.childNodes.length), _.parseHTML = function (t, e, i) {
		return "string" != typeof t ? [] : ("boolean" == typeof e && (i = e, e = !1), e || (v.createHTMLDocument ? ((o = (e = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, e.head.appendChild(o)) : e = E), s = !i && [], (n = A.exec(t)) ? [e.createElement(n[1])] : (n = vt([t], e, s), s && s.length && _(s).remove(), _.merge([], n.childNodes)));
		var o, n, s
	}, _.fn.load = function (t, e, i) {
		var o, n, s, r = this,
			a = t.indexOf(" ");
		return -1 < a && (o = he(t.slice(a)), t = t.slice(0, a)), b(e) ? (i = e, e = void 0) : e && "object" == typeof e && (n = "POST"), 0 < r.length && _.ajax({
			url: t,
			type: n || "GET",
			dataType: "html",
			data: e
		}).done(function (t) {
			s = arguments, r.html(o ? _("<div>").append(_.parseHTML(t)).find(o) : t)
		}).always(i && function (t, e) {
			r.each(function () {
				i.apply(this, s || [t.responseText, e, t])
			})
		}), this
	}, _.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
		_.fn[e] = function (t) {
			return this.on(e, t)
		}
	}), _.expr.pseudos.animated = function (e) {
		return _.grep(_.timers, function (t) {
			return e === t.elem
		}).length
	}, _.offset = {
		setOffset: function (t, e, i) {
			var o, n, s, r, a, l, c = _.css(t, "position"),
				d = _(t),
				u = {};
			"static" === c && (t.style.position = "relative"), a = d.offset(), s = _.css(t, "top"), l = _.css(t, "left"), ("absolute" === c || "fixed" === c) && -1 < (s + l).indexOf("auto") ? (r = (o = d.position()).top, n = o.left) : (r = parseFloat(s) || 0, n = parseFloat(l) || 0), b(e) && (e = e.call(t, i, _.extend({}, a))), null != e.top && (u.top = e.top - a.top + r), null != e.left && (u.left = e.left - a.left + n), "using" in e ? e.using.call(t, u) : d.css(u)
		}
	}, _.fn.extend({
		offset: function (e) {
			if (arguments.length) return void 0 === e ? this : this.each(function (t) {
				_.offset.setOffset(this, e, t)
			});
			var t, i, o = this[0];
			return o ? o.getClientRects().length ? (t = o.getBoundingClientRect(), i = o.ownerDocument.defaultView, {
				top: t.top + i.pageYOffset,
				left: t.left + i.pageXOffset
			}) : {
				top: 0,
				left: 0
			} : void 0
		},
		position: function () {
			if (this[0]) {
				var t, e, i, o = this[0],
					n = {
						top: 0,
						left: 0
					};
				if ("fixed" === _.css(o, "position")) e = o.getBoundingClientRect();
				else {
					for (e = this.offset(), i = o.ownerDocument, t = o.offsetParent || i.documentElement; t && (t === i.body || t === i.documentElement) && "static" === _.css(t, "position");) t = t.parentNode;
					t && t !== o && 1 === t.nodeType && ((n = _(t).offset()).top += _.css(t, "borderTopWidth", !0), n.left += _.css(t, "borderLeftWidth", !0))
				}
				return {
					top: e.top - n.top - _.css(o, "marginTop", !0),
					left: e.left - n.left - _.css(o, "marginLeft", !0)
				}
			}
		},
		offsetParent: function () {
			return this.map(function () {
				for (var t = this.offsetParent; t && "static" === _.css(t, "position");) t = t.offsetParent;
				return t || bt
			})
		}
	}), _.each({
		scrollLeft: "pageXOffset",
		scrollTop: "pageYOffset"
	}, function (e, n) {
		var s = "pageYOffset" === n;
		_.fn[e] = function (t) {
			return B(this, function (t, e, i) {
				var o;
				if (y(t) ? o = t : 9 === t.nodeType && (o = t.defaultView), void 0 === i) return o ? o[n] : t[e];
				o ? o.scrollTo(s ? o.pageXOffset : i, s ? i : o.pageYOffset) : t[e] = i
			}, e, t, arguments.length)
		}
	}), _.each(["top", "left"], function (t, i) {
		_.cssHooks[i] = Rt(v.pixelPosition, function (t, e) {
			if (e) return e = zt(t, i), Pt.test(e) ? _(t).position()[i] + "px" : e
		})
	}), _.each({
		Height: "height",
		Width: "width"
	}, function (r, a) {
		_.each({
			padding: "inner" + r,
			content: a,
			"": "outer" + r
		}, function (o, s) {
			_.fn[s] = function (t, e) {
				var i = arguments.length && (o || "boolean" != typeof t),
					n = o || (!0 === t || !0 === e ? "margin" : "border");
				return B(this, function (t, e, i) {
					var o;
					return y(t) ? 0 === s.indexOf("outer") ? t["inner" + r] : t.document.documentElement["client" + r] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + r], o["scroll" + r], t.body["offset" + r], o["offset" + r], o["client" + r])) : void 0 === i ? _.css(t, e, n) : _.style(t, e, i, n)
				}, a, i ? t : void 0, i)
			}
		})
	}), _.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (t, i) {
		_.fn[i] = function (t, e) {
			return 0 < arguments.length ? this.on(i, null, t, e) : this.trigger(i)
		}
	}), _.fn.extend({
		hover: function (t, e) {
			return this.mouseenter(t).mouseleave(e || t)
		}
	}), _.fn.extend({
		bind: function (t, e, i) {
			return this.on(t, null, e, i)
		},
		unbind: function (t, e) {
			return this.off(t, null, e)
		},
		delegate: function (t, e, i, o) {
			return this.on(e, t, i, o)
		},
		undelegate: function (t, e, i) {
			return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i)
		}
	}), _.proxy = function (t, e) {
		var i, o, n;
		if ("string" == typeof e && (i = t[e], e = t, t = i), b(t)) return o = a.call(arguments, 2), (n = function () {
			return t.apply(e || this, o.concat(a.call(arguments)))
		}).guid = t.guid = t.guid || _.guid++, n
	}, _.holdReady = function (t) {
		t ? _.readyWait++ : _.ready(!0)
	}, _.isArray = Array.isArray, _.parseJSON = JSON.parse, _.nodeName = S, _.isFunction = b, _.isWindow = y, _.camelCase = V, _.type = x, _.now = Date.now, _.isNumeric = function (t) {
		var e = _.type(t);
		return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t))
	}, "function" == typeof define && define.amd && define("jquery", [], function () {
		return _
	});
	var Ue = k.jQuery,
		Ve = k.$;
	return _.noConflict = function (t) {
		return k.$ === _ && (k.$ = Ve), t && k.jQuery === _ && (k.jQuery = Ue), _
	}, t || (k.jQuery = k.$ = _), _
}),
function (t, e) {
	"object" == typeof exports && "undefined" != typeof module ? e(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], e) : e(t.bootstrap = {}, t.jQuery, t.Popper)
}(this, function (t, e, d) {
	"use strict";

	function o(t, e) {
		for (var i = 0; i < e.length; i++) {
			var o = e[i];
			o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)
		}
	}

	function r(t, e, i) {
		return e && o(t.prototype, e), i && o(t, i), t
	}

	function c(n) {
		for (var t = 1; t < arguments.length; t++) {
			var s = null != arguments[t] ? arguments[t] : {},
				e = Object.keys(s);
			"function" == typeof Object.getOwnPropertySymbols && (e = e.concat(Object.getOwnPropertySymbols(s).filter(function (t) {
				return Object.getOwnPropertyDescriptor(s, t).enumerable
			}))), e.forEach(function (t) {
				var e, i, o;
				e = n, o = s[i = t], i in e ? Object.defineProperty(e, i, {
					value: o,
					enumerable: !0,
					configurable: !0,
					writable: !0
				}) : e[i] = o
			})
		}
		return n
	}
	e = e && e.hasOwnProperty("default") ? e.default : e, d = d && d.hasOwnProperty("default") ? d.default : d;
	var n, i, s, a, l, u, p, h, f, m, g, v, b, y, w, x, T, k, E, _, C, S, A, $, D, I, O, N, L, j, P, H, M, z, R, q, B, W, F, U, V, Q, Y, G, K, X, J, Z, tt, et, it, ot, nt, st, rt, at, lt, ct, dt, ut, pt, ht, ft, mt, gt, vt, bt, yt, wt, xt, Tt, kt, Et, _t, Ct, St, At, $t, Dt, It, Ot, Nt, Lt, jt, Pt, Ht, Mt, zt, Rt, qt, Bt, Wt, Ft, Ut, Vt, Qt, Yt, Gt, Kt, Xt, Jt, Zt, te, ee, ie, oe, ne, se, re, ae, le, ce, de, ue, pe, he, fe, me, ge, ve, be, ye, we, xe, Te, ke, Ee = (Te = "transitionend", ke = {
			TRANSITION_END: "bsTransitionEnd",
			getUID: function (t) {
				for (; t += ~~(1e6 * Math.random()), document.getElementById(t););
				return t
			},
			getSelectorFromElement: function (t) {
				var e = t.getAttribute("data-target");
				e && "#" !== e || (e = t.getAttribute("href") || "");
				try {
					return 0 < xe(document).find(e).length ? e : null
				} catch (t) {
					return null
				}
			},
			getTransitionDurationFromElement: function (t) {
				if (!t) return 0;
				var e = xe(t).css("transition-duration");
				return parseFloat(e) ? (e = e.split(",")[0], 1e3 * parseFloat(e)) : 0
			},
			reflow: function (t) {
				return t.offsetHeight
			},
			triggerTransitionEnd: function (t) {
				xe(t).trigger(Te)
			},
			supportsTransitionEnd: function () {
				return Boolean(Te)
			},
			isElement: function (t) {
				return (t[0] || t).nodeType
			},
			typeCheckConfig: function (t, e, i) {
				for (var o in i)
					if (Object.prototype.hasOwnProperty.call(i, o)) {
						var n = i[o],
							s = e[o],
							r = s && ke.isElement(s) ? "element" : (a = s, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());
						if (!new RegExp(n).test(r)) throw new Error(t.toUpperCase() + ': Option "' + o + '" provided type "' + r + '" but expected type "' + n + '".')
					}
				var a
			}
		}, (xe = e).fn.emulateTransitionEnd = function (t) {
			var e = this,
				i = !1;
			return xe(this).one(ke.TRANSITION_END, function () {
				i = !0
			}), setTimeout(function () {
				i || ke.triggerTransitionEnd(e)
			}, t), this
		}, xe.event.special[ke.TRANSITION_END] = {
			bindType: Te,
			delegateType: Te,
			handle: function (t) {
				if (xe(t.target).is(this)) return t.handleObj.handler.apply(this, arguments)
			}
		}, ke),
		_e = (i = "alert", a = "." + (s = "bs.alert"), l = (n = e).fn[i], u = {
			CLOSE: "close" + a,
			CLOSED: "closed" + a,
			CLICK_DATA_API: "click" + a + ".data-api"
		}, "alert", "fade", "show", p = function () {
			function o(t) {
				this._element = t
			}
			var t = o.prototype;
			return t.close = function (t) {
				var e = this._element;
				t && (e = this._getRootElement(t)), this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e)
			}, t.dispose = function () {
				n.removeData(this._element, s), this._element = null
			}, t._getRootElement = function (t) {
				var e = Ee.getSelectorFromElement(t),
					i = !1;
				return e && (i = n(e)[0]), i || (i = n(t).closest(".alert")[0]), i
			}, t._triggerCloseEvent = function (t) {
				var e = n.Event(u.CLOSE);
				return n(t).trigger(e), e
			}, t._removeElement = function (e) {
				var i = this;
				if (n(e).removeClass("show"), n(e).hasClass("fade")) {
					var t = Ee.getTransitionDurationFromElement(e);
					n(e).one(Ee.TRANSITION_END, function (t) {
						return i._destroyElement(e, t)
					}).emulateTransitionEnd(t)
				} else this._destroyElement(e)
			}, t._destroyElement = function (t) {
				n(t).detach().trigger(u.CLOSED).remove()
			}, o._jQueryInterface = function (i) {
				return this.each(function () {
					var t = n(this),
						e = t.data(s);
					e || (e = new o(this), t.data(s, e)), "close" === i && e[i](this)
				})
			}, o._handleDismiss = function (e) {
				return function (t) {
					t && t.preventDefault(), e.close(this)
				}
			}, r(o, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}]), o
		}(), n(document).on(u.CLICK_DATA_API, '[data-dismiss="alert"]', p._handleDismiss(new p)), n.fn[i] = p._jQueryInterface, n.fn[i].Constructor = p, n.fn[i].noConflict = function () {
			return n.fn[i] = l, p._jQueryInterface
		}, p),
		Ce = (f = "button", g = "." + (m = "bs.button"), v = ".data-api", b = (h = e).fn[f], y = "active", "btn", w = '[data-toggle^="button"]', '[data-toggle="buttons"]', "input", ".active", ".btn", x = {
			CLICK_DATA_API: "click" + g + v,
			FOCUS_BLUR_DATA_API: "focus" + g + v + " blur" + g + v
		}, T = function () {
			function i(t) {
				this._element = t
			}
			var t = i.prototype;
			return t.toggle = function () {
				var t = !0,
					e = !0,
					i = h(this._element).closest('[data-toggle="buttons"]')[0];
				if (i) {
					var o = h(this._element).find("input")[0];
					if (o) {
						if ("radio" === o.type)
							if (o.checked && h(this._element).hasClass(y)) t = !1;
							else {
								var n = h(i).find(".active")[0];
								n && h(n).removeClass(y)
							}
						if (t) {
							if (o.hasAttribute("disabled") || i.hasAttribute("disabled") || o.classList.contains("disabled") || i.classList.contains("disabled")) return;
							o.checked = !h(this._element).hasClass(y), h(o).trigger("change")
						}
						o.focus(), e = !1
					}
				}
				e && this._element.setAttribute("aria-pressed", !h(this._element).hasClass(y)), t && h(this._element).toggleClass(y)
			}, t.dispose = function () {
				h.removeData(this._element, m), this._element = null
			}, i._jQueryInterface = function (e) {
				return this.each(function () {
					var t = h(this).data(m);
					t || (t = new i(this), h(this).data(m, t)), "toggle" === e && t[e]()
				})
			}, r(i, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}]), i
		}(), h(document).on(x.CLICK_DATA_API, w, function (t) {
			t.preventDefault();
			var e = t.target;
			h(e).hasClass("btn") || (e = h(e).closest(".btn")), T._jQueryInterface.call(h(e), "toggle")
		}).on(x.FOCUS_BLUR_DATA_API, w, function (t) {
			var e = h(t.target).closest(".btn")[0];
			h(e).toggleClass("focus", /^focus(in)?$/.test(t.type))
		}), h.fn[f] = T._jQueryInterface, h.fn[f].Constructor = T, h.fn[f].noConflict = function () {
			return h.fn[f] = b, T._jQueryInterface
		}, T),
		Se = (E = "carousel", C = "." + (_ = "bs.carousel"), S = ".data-api", A = (k = e).fn[E], $ = {
			interval: 5e3,
			keyboard: !0,
			slide: !1,
			pause: "hover",
			wrap: !0
		}, D = {
			interval: "(number|boolean)",
			keyboard: "boolean",
			slide: "(boolean|string)",
			pause: "(string|boolean)",
			wrap: "boolean"
		}, I = "next", O = "prev", "left", "right", N = {
			SLIDE: "slide" + C,
			SLID: "slid" + C,
			KEYDOWN: "keydown" + C,
			MOUSEENTER: "mouseenter" + C,
			MOUSELEAVE: "mouseleave" + C,
			TOUCHEND: "touchend" + C,
			LOAD_DATA_API: "load" + C + S,
			CLICK_DATA_API: "click" + C + S
		}, "carousel", L = "active", "slide", "carousel-item-right", "carousel-item-left", "carousel-item-next", "carousel-item-prev", j = {
			ACTIVE: ".active",
			ACTIVE_ITEM: ".active.carousel-item",
			ITEM: ".carousel-item",
			NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
			INDICATORS: ".carousel-indicators",
			DATA_SLIDE: "[data-slide], [data-slide-to]",
			DATA_RIDE: '[data-ride="carousel"]'
		}, P = function () {
			function s(t, e) {
				this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(e), this._element = k(t)[0], this._indicatorsElement = k(this._element).find(j.INDICATORS)[0], this._addEventListeners()
			}
			var t = s.prototype;
			return t.next = function () {
				this._isSliding || this._slide(I)
			}, t.nextWhenVisible = function () {
				!document.hidden && k(this._element).is(":visible") && "hidden" !== k(this._element).css("visibility") && this.next()
			}, t.prev = function () {
				this._isSliding || this._slide(O)
			}, t.pause = function (t) {
				t || (this._isPaused = !0), k(this._element).find(j.NEXT_PREV)[0] && (Ee.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
			}, t.cycle = function (t) {
				t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
			}, t.to = function (t) {
				var e = this;
				this._activeElement = k(this._element).find(j.ACTIVE_ITEM)[0];
				var i = this._getItemIndex(this._activeElement);
				if (!(t > this._items.length - 1 || t < 0))
					if (this._isSliding) k(this._element).one(N.SLID, function () {
						return e.to(t)
					});
					else {
						if (i === t) return this.pause(), void this.cycle();
						var o = i < t ? I : O;
						this._slide(o, this._items[t])
					}
			}, t.dispose = function () {
				k(this._element).off(C), k.removeData(this._element, _), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
			}, t._getConfig = function (t) {
				return t = c({}, $, t), Ee.typeCheckConfig(E, t, D), t
			}, t._addEventListeners = function () {
				var e = this;
				this._config.keyboard && k(this._element).on(N.KEYDOWN, function (t) {
					return e._keydown(t)
				}), "hover" === this._config.pause && (k(this._element).on(N.MOUSEENTER, function (t) {
					return e.pause(t)
				}).on(N.MOUSELEAVE, function (t) {
					return e.cycle(t)
				}), "ontouchstart" in document.documentElement && k(this._element).on(N.TOUCHEND, function () {
					e.pause(), e.touchTimeout && clearTimeout(e.touchTimeout), e.touchTimeout = setTimeout(function (t) {
						return e.cycle(t)
					}, 500 + e._config.interval)
				}))
			}, t._keydown = function (t) {
				if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
					case 37:
						t.preventDefault(), this.prev();
						break;
					case 39:
						t.preventDefault(), this.next()
				}
			}, t._getItemIndex = function (t) {
				return this._items = k.makeArray(k(t).parent().find(j.ITEM)), this._items.indexOf(t)
			}, t._getItemByDirection = function (t, e) {
				var i = t === I,
					o = t === O,
					n = this._getItemIndex(e),
					s = this._items.length - 1;
				if ((o && 0 === n || i && n === s) && !this._config.wrap) return e;
				var r = (n + (t === O ? -1 : 1)) % this._items.length;
				return -1 === r ? this._items[this._items.length - 1] : this._items[r]
			}, t._triggerSlideEvent = function (t, e) {
				var i = this._getItemIndex(t),
					o = this._getItemIndex(k(this._element).find(j.ACTIVE_ITEM)[0]),
					n = k.Event(N.SLIDE, {
						relatedTarget: t,
						direction: e,
						from: o,
						to: i
					});
				return k(this._element).trigger(n), n
			}, t._setActiveIndicatorElement = function (t) {
				if (this._indicatorsElement) {
					k(this._indicatorsElement).find(j.ACTIVE).removeClass(L);
					var e = this._indicatorsElement.children[this._getItemIndex(t)];
					e && k(e).addClass(L)
				}
			}, t._slide = function (t, e) {
				var i, o, n, s = this,
					r = k(this._element).find(j.ACTIVE_ITEM)[0],
					a = this._getItemIndex(r),
					l = e || r && this._getItemByDirection(t, r),
					c = this._getItemIndex(l),
					d = Boolean(this._interval);
				if (t === I ? (i = "carousel-item-left", o = "carousel-item-next", n = "left") : (i = "carousel-item-right", o = "carousel-item-prev", n = "right"), l && k(l).hasClass(L)) this._isSliding = !1;
				else if (!this._triggerSlideEvent(l, n).isDefaultPrevented() && r && l) {
					this._isSliding = !0, d && this.pause(), this._setActiveIndicatorElement(l);
					var u = k.Event(N.SLID, {
						relatedTarget: l,
						direction: n,
						from: a,
						to: c
					});
					if (k(this._element).hasClass("slide")) {
						k(l).addClass(o), Ee.reflow(l), k(r).addClass(i), k(l).addClass(i);
						var p = Ee.getTransitionDurationFromElement(r);
						k(r).one(Ee.TRANSITION_END, function () {
							k(l).removeClass(i + " " + o).addClass(L), k(r).removeClass(L + " " + o + " " + i), s._isSliding = !1, setTimeout(function () {
								return k(s._element).trigger(u)
							}, 0)
						}).emulateTransitionEnd(p)
					} else k(r).removeClass(L), k(l).addClass(L), this._isSliding = !1, k(this._element).trigger(u);
					d && this.cycle()
				}
			}, s._jQueryInterface = function (o) {
				return this.each(function () {
					var t = k(this).data(_),
						e = c({}, $, k(this).data());
					"object" == typeof o && (e = c({}, e, o));
					var i = "string" == typeof o ? o : e.slide;
					if (t || (t = new s(this, e), k(this).data(_, t)), "number" == typeof o) t.to(o);
					else if ("string" == typeof i) {
						if (void 0 === t[i]) throw new TypeError('No method named "' + i + '"');
						t[i]()
					} else e.interval && (t.pause(), t.cycle())
				})
			}, s._dataApiClickHandler = function (t) {
				var e = Ee.getSelectorFromElement(this);
				if (e) {
					var i = k(e)[0];
					if (i && k(i).hasClass("carousel")) {
						var o = c({}, k(i).data(), k(this).data()),
							n = this.getAttribute("data-slide-to");
						n && (o.interval = !1), s._jQueryInterface.call(k(i), o), n && k(i).data(_).to(n), t.preventDefault()
					}
				}
			}, r(s, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return $
				}
			}]), s
		}(), k(document).on(N.CLICK_DATA_API, j.DATA_SLIDE, P._dataApiClickHandler), k(window).on(N.LOAD_DATA_API, function () {
			k(j.DATA_RIDE).each(function () {
				var t = k(this);
				P._jQueryInterface.call(t, t.data())
			})
		}), k.fn[E] = P._jQueryInterface, k.fn[E].Constructor = P, k.fn[E].noConflict = function () {
			return k.fn[E] = A, P._jQueryInterface
		}, P),
		Ae = (M = "collapse", R = "." + (z = "bs.collapse"), q = (H = e).fn[M], B = {
			toggle: !0,
			parent: ""
		}, W = {
			toggle: "boolean",
			parent: "(string|element)"
		}, F = {
			SHOW: "show" + R,
			SHOWN: "shown" + R,
			HIDE: "hide" + R,
			HIDDEN: "hidden" + R,
			CLICK_DATA_API: "click" + R + ".data-api"
		}, U = "show", V = "collapse", Q = "collapsing", Y = "collapsed", "width", "height", G = {
			ACTIVES: ".show, .collapsing",
			DATA_TOGGLE: '[data-toggle="collapse"]'
		}, K = function () {
			function a(t, e) {
				this._isTransitioning = !1, this._element = t, this._config = this._getConfig(e), this._triggerArray = H.makeArray(H('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'));
				for (var i = H(G.DATA_TOGGLE), o = 0; o < i.length; o++) {
					var n = i[o],
						s = Ee.getSelectorFromElement(n);
					null !== s && 0 < H(s).filter(t).length && (this._selector = s, this._triggerArray.push(n))
				}
				this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
			}
			var t = a.prototype;
			return t.toggle = function () {
				H(this._element).hasClass(U) ? this.hide() : this.show()
			}, t.show = function () {
				var t, e, i = this;
				if (!(this._isTransitioning || H(this._element).hasClass(U) || (this._parent && 0 === (t = H.makeArray(H(this._parent).find(G.ACTIVES).filter('[data-parent="' + this._config.parent + '"]'))).length && (t = null), t && (e = H(t).not(this._selector).data(z)) && e._isTransitioning))) {
					var o = H.Event(F.SHOW);
					if (H(this._element).trigger(o), !o.isDefaultPrevented()) {
						t && (a._jQueryInterface.call(H(t).not(this._selector), "hide"), e || H(t).data(z, null));
						var n = this._getDimension();
						H(this._element).removeClass(V).addClass(Q), (this._element.style[n] = 0) < this._triggerArray.length && H(this._triggerArray).removeClass(Y).attr("aria-expanded", !0), this.setTransitioning(!0);
						var s = "scroll" + (n[0].toUpperCase() + n.slice(1)),
							r = Ee.getTransitionDurationFromElement(this._element);
						H(this._element).one(Ee.TRANSITION_END, function () {
							H(i._element).removeClass(Q).addClass(V).addClass(U), i._element.style[n] = "", i.setTransitioning(!1), H(i._element).trigger(F.SHOWN)
						}).emulateTransitionEnd(r), this._element.style[n] = this._element[s] + "px"
					}
				}
			}, t.hide = function () {
				var t = this;
				if (!this._isTransitioning && H(this._element).hasClass(U)) {
					var e = H.Event(F.HIDE);
					if (H(this._element).trigger(e), !e.isDefaultPrevented()) {
						var i = this._getDimension();
						if (this._element.style[i] = this._element.getBoundingClientRect()[i] + "px", Ee.reflow(this._element), H(this._element).addClass(Q).removeClass(V).removeClass(U), 0 < this._triggerArray.length)
							for (var o = 0; o < this._triggerArray.length; o++) {
								var n = this._triggerArray[o],
									s = Ee.getSelectorFromElement(n);
								null !== s && (H(s).hasClass(U) || H(n).addClass(Y).attr("aria-expanded", !1))
							}
						this.setTransitioning(!0), this._element.style[i] = "";
						var r = Ee.getTransitionDurationFromElement(this._element);
						H(this._element).one(Ee.TRANSITION_END, function () {
							t.setTransitioning(!1), H(t._element).removeClass(Q).addClass(V).trigger(F.HIDDEN)
						}).emulateTransitionEnd(r)
					}
				}
			}, t.setTransitioning = function (t) {
				this._isTransitioning = t
			}, t.dispose = function () {
				H.removeData(this._element, z), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
			}, t._getConfig = function (t) {
				return (t = c({}, B, t)).toggle = Boolean(t.toggle), Ee.typeCheckConfig(M, t, W), t
			}, t._getDimension = function () {
				return H(this._element).hasClass("width") ? "width" : "height"
			}, t._getParent = function () {
				var i = this,
					t = null;
				Ee.isElement(this._config.parent) ? (t = this._config.parent, void 0 !== this._config.parent.jquery && (t = this._config.parent[0])) : t = H(this._config.parent)[0];
				var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
				return H(t).find(e).each(function (t, e) {
					i._addAriaAndCollapsedClass(a._getTargetFromElement(e), [e])
				}), t
			}, t._addAriaAndCollapsedClass = function (t, e) {
				if (t) {
					var i = H(t).hasClass(U);
					0 < e.length && H(e).toggleClass(Y, !i).attr("aria-expanded", i)
				}
			}, a._getTargetFromElement = function (t) {
				var e = Ee.getSelectorFromElement(t);
				return e ? H(e)[0] : null
			}, a._jQueryInterface = function (o) {
				return this.each(function () {
					var t = H(this),
						e = t.data(z),
						i = c({}, B, t.data(), "object" == typeof o && o ? o : {});
					if (!e && i.toggle && /show|hide/.test(o) && (i.toggle = !1), e || (e = new a(this, i), t.data(z, e)), "string" == typeof o) {
						if (void 0 === e[o]) throw new TypeError('No method named "' + o + '"');
						e[o]()
					}
				})
			}, r(a, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return B
				}
			}]), a
		}(), H(document).on(F.CLICK_DATA_API, G.DATA_TOGGLE, function (t) {
			"A" === t.currentTarget.tagName && t.preventDefault();
			var i = H(this),
				e = Ee.getSelectorFromElement(this);
			H(e).each(function () {
				var t = H(this),
					e = t.data(z) ? "toggle" : i.data();
				K._jQueryInterface.call(t, e)
			})
		}), H.fn[M] = K._jQueryInterface, H.fn[M].Constructor = K, H.fn[M].noConflict = function () {
			return H.fn[M] = q, K._jQueryInterface
		}, K),
		$e = (J = "dropdown", tt = "." + (Z = "bs.dropdown"), et = ".data-api", it = (X = e).fn[J], ot = new RegExp("38|40|27"), nt = {
			HIDE: "hide" + tt,
			HIDDEN: "hidden" + tt,
			SHOW: "show" + tt,
			SHOWN: "shown" + tt,
			CLICK: "click" + tt,
			CLICK_DATA_API: "click" + tt + et,
			KEYDOWN_DATA_API: "keydown" + tt + et,
			KEYUP_DATA_API: "keyup" + tt + et
		}, st = "disabled", rt = "show", "dropup", "dropright", "dropleft", at = "dropdown-menu-right", "position-static", lt = '[data-toggle="dropdown"]', ".dropdown form", ct = ".dropdown-menu", ".navbar-nav", ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)", "top-start", "top-end", "bottom-start", "bottom-end", "right-start", "left-start", dt = {
			offset: 0,
			flip: !0,
			boundary: "scrollParent",
			reference: "toggle",
			display: "dynamic"
		}, ut = {
			offset: "(number|string|function)",
			flip: "boolean",
			boundary: "(string|element)",
			reference: "(string|element)",
			display: "string"
		}, pt = function () {
			function l(t, e) {
				this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
			}
			var t = l.prototype;
			return t.toggle = function () {
				if (!this._element.disabled && !X(this._element).hasClass(st)) {
					var t = l._getParentFromElement(this._element),
						e = X(this._menu).hasClass(rt);
					if (l._clearMenus(), !e) {
						var i = {
								relatedTarget: this._element
							},
							o = X.Event(nt.SHOW, i);
						if (X(t).trigger(o), !o.isDefaultPrevented()) {
							if (!this._inNavbar) {
								if (void 0 === d) throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");
								var n = this._element;
								"parent" === this._config.reference ? n = t : Ee.isElement(this._config.reference) && (n = this._config.reference, void 0 !== this._config.reference.jquery && (n = this._config.reference[0])), "scrollParent" !== this._config.boundary && X(t).addClass("position-static"), this._popper = new d(n, this._menu, this._getPopperConfig())
							}
							"ontouchstart" in document.documentElement && 0 === X(t).closest(".navbar-nav").length && X(document.body).children().on("mouseover", null, X.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), X(this._menu).toggleClass(rt), X(t).toggleClass(rt).trigger(X.Event(nt.SHOWN, i))
						}
					}
				}
			}, t.dispose = function () {
				X.removeData(this._element, Z), X(this._element).off(tt), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null)
			}, t.update = function () {
				this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
			}, t._addEventListeners = function () {
				var e = this;
				X(this._element).on(nt.CLICK, function (t) {
					t.preventDefault(), t.stopPropagation(), e.toggle()
				})
			}, t._getConfig = function (t) {
				return t = c({}, this.constructor.Default, X(this._element).data(), t), Ee.typeCheckConfig(J, t, this.constructor.DefaultType), t
			}, t._getMenuElement = function () {
				if (!this._menu) {
					var t = l._getParentFromElement(this._element);
					this._menu = X(t).find(ct)[0]
				}
				return this._menu
			}, t._getPlacement = function () {
				var t = X(this._element).parent(),
					e = "bottom-start";
				return t.hasClass("dropup") ? (e = "top-start", X(this._menu).hasClass(at) && (e = "top-end")) : t.hasClass("dropright") ? e = "right-start" : t.hasClass("dropleft") ? e = "left-start" : X(this._menu).hasClass(at) && (e = "bottom-end"), e
			}, t._detectNavbar = function () {
				return 0 < X(this._element).closest(".navbar").length
			}, t._getPopperConfig = function () {
				var e = this,
					t = {};
				"function" == typeof this._config.offset ? t.fn = function (t) {
					return t.offsets = c({}, t.offsets, e._config.offset(t.offsets) || {}), t
				} : t.offset = this._config.offset;
				var i = {
					placement: this._getPlacement(),
					modifiers: {
						offset: t,
						flip: {
							enabled: this._config.flip
						},
						preventOverflow: {
							boundariesElement: this._config.boundary
						}
					}
				};
				return "static" === this._config.display && (i.modifiers.applyStyle = {
					enabled: !1
				}), i
			}, l._jQueryInterface = function (e) {
				return this.each(function () {
					var t = X(this).data(Z);
					if (t || (t = new l(this, "object" == typeof e ? e : null), X(this).data(Z, t)), "string" == typeof e) {
						if (void 0 === t[e]) throw new TypeError('No method named "' + e + '"');
						t[e]()
					}
				})
			}, l._clearMenus = function (t) {
				if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which))
					for (var e = X.makeArray(X(lt)), i = 0; i < e.length; i++) {
						var o = l._getParentFromElement(e[i]),
							n = X(e[i]).data(Z),
							s = {
								relatedTarget: e[i]
							};
						if (n) {
							var r = n._menu;
							if (X(o).hasClass(rt) && !(t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && X.contains(o, t.target))) {
								var a = X.Event(nt.HIDE, s);
								X(o).trigger(a), a.isDefaultPrevented() || ("ontouchstart" in document.documentElement && X(document.body).children().off("mouseover", null, X.noop), e[i].setAttribute("aria-expanded", "false"), X(r).removeClass(rt), X(o).removeClass(rt).trigger(X.Event(nt.HIDDEN, s)))
							}
						}
					}
			}, l._getParentFromElement = function (t) {
				var e, i = Ee.getSelectorFromElement(t);
				return i && (e = X(i)[0]), e || t.parentNode
			}, l._dataApiKeydownHandler = function (t) {
				if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || 27 !== t.which && (40 !== t.which && 38 !== t.which || X(t.target).closest(ct).length)) : ot.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !X(this).hasClass(st))) {
					var e = l._getParentFromElement(this),
						i = X(e).hasClass(rt);
					if ((i || 27 === t.which && 32 === t.which) && (!i || 27 !== t.which && 32 !== t.which)) {
						var o = X(e).find(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)").get();
						if (0 !== o.length) {
							var n = o.indexOf(t.target);
							38 === t.which && 0 < n && n--, 40 === t.which && n < o.length - 1 && n++, n < 0 && (n = 0), o[n].focus()
						}
					} else {
						if (27 === t.which) {
							var s = X(e).find(lt)[0];
							X(s).trigger("focus")
						}
						X(this).trigger("click")
					}
				}
			}, r(l, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return dt
				}
			}, {
				key: "DefaultType",
				get: function () {
					return ut
				}
			}]), l
		}(), X(document).on(nt.KEYDOWN_DATA_API, lt, pt._dataApiKeydownHandler).on(nt.KEYDOWN_DATA_API, ct, pt._dataApiKeydownHandler).on(nt.CLICK_DATA_API + " " + nt.KEYUP_DATA_API, pt._clearMenus).on(nt.CLICK_DATA_API, lt, function (t) {
			t.preventDefault(), t.stopPropagation(), pt._jQueryInterface.call(X(this), "toggle")
		}).on(nt.CLICK_DATA_API, ".dropdown form", function (t) {
			t.stopPropagation()
		}), X.fn[J] = pt._jQueryInterface, X.fn[J].Constructor = pt, X.fn[J].noConflict = function () {
			return X.fn[J] = it, pt._jQueryInterface
		}, pt),
		De = (ft = "modal", gt = "." + (mt = "bs.modal"), vt = (ht = e).fn[ft], bt = {
			backdrop: !0,
			keyboard: !0,
			focus: !0,
			show: !0
		}, yt = {
			backdrop: "(boolean|string)",
			keyboard: "boolean",
			focus: "boolean",
			show: "boolean"
		}, wt = {
			HIDE: "hide" + gt,
			HIDDEN: "hidden" + gt,
			SHOW: "show" + gt,
			SHOWN: "shown" + gt,
			FOCUSIN: "focusin" + gt,
			RESIZE: "resize" + gt,
			CLICK_DISMISS: "click.dismiss" + gt,
			KEYDOWN_DISMISS: "keydown.dismiss" + gt,
			MOUSEUP_DISMISS: "mouseup.dismiss" + gt,
			MOUSEDOWN_DISMISS: "mousedown.dismiss" + gt,
			CLICK_DATA_API: "click" + gt + ".data-api"
		}, "modal-scrollbar-measure", "modal-backdrop", xt = "modal-open", Tt = "fade", kt = "show", Et = {
			DIALOG: ".modal-dialog",
			DATA_TOGGLE: '[data-toggle="modal"]',
			DATA_DISMISS: '[data-dismiss="modal"]',
			FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
			STICKY_CONTENT: ".sticky-top",
			NAVBAR_TOGGLER: ".navbar-toggler"
		}, _t = function () {
			function n(t, e) {
				this._config = this._getConfig(e), this._element = t, this._dialog = ht(t).find(Et.DIALOG)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._scrollbarWidth = 0
			}
			var t = n.prototype;
			return t.toggle = function (t) {
				return this._isShown ? this.hide() : this.show(t)
			}, t.show = function (t) {
				var e = this;
				if (!this._isTransitioning && !this._isShown) {
					ht(this._element).hasClass(Tt) && (this._isTransitioning = !0);
					var i = ht.Event(wt.SHOW, {
						relatedTarget: t
					});
					ht(this._element).trigger(i), this._isShown || i.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), ht(document.body).addClass(xt), this._setEscapeEvent(), this._setResizeEvent(), ht(this._element).on(wt.CLICK_DISMISS, Et.DATA_DISMISS, function (t) {
						return e.hide(t)
					}), ht(this._dialog).on(wt.MOUSEDOWN_DISMISS, function () {
						ht(e._element).one(wt.MOUSEUP_DISMISS, function (t) {
							ht(t.target).is(e._element) && (e._ignoreBackdropClick = !0)
						})
					}), this._showBackdrop(function () {
						return e._showElement(t)
					}))
				}
			}, t.hide = function (t) {
				var e = this;
				if (t && t.preventDefault(), !this._isTransitioning && this._isShown) {
					var i = ht.Event(wt.HIDE);
					if (ht(this._element).trigger(i), this._isShown && !i.isDefaultPrevented()) {
						this._isShown = !1;
						var o = ht(this._element).hasClass(Tt);
						if (o && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), ht(document).off(wt.FOCUSIN), ht(this._element).removeClass(kt), ht(this._element).off(wt.CLICK_DISMISS), ht(this._dialog).off(wt.MOUSEDOWN_DISMISS), o) {
							var n = Ee.getTransitionDurationFromElement(this._element);
							ht(this._element).one(Ee.TRANSITION_END, function (t) {
								return e._hideModal(t)
							}).emulateTransitionEnd(n)
						} else this._hideModal()
					}
				}
			}, t.dispose = function () {
				ht.removeData(this._element, mt), ht(window, document, this._element, this._backdrop).off(gt), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null
			}, t.handleUpdate = function () {
				this._adjustDialog()
			}, t._getConfig = function (t) {
				return t = c({}, bt, t), Ee.typeCheckConfig(ft, t, yt), t
			}, t._showElement = function (t) {
				var e = this,
					i = ht(this._element).hasClass(Tt);
				this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, i && Ee.reflow(this._element), ht(this._element).addClass(kt), this._config.focus && this._enforceFocus();
				var o = ht.Event(wt.SHOWN, {
						relatedTarget: t
					}),
					n = function () {
						e._config.focus && e._element.focus(), e._isTransitioning = !1, ht(e._element).trigger(o)
					};
				if (i) {
					var s = Ee.getTransitionDurationFromElement(this._element);
					ht(this._dialog).one(Ee.TRANSITION_END, n).emulateTransitionEnd(s)
				} else n()
			}, t._enforceFocus = function () {
				var e = this;
				ht(document).off(wt.FOCUSIN).on(wt.FOCUSIN, function (t) {
					document !== t.target && e._element !== t.target && 0 === ht(e._element).has(t.target).length && e._element.focus()
				})
			}, t._setEscapeEvent = function () {
				var e = this;
				this._isShown && this._config.keyboard ? ht(this._element).on(wt.KEYDOWN_DISMISS, function (t) {
					27 === t.which && (t.preventDefault(), e.hide())
				}) : this._isShown || ht(this._element).off(wt.KEYDOWN_DISMISS)
			}, t._setResizeEvent = function () {
				var e = this;
				this._isShown ? ht(window).on(wt.RESIZE, function (t) {
					return e.handleUpdate(t)
				}) : ht(window).off(wt.RESIZE)
			}, t._hideModal = function () {
				var t = this;
				this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function () {
					ht(document.body).removeClass(xt), t._resetAdjustments(), t._resetScrollbar(), ht(t._element).trigger(wt.HIDDEN)
				})
			}, t._removeBackdrop = function () {
				this._backdrop && (ht(this._backdrop).remove(), this._backdrop = null)
			}, t._showBackdrop = function (t) {
				var e = this,
					i = ht(this._element).hasClass(Tt) ? Tt : "";
				if (this._isShown && this._config.backdrop) {
					if (this._backdrop = document.createElement("div"), this._backdrop.className = "modal-backdrop", i && ht(this._backdrop).addClass(i), ht(this._backdrop).appendTo(document.body), ht(this._element).on(wt.CLICK_DISMISS, function (t) {
							e._ignoreBackdropClick ? e._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === e._config.backdrop ? e._element.focus() : e.hide())
						}), i && Ee.reflow(this._backdrop), ht(this._backdrop).addClass(kt), !t) return;
					if (!i) return void t();
					var o = Ee.getTransitionDurationFromElement(this._backdrop);
					ht(this._backdrop).one(Ee.TRANSITION_END, t).emulateTransitionEnd(o)
				} else if (!this._isShown && this._backdrop) {
					ht(this._backdrop).removeClass(kt);
					var n = function () {
						e._removeBackdrop(), t && t()
					};
					if (ht(this._element).hasClass(Tt)) {
						var s = Ee.getTransitionDurationFromElement(this._backdrop);
						ht(this._backdrop).one(Ee.TRANSITION_END, n).emulateTransitionEnd(s)
					} else n()
				} else t && t()
			}, t._adjustDialog = function () {
				var t = this._element.scrollHeight > document.documentElement.clientHeight;
				!this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px")
			}, t._resetAdjustments = function () {
				this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
			}, t._checkScrollbar = function () {
				var t = document.body.getBoundingClientRect();
				this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
			}, t._setScrollbar = function () {
				var n = this;
				if (this._isBodyOverflowing) {
					ht(Et.FIXED_CONTENT).each(function (t, e) {
						var i = ht(e)[0].style.paddingRight,
							o = ht(e).css("padding-right");
						ht(e).data("padding-right", i).css("padding-right", parseFloat(o) + n._scrollbarWidth + "px")
					}), ht(Et.STICKY_CONTENT).each(function (t, e) {
						var i = ht(e)[0].style.marginRight,
							o = ht(e).css("margin-right");
						ht(e).data("margin-right", i).css("margin-right", parseFloat(o) - n._scrollbarWidth + "px")
					}), ht(Et.NAVBAR_TOGGLER).each(function (t, e) {
						var i = ht(e)[0].style.marginRight,
							o = ht(e).css("margin-right");
						ht(e).data("margin-right", i).css("margin-right", parseFloat(o) + n._scrollbarWidth + "px")
					});
					var t = document.body.style.paddingRight,
						e = ht(document.body).css("padding-right");
					ht(document.body).data("padding-right", t).css("padding-right", parseFloat(e) + this._scrollbarWidth + "px")
				}
			}, t._resetScrollbar = function () {
				ht(Et.FIXED_CONTENT).each(function (t, e) {
					var i = ht(e).data("padding-right");
					void 0 !== i && ht(e).css("padding-right", i).removeData("padding-right")
				}), ht(Et.STICKY_CONTENT + ", " + Et.NAVBAR_TOGGLER).each(function (t, e) {
					var i = ht(e).data("margin-right");
					void 0 !== i && ht(e).css("margin-right", i).removeData("margin-right")
				});
				var t = ht(document.body).data("padding-right");
				void 0 !== t && ht(document.body).css("padding-right", t).removeData("padding-right")
			}, t._getScrollbarWidth = function () {
				var t = document.createElement("div");
				t.className = "modal-scrollbar-measure", document.body.appendChild(t);
				var e = t.getBoundingClientRect().width - t.clientWidth;
				return document.body.removeChild(t), e
			}, n._jQueryInterface = function (i, o) {
				return this.each(function () {
					var t = ht(this).data(mt),
						e = c({}, bt, ht(this).data(), "object" == typeof i && i ? i : {});
					if (t || (t = new n(this, e), ht(this).data(mt, t)), "string" == typeof i) {
						if (void 0 === t[i]) throw new TypeError('No method named "' + i + '"');
						t[i](o)
					} else e.show && t.show(o)
				})
			}, r(n, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return bt
				}
			}]), n
		}(), ht(document).on(wt.CLICK_DATA_API, Et.DATA_TOGGLE, function (t) {
			var e, i = this,
				o = Ee.getSelectorFromElement(this);
			o && (e = ht(o)[0]);
			var n = ht(e).data(mt) ? "toggle" : c({}, ht(e).data(), ht(this).data());
			"A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();
			var s = ht(e).one(wt.SHOW, function (t) {
				t.isDefaultPrevented() || s.one(wt.HIDDEN, function () {
					ht(i).is(":visible") && i.focus()
				})
			});
			_t._jQueryInterface.call(ht(e), n, this)
		}), ht.fn[ft] = _t._jQueryInterface, ht.fn[ft].Constructor = _t, ht.fn[ft].noConflict = function () {
			return ht.fn[ft] = vt, _t._jQueryInterface
		}, _t),
		Ie = (St = "tooltip", $t = "." + (At = "bs.tooltip"), Dt = (Ct = e).fn[St], It = "bs-tooltip", Ot = new RegExp("(^|\\s)" + It + "\\S+", "g"), jt = {
			animation: !0,
			template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
			trigger: "hover focus",
			title: "",
			delay: 0,
			html: (Lt = {
				AUTO: "auto",
				TOP: "top",
				RIGHT: "right",
				BOTTOM: "bottom",
				LEFT: "left"
			}, !1),
			selector: (Nt = {
				animation: "boolean",
				template: "string",
				title: "(string|element|function)",
				trigger: "string",
				delay: "(number|object)",
				html: "boolean",
				selector: "(string|boolean)",
				placement: "(string|function)",
				offset: "(number|string)",
				container: "(string|element|boolean)",
				fallbackPlacement: "(string|array)",
				boundary: "(string|element)"
			}, !1),
			placement: "top",
			offset: 0,
			container: !1,
			fallbackPlacement: "flip",
			boundary: "scrollParent"
		}, "out", Ht = {
			HIDE: "hide" + $t,
			HIDDEN: "hidden" + $t,
			SHOW: (Pt = "show") + $t,
			SHOWN: "shown" + $t,
			INSERTED: "inserted" + $t,
			CLICK: "click" + $t,
			FOCUSIN: "focusin" + $t,
			FOCUSOUT: "focusout" + $t,
			MOUSEENTER: "mouseenter" + $t,
			MOUSELEAVE: "mouseleave" + $t
		}, Mt = "fade", zt = "show", ".tooltip-inner", ".arrow", Rt = "hover", qt = "focus", "click", "manual", Bt = function () {
			function o(t, e) {
				if (void 0 === d) throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");
				this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners()
			}
			var t = o.prototype;
			return t.enable = function () {
				this._isEnabled = !0
			}, t.disable = function () {
				this._isEnabled = !1
			}, t.toggleEnabled = function () {
				this._isEnabled = !this._isEnabled
			}, t.toggle = function (t) {
				if (this._isEnabled)
					if (t) {
						var e = this.constructor.DATA_KEY,
							i = Ct(t.currentTarget).data(e);
						i || (i = new this.constructor(t.currentTarget, this._getDelegateConfig()), Ct(t.currentTarget).data(e, i)), i._activeTrigger.click = !i._activeTrigger.click, i._isWithActiveTrigger() ? i._enter(null, i) : i._leave(null, i)
					} else {
						if (Ct(this.getTipElement()).hasClass(zt)) return void this._leave(null, this);
						this._enter(null, this)
					}
			}, t.dispose = function () {
				clearTimeout(this._timeout), Ct.removeData(this.element, this.constructor.DATA_KEY), Ct(this.element).off(this.constructor.EVENT_KEY), Ct(this.element).closest(".modal").off("hide.bs.modal"), this.tip && Ct(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, (this._activeTrigger = null) !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
			}, t.show = function () {
				var e = this;
				if ("none" === Ct(this.element).css("display")) throw new Error("Please use show on visible elements");
				var t = Ct.Event(this.constructor.Event.SHOW);
				if (this.isWithContent() && this._isEnabled) {
					Ct(this.element).trigger(t);
					var i = Ct.contains(this.element.ownerDocument.documentElement, this.element);
					if (t.isDefaultPrevented() || !i) return;
					var o = this.getTipElement(),
						n = Ee.getUID(this.constructor.NAME);
					o.setAttribute("id", n), this.element.setAttribute("aria-describedby", n), this.setContent(), this.config.animation && Ct(o).addClass(Mt);
					var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
						r = this._getAttachment(s);
					this.addAttachmentClass(r);
					var a = !1 === this.config.container ? document.body : Ct(this.config.container);
					Ct(o).data(this.constructor.DATA_KEY, this), Ct.contains(this.element.ownerDocument.documentElement, this.tip) || Ct(o).appendTo(a), Ct(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new d(this.element, o, {
						placement: r,
						modifiers: {
							offset: {
								offset: this.config.offset
							},
							flip: {
								behavior: this.config.fallbackPlacement
							},
							arrow: {
								element: ".arrow"
							},
							preventOverflow: {
								boundariesElement: this.config.boundary
							}
						},
						onCreate: function (t) {
							t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t)
						},
						onUpdate: function (t) {
							e._handlePopperPlacementChange(t)
						}
					}), Ct(o).addClass(zt), "ontouchstart" in document.documentElement && Ct(document.body).children().on("mouseover", null, Ct.noop);
					var l = function () {
						e.config.animation && e._fixTransition();
						var t = e._hoverState;
						e._hoverState = null, Ct(e.element).trigger(e.constructor.Event.SHOWN), "out" === t && e._leave(null, e)
					};
					if (Ct(this.tip).hasClass(Mt)) {
						var c = Ee.getTransitionDurationFromElement(this.tip);
						Ct(this.tip).one(Ee.TRANSITION_END, l).emulateTransitionEnd(c)
					} else l()
				}
			}, t.hide = function (t) {
				var e = this,
					i = this.getTipElement(),
					o = Ct.Event(this.constructor.Event.HIDE),
					n = function () {
						e._hoverState !== Pt && i.parentNode && i.parentNode.removeChild(i), e._cleanTipClass(), e.element.removeAttribute("aria-describedby"), Ct(e.element).trigger(e.constructor.Event.HIDDEN), null !== e._popper && e._popper.destroy(), t && t()
					};
				if (Ct(this.element).trigger(o), !o.isDefaultPrevented()) {
					if (Ct(i).removeClass(zt), "ontouchstart" in document.documentElement && Ct(document.body).children().off("mouseover", null, Ct.noop), this._activeTrigger.click = !1, this._activeTrigger[qt] = !1, this._activeTrigger[Rt] = !1, Ct(this.tip).hasClass(Mt)) {
						var s = Ee.getTransitionDurationFromElement(i);
						Ct(i).one(Ee.TRANSITION_END, n).emulateTransitionEnd(s)
					} else n();
					this._hoverState = ""
				}
			}, t.update = function () {
				null !== this._popper && this._popper.scheduleUpdate()
			}, t.isWithContent = function () {
				return Boolean(this.getTitle())
			}, t.addAttachmentClass = function (t) {
				Ct(this.getTipElement()).addClass(It + "-" + t)
			}, t.getTipElement = function () {
				return this.tip = this.tip || Ct(this.config.template)[0], this.tip
			}, t.setContent = function () {
				var t = Ct(this.getTipElement());
				this.setElementContent(t.find(".tooltip-inner"), this.getTitle()), t.removeClass(Mt + " " + zt)
			}, t.setElementContent = function (t, e) {
				var i = this.config.html;
				"object" == typeof e && (e.nodeType || e.jquery) ? i ? Ct(e).parent().is(t) || t.empty().append(e) : t.text(Ct(e).text()) : t[i ? "html" : "text"](e)
			}, t.getTitle = function () {
				var t = this.element.getAttribute("data-original-title");
				return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t
			}, t._getAttachment = function (t) {
				return Lt[t.toUpperCase()]
			}, t._setListeners = function () {
				var o = this;
				this.config.trigger.split(" ").forEach(function (t) {
					if ("click" === t) Ct(o.element).on(o.constructor.Event.CLICK, o.config.selector, function (t) {
						return o.toggle(t)
					});
					else if ("manual" !== t) {
						var e = t === Rt ? o.constructor.Event.MOUSEENTER : o.constructor.Event.FOCUSIN,
							i = t === Rt ? o.constructor.Event.MOUSELEAVE : o.constructor.Event.FOCUSOUT;
						Ct(o.element).on(e, o.config.selector, function (t) {
							return o._enter(t)
						}).on(i, o.config.selector, function (t) {
							return o._leave(t)
						})
					}
					Ct(o.element).closest(".modal").on("hide.bs.modal", function () {
						return o.hide()
					})
				}), this.config.selector ? this.config = c({}, this.config, {
					trigger: "manual",
					selector: ""
				}) : this._fixTitle()
			}, t._fixTitle = function () {
				var t = typeof this.element.getAttribute("data-original-title");
				(this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
			}, t._enter = function (t, e) {
				var i = this.constructor.DATA_KEY;
				(e = e || Ct(t.currentTarget).data(i)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), Ct(t.currentTarget).data(i, e)), t && (e._activeTrigger["focusin" === t.type ? qt : Rt] = !0), Ct(e.getTipElement()).hasClass(zt) || e._hoverState === Pt ? e._hoverState = Pt : (clearTimeout(e._timeout), e._hoverState = Pt, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function () {
					e._hoverState === Pt && e.show()
				}, e.config.delay.show) : e.show())
			}, t._leave = function (t, e) {
				var i = this.constructor.DATA_KEY;
				(e = e || Ct(t.currentTarget).data(i)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), Ct(t.currentTarget).data(i, e)), t && (e._activeTrigger["focusout" === t.type ? qt : Rt] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = "out", e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function () {
					"out" === e._hoverState && e.hide()
				}, e.config.delay.hide) : e.hide())
			}, t._isWithActiveTrigger = function () {
				for (var t in this._activeTrigger)
					if (this._activeTrigger[t]) return !0;
				return !1
			}, t._getConfig = function (t) {
				return "number" == typeof (t = c({}, this.constructor.Default, Ct(this.element).data(), "object" == typeof t && t ? t : {})).delay && (t.delay = {
					show: t.delay,
					hide: t.delay
				}), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), Ee.typeCheckConfig(St, t, this.constructor.DefaultType), t
			}, t._getDelegateConfig = function () {
				var t = {};
				if (this.config)
					for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
				return t
			}, t._cleanTipClass = function () {
				var t = Ct(this.getTipElement()),
					e = t.attr("class").match(Ot);
				null !== e && 0 < e.length && t.removeClass(e.join(""))
			}, t._handlePopperPlacementChange = function (t) {
				this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement))
			}, t._fixTransition = function () {
				var t = this.getTipElement(),
					e = this.config.animation;
				null === t.getAttribute("x-placement") && (Ct(t).removeClass(Mt), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e)
			}, o._jQueryInterface = function (i) {
				return this.each(function () {
					var t = Ct(this).data(At),
						e = "object" == typeof i && i;
					if ((t || !/dispose|hide/.test(i)) && (t || (t = new o(this, e), Ct(this).data(At, t)), "string" == typeof i)) {
						if (void 0 === t[i]) throw new TypeError('No method named "' + i + '"');
						t[i]()
					}
				})
			}, r(o, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return jt
				}
			}, {
				key: "NAME",
				get: function () {
					return St
				}
			}, {
				key: "DATA_KEY",
				get: function () {
					return At
				}
			}, {
				key: "Event",
				get: function () {
					return Ht
				}
			}, {
				key: "EVENT_KEY",
				get: function () {
					return $t
				}
			}, {
				key: "DefaultType",
				get: function () {
					return Nt
				}
			}]), o
		}(), Ct.fn[St] = Bt._jQueryInterface, Ct.fn[St].Constructor = Bt, Ct.fn[St].noConflict = function () {
			return Ct.fn[St] = Dt, Bt._jQueryInterface
		}, Bt),
		Oe = (Ft = "popover", Vt = "." + (Ut = "bs.popover"), Qt = (Wt = e).fn[Ft], Yt = "bs-popover", Gt = new RegExp("(^|\\s)" + Yt + "\\S+", "g"), Kt = c({}, Ie.Default, {
			placement: "right",
			trigger: "click",
			content: "",
			template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
		}), Xt = c({}, Ie.DefaultType, {
			content: "(string|element|function)"
		}), "fade", ".popover-header", ".popover-body", Jt = {
			HIDE: "hide" + Vt,
			HIDDEN: "hidden" + Vt,
			SHOW: "show" + Vt,
			SHOWN: "shown" + Vt,
			INSERTED: "inserted" + Vt,
			CLICK: "click" + Vt,
			FOCUSIN: "focusin" + Vt,
			FOCUSOUT: "focusout" + Vt,
			MOUSEENTER: "mouseenter" + Vt,
			MOUSELEAVE: "mouseleave" + Vt
		}, Zt = function (t) {
			var e, i;

			function o() {
				return t.apply(this, arguments) || this
			}
			i = t, (e = o).prototype = Object.create(i.prototype), (e.prototype.constructor = e).__proto__ = i;
			var n = o.prototype;
			return n.isWithContent = function () {
				return this.getTitle() || this._getContent()
			}, n.addAttachmentClass = function (t) {
				Wt(this.getTipElement()).addClass(Yt + "-" + t)
			}, n.getTipElement = function () {
				return this.tip = this.tip || Wt(this.config.template)[0], this.tip
			}, n.setContent = function () {
				var t = Wt(this.getTipElement());
				this.setElementContent(t.find(".popover-header"), this.getTitle());
				var e = this._getContent();
				"function" == typeof e && (e = e.call(this.element)), this.setElementContent(t.find(".popover-body"), e), t.removeClass("fade show")
			}, n._getContent = function () {
				return this.element.getAttribute("data-content") || this.config.content
			}, n._cleanTipClass = function () {
				var t = Wt(this.getTipElement()),
					e = t.attr("class").match(Gt);
				null !== e && 0 < e.length && t.removeClass(e.join(""))
			}, o._jQueryInterface = function (i) {
				return this.each(function () {
					var t = Wt(this).data(Ut),
						e = "object" == typeof i ? i : null;
					if ((t || !/destroy|hide/.test(i)) && (t || (t = new o(this, e), Wt(this).data(Ut, t)), "string" == typeof i)) {
						if (void 0 === t[i]) throw new TypeError('No method named "' + i + '"');
						t[i]()
					}
				})
			}, r(o, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return Kt
				}
			}, {
				key: "NAME",
				get: function () {
					return Ft
				}
			}, {
				key: "DATA_KEY",
				get: function () {
					return Ut
				}
			}, {
				key: "Event",
				get: function () {
					return Jt
				}
			}, {
				key: "EVENT_KEY",
				get: function () {
					return Vt
				}
			}, {
				key: "DefaultType",
				get: function () {
					return Xt
				}
			}]), o
		}(Ie), Wt.fn[Ft] = Zt._jQueryInterface, Wt.fn[Ft].Constructor = Zt, Wt.fn[Ft].noConflict = function () {
			return Wt.fn[Ft] = Qt, Zt._jQueryInterface
		}, Zt),
		Ne = (ee = "scrollspy", oe = "." + (ie = "bs.scrollspy"), ne = (te = e).fn[ee], se = {
			offset: 10,
			method: "auto",
			target: ""
		}, re = {
			offset: "number",
			method: "string",
			target: "(string|element)"
		}, ae = {
			ACTIVATE: "activate" + oe,
			SCROLL: "scroll" + oe,
			LOAD_DATA_API: "load" + oe + ".data-api"
		}, "dropdown-item", le = "active", ce = {
			DATA_SPY: '[data-spy="scroll"]',
			ACTIVE: ".active",
			NAV_LIST_GROUP: ".nav, .list-group",
			NAV_LINKS: ".nav-link",
			NAV_ITEMS: ".nav-item",
			LIST_ITEMS: ".list-group-item",
			DROPDOWN: ".dropdown",
			DROPDOWN_ITEMS: ".dropdown-item",
			DROPDOWN_TOGGLE: ".dropdown-toggle"
		}, "offset", de = "position", ue = function () {
			function i(t, e) {
				var i = this;
				this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(e), this._selector = this._config.target + " " + ce.NAV_LINKS + "," + this._config.target + " " + ce.LIST_ITEMS + "," + this._config.target + " " + ce.DROPDOWN_ITEMS, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, te(this._scrollElement).on(ae.SCROLL, function (t) {
					return i._process(t)
				}), this.refresh(), this._process()
			}
			var t = i.prototype;
			return t.refresh = function () {
				var e = this,
					t = this._scrollElement === this._scrollElement.window ? "offset" : de,
					n = "auto" === this._config.method ? t : this._config.method,
					s = n === de ? this._getScrollTop() : 0;
				this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), te.makeArray(te(this._selector)).map(function (t) {
					var e, i = Ee.getSelectorFromElement(t);
					if (i && (e = te(i)[0]), e) {
						var o = e.getBoundingClientRect();
						if (o.width || o.height) return [te(e)[n]().top + s, i]
					}
					return null
				}).filter(function (t) {
					return t
				}).sort(function (t, e) {
					return t[0] - e[0]
				}).forEach(function (t) {
					e._offsets.push(t[0]), e._targets.push(t[1])
				})
			}, t.dispose = function () {
				te.removeData(this._element, ie), te(this._scrollElement).off(oe), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
			}, t._getConfig = function (t) {
				if ("string" != typeof (t = c({}, se, "object" == typeof t && t ? t : {})).target) {
					var e = te(t.target).attr("id");
					e || (e = Ee.getUID(ee), te(t.target).attr("id", e)), t.target = "#" + e
				}
				return Ee.typeCheckConfig(ee, t, re), t
			}, t._getScrollTop = function () {
				return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
			}, t._getScrollHeight = function () {
				return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
			}, t._getOffsetHeight = function () {
				return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
			}, t._process = function () {
				var t = this._getScrollTop() + this._config.offset,
					e = this._getScrollHeight(),
					i = this._config.offset + e - this._getOffsetHeight();
				if (this._scrollHeight !== e && this.refresh(), i <= t) {
					var o = this._targets[this._targets.length - 1];
					this._activeTarget !== o && this._activate(o)
				} else {
					if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();
					for (var n = this._offsets.length; n--;) this._activeTarget !== this._targets[n] && t >= this._offsets[n] && (void 0 === this._offsets[n + 1] || t < this._offsets[n + 1]) && this._activate(this._targets[n])
				}
			}, t._activate = function (e) {
				this._activeTarget = e, this._clear();
				var t = this._selector.split(",");
				t = t.map(function (t) {
					return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
				});
				var i = te(t.join(","));
				i.hasClass("dropdown-item") ? (i.closest(ce.DROPDOWN).find(ce.DROPDOWN_TOGGLE).addClass(le), i.addClass(le)) : (i.addClass(le), i.parents(ce.NAV_LIST_GROUP).prev(ce.NAV_LINKS + ", " + ce.LIST_ITEMS).addClass(le), i.parents(ce.NAV_LIST_GROUP).prev(ce.NAV_ITEMS).children(ce.NAV_LINKS).addClass(le)), te(this._scrollElement).trigger(ae.ACTIVATE, {
					relatedTarget: e
				})
			}, t._clear = function () {
				te(this._selector).filter(ce.ACTIVE).removeClass(le)
			}, i._jQueryInterface = function (e) {
				return this.each(function () {
					var t = te(this).data(ie);
					if (t || (t = new i(this, "object" == typeof e && e), te(this).data(ie, t)), "string" == typeof e) {
						if (void 0 === t[e]) throw new TypeError('No method named "' + e + '"');
						t[e]()
					}
				})
			}, r(i, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}, {
				key: "Default",
				get: function () {
					return se
				}
			}]), i
		}(), te(window).on(ae.LOAD_DATA_API, function () {
			for (var t = te.makeArray(te(ce.DATA_SPY)), e = t.length; e--;) {
				var i = te(t[e]);
				ue._jQueryInterface.call(i, i.data())
			}
		}), te.fn[ee] = ue._jQueryInterface, te.fn[ee].Constructor = ue, te.fn[ee].noConflict = function () {
			return te.fn[ee] = ne, ue._jQueryInterface
		}, ue),
		Le = (fe = "." + (he = "bs.tab"), me = (pe = e).fn.tab, ge = {
			HIDE: "hide" + fe,
			HIDDEN: "hidden" + fe,
			SHOW: "show" + fe,
			SHOWN: "shown" + fe,
			CLICK_DATA_API: "click" + fe + ".data-api"
		}, "dropdown-menu", ve = "active", "disabled", "fade", "show", ".dropdown", ".nav, .list-group", be = ".active", ye = "> li > .active", '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', ".dropdown-toggle", "> .dropdown-menu .active", we = function () {
			function o(t) {
				this._element = t
			}
			var t = o.prototype;
			return t.show = function () {
				var i = this;
				if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && pe(this._element).hasClass(ve) || pe(this._element).hasClass("disabled"))) {
					var t, o, e = pe(this._element).closest(".nav, .list-group")[0],
						n = Ee.getSelectorFromElement(this._element);
					if (e) {
						var s = "UL" === e.nodeName ? ye : be;
						o = (o = pe.makeArray(pe(e).find(s)))[o.length - 1]
					}
					var r = pe.Event(ge.HIDE, {
							relatedTarget: this._element
						}),
						a = pe.Event(ge.SHOW, {
							relatedTarget: o
						});
					if (o && pe(o).trigger(r), pe(this._element).trigger(a), !a.isDefaultPrevented() && !r.isDefaultPrevented()) {
						n && (t = pe(n)[0]), this._activate(this._element, e);
						var l = function () {
							var t = pe.Event(ge.HIDDEN, {
									relatedTarget: i._element
								}),
								e = pe.Event(ge.SHOWN, {
									relatedTarget: o
								});
							pe(o).trigger(t), pe(i._element).trigger(e)
						};
						t ? this._activate(t, t.parentNode, l) : l()
					}
				}
			}, t.dispose = function () {
				pe.removeData(this._element, he), this._element = null
			}, t._activate = function (t, e, i) {
				var o = this,
					n = ("UL" === e.nodeName ? pe(e).find(ye) : pe(e).children(be))[0],
					s = i && n && pe(n).hasClass("fade"),
					r = function () {
						return o._transitionComplete(t, n, i)
					};
				if (n && s) {
					var a = Ee.getTransitionDurationFromElement(n);
					pe(n).one(Ee.TRANSITION_END, r).emulateTransitionEnd(a)
				} else r()
			}, t._transitionComplete = function (t, e, i) {
				if (e) {
					pe(e).removeClass("show " + ve);
					var o = pe(e.parentNode).find("> .dropdown-menu .active")[0];
					o && pe(o).removeClass(ve), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1)
				}
				if (pe(t).addClass(ve), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), Ee.reflow(t), pe(t).addClass("show"), t.parentNode && pe(t.parentNode).hasClass("dropdown-menu")) {
					var n = pe(t).closest(".dropdown")[0];
					n && pe(n).find(".dropdown-toggle").addClass(ve), t.setAttribute("aria-expanded", !0)
				}
				i && i()
			}, o._jQueryInterface = function (i) {
				return this.each(function () {
					var t = pe(this),
						e = t.data(he);
					if (e || (e = new o(this), t.data(he, e)), "string" == typeof i) {
						if (void 0 === e[i]) throw new TypeError('No method named "' + i + '"');
						e[i]()
					}
				})
			}, r(o, null, [{
				key: "VERSION",
				get: function () {
					return "4.1.1"
				}
			}]), o
		}(), pe(document).on(ge.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', function (t) {
			t.preventDefault(), we._jQueryInterface.call(pe(this), "show")
		}), pe.fn.tab = we._jQueryInterface, pe.fn.tab.Constructor = we, pe.fn.tab.noConflict = function () {
			return pe.fn.tab = me, we._jQueryInterface
		}, we);
	! function (t) {
		if (void 0 === t) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
		var e = t.fn.jquery.split(" ")[0].split(".");
		if (e[0] < 2 && e[1] < 9 || 1 === e[0] && 9 === e[1] && e[2] < 1 || 4 <= e[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
	}(e), t.Util = Ee, t.Alert = _e, t.Button = Ce, t.Carousel = Se, t.Collapse = Ae, t.Dropdown = $e, t.Modal = De, t.Popover = Oe, t.Scrollspy = Ne, t.Tab = Le, t.Tooltip = Ie, Object.defineProperty(t, "__esModule", {
		value: !0
	})
}),
function (t) {
	var e, n, i, o, s, r, a, l = navigator.userAgent;
	t.HTMLPictureElement && /ecko/.test(l) && l.match(/rv\:(\d+)/) && RegExp.$1 < 45 && addEventListener("resize", (n = document.createElement("source"), i = function (t) {
		var e, i, o = t.parentNode;
		"PICTURE" === o.nodeName.toUpperCase() ? (e = n.cloneNode(), o.insertBefore(e, o.firstElementChild), setTimeout(function () {
			o.removeChild(e)
		})) : (!t._pfLastSize || t.offsetWidth > t._pfLastSize) && (t._pfLastSize = t.offsetWidth, i = t.sizes, t.sizes += ",100vw", setTimeout(function () {
			t.sizes = i
		}))
	}, o = function () {
		var t, e = document.querySelectorAll("picture > img, img[srcset][sizes]");
		for (t = 0; t < e.length; t++) i(e[t])
	}, s = function () {
		clearTimeout(e), e = setTimeout(o, 99)
	}, r = t.matchMedia && matchMedia("(orientation: landscape)"), a = function () {
		s(), r && r.addListener && r.addListener(s)
	}, n.srcset = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==", /^[c|i]|d$/.test(document.readyState || "") ? a() : document.addEventListener("DOMContentLoaded", a), s))
}(window),
function (t, s, c) {
	"use strict";

	function d(t) {
		return " " === t || "\t" === t || "\n" === t || "\f" === t || "\r" === t
	}

	function E(t, e) {
		return t.res - e.res
	}

	function _(t, e) {
		var i, o, n;
		if (t && e)
			for (n = A.parseSet(e), t = A.makeUrl(t), i = 0; i < n.length; i++)
				if (t === A.makeUrl(n[i].url)) {
					o = n[i];
					break
				}
		return o
	}
	s.createElement("picture");
	var n, u, l, o, r, e, i, a, p, h, f, m, g, v, b, y, w, x, T, k, C, S, A = {},
		$ = !1,
		D = function () {},
		I = s.createElement("img"),
		O = I.getAttribute,
		N = I.setAttribute,
		L = I.removeAttribute,
		j = s.documentElement,
		P = {},
		H = {
			algorithm: ""
		},
		M = "data-pfsrc",
		z = M + "set",
		R = navigator.userAgent,
		q = /rident/.test(R) || /ecko/.test(R) && R.match(/rv\:(\d+)/) && 35 < RegExp.$1,
		B = "currentSrc",
		W = /\s+\+?\d+(e\d+)?w/,
		F = /(\([^)]+\))?\s*(.+)/,
		U = t.picturefillCFG,
		V = "font-size:100%!important;",
		Q = !0,
		Y = {},
		G = {},
		K = t.devicePixelRatio,
		X = {
			px: 1,
			in: 96
		},
		J = s.createElement("a"),
		Z = !1,
		tt = /^[ \t\n\r\u000c]+/,
		et = /^[, \t\n\r\u000c]+/,
		it = /^[^ \t\n\r\u000c]+/,
		ot = /[,]+$/,
		nt = /^\d+$/,
		st = /^-?(?:[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/,
		rt = function (t, e, i, o) {
			t.addEventListener ? t.addEventListener(e, i, o || !1) : t.attachEvent && t.attachEvent("on" + e, i)
		},
		at = function (e) {
			var i = {};
			return function (t) {
				return t in i || (i[t] = e(t)), i[t]
			}
		},
		lt = (o = /^([\d\.]+)(em|vw|px)$/, r = at(function (t) {
			return "return " + function () {
				for (var t = arguments, e = 0, i = t[0]; ++e in t;) i = i.replace(t[e], t[++e]);
				return i
			}((t || "").toLowerCase(), /\band\b/g, "&&", /,/g, "||", /min-([a-z-\s]+):/g, "e.$1>=", /max-([a-z-\s]+):/g, "e.$1<=", /calc([^)]+)/g, "($1)", /(\d+[\.]*[\d]*)([a-z]+)/g, "($1 * e.$2)", /^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/gi, "") + ";"
		}), function (t, e) {
			var i;
			if (!(t in Y))
				if (Y[t] = !1, e && (i = t.match(o))) Y[t] = i[1] * X[i[2]];
				else try {
					Y[t] = new Function("e", r(t))(X)
				} catch (t) {}
			return Y[t]
		}),
		ct = function (t, e) {
			return t.w ? (t.cWidth = A.calcListLength(e || "100vw"), t.res = t.w / t.cWidth) : t.res = t.d, t
		},
		dt = function (t) {
			if ($) {
				var e, i, o, n = t || {};
				if (n.elements && 1 === n.elements.nodeType && ("IMG" === n.elements.nodeName.toUpperCase() ? n.elements = [n.elements] : (n.context = n.elements, n.elements = null)), o = (e = n.elements || A.qsa(n.context || s, n.reevaluate || n.reselect ? A.sel : A.selShort)).length) {
					for (A.setupRun(n), Z = !0, i = 0; i < o; i++) A.fillImg(e[i], n);
					A.teardownRun(n)
				}
			}
		};
	t.console && console.warn, B in I || (B = "src"), P["image/jpeg"] = !0, P["image/gif"] = !0, P["image/png"] = !0, P["image/svg+xml"] = s.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1"), A.ns = ("pf" + (new Date).getTime()).substr(0, 9), A.supSrcset = "srcset" in I, A.supSizes = "sizes" in I, A.supPicture = !!t.HTMLPictureElement, A.supSrcset && A.supPicture && !A.supSizes && (T = s.createElement("img"), I.srcset = "data:,a", T.src = "data:,a", A.supSrcset = I.complete === T.complete, A.supPicture = A.supSrcset && A.supPicture), A.supSrcset && !A.supSizes ? (y = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==", w = s.createElement("img"), x = function () {
		2 === w.width && (A.supSizes = !0), u = A.supSrcset && !A.supSizes, $ = !0, setTimeout(dt)
	}, w.onload = x, w.onerror = x, w.setAttribute("sizes", "9px"), w.srcset = y + " 1w,data:image/gif;base64,R0lGODlhAgABAPAAAP///wAAACH5BAAAAAAALAAAAAACAAEAAAICBAoAOw== 9w", w.src = y) : $ = !0, A.selShort = "picture>img,img[srcset]", A.sel = A.selShort, A.cfg = H, A.DPR = K || 1, A.u = X, A.types = P, A.setSize = D, A.makeUrl = at(function (t) {
		return J.href = t, J.href
	}), A.qsa = function (t, e) {
		return "querySelector" in t ? t.querySelectorAll(e) : []
	}, A.matchesMedia = function () {
		return t.matchMedia && (matchMedia("(min-width: 0.1em)") || {}).matches ? A.matchesMedia = function (t) {
			return !t || matchMedia(t).matches
		} : A.matchesMedia = A.mMQ, A.matchesMedia.apply(this, arguments)
	}, A.mMQ = function (t) {
		return !t || lt(t)
	}, A.calcLength = function (t) {
		var e = lt(t, !0) || !1;
		return e < 0 && (e = !1), e
	}, A.supportsType = function (t) {
		return !t || P[t]
	}, A.parseSize = at(function (t) {
		var e = (t || "").match(F);
		return {
			media: e && e[1],
			length: e && e[2]
		}
	}), A.parseSet = function (t) {
		return t.cands || (t.cands = function (o, u) {
			function t(t) {
				var e, i = t.exec(o.substring(l));
				return i ? (e = i[0], l += e.length, e) : void 0
			}

			function e() {
				var t, e, i, o, n, s, r, a, l, c = !1,
					d = {};
				for (o = 0; o < h.length; o++) s = (n = h[o])[n.length - 1], r = n.substring(0, n.length - 1), a = parseInt(r, 10), l = parseFloat(r), nt.test(r) && "w" === s ? ((t || e) && (c = !0), 0 === a ? c = !0 : t = a) : st.test(r) && "x" === s ? ((t || e || i) && (c = !0), l < 0 ? c = !0 : e = l) : nt.test(r) && "h" === s ? ((i || e) && (c = !0), 0 === a ? c = !0 : i = a) : c = !0;
				c || (d.url = p, t && (d.w = t), e && (d.d = e), i && (d.h = i), i || e || t || (d.d = 1), 1 === d.d && (u.has1x = !0), d.set = u, f.push(d))
			}

			function i() {
				for (t(tt), n = "", s = "in descriptor";;) {
					if (r = o.charAt(l), "in descriptor" === s)
						if (d(r)) n && (h.push(n), n = "", s = "after descriptor");
						else {
							if ("," === r) return l += 1, n && h.push(n), void e();
							if ("(" === r) n += r, s = "in parens";
							else {
								if ("" === r) return n && h.push(n), void e();
								n += r
							}
						}
					else if ("in parens" === s)
						if (")" === r) n += r, s = "in descriptor";
						else {
							if ("" === r) return h.push(n), void e();
							n += r
						}
					else if ("after descriptor" === s)
						if (d(r));
						else {
							if ("" === r) return void e();
							s = "in descriptor", l -= 1
						}
					l += 1
				}
			}
			for (var p, h, n, s, r, a = o.length, l = 0, f = [];;) {
				if (t(et), a <= l) return f;
				p = t(it), h = [], "," === p.slice(-1) ? (p = p.replace(ot, ""), e()) : i()
			}
		}(t.srcset, t)), t.cands
	}, A.getEmValue = function () {
		var t;
		if (!n && (t = s.body)) {
			var e = s.createElement("div"),
				i = j.style.cssText,
				o = t.style.cssText;
			e.style.cssText = "position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)", j.style.cssText = V, t.style.cssText = V, t.appendChild(e), n = e.offsetWidth, t.removeChild(e), n = parseFloat(n, 10), j.style.cssText = i, t.style.cssText = o
		}
		return n || 16
	}, A.calcListLength = function (t) {
		if (!(t in G) || H.uT) {
			var e = A.calcLength(function (t) {
				var e, i, o, n, s, r, a, l = /^(?:[+-]?[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?(?:ch|cm|em|ex|in|mm|pc|pt|px|rem|vh|vmin|vmax|vw)$/i,
					c = /^calc\((?:[0-9a-z \.\+\-\*\/\(\)]+)\)$/i;
				for (o = (i = function (t) {
						function e() {
							n && (s.push(n), n = "")
						}

						function i() {
							s[0] && (r.push(s), s = [])
						}
						for (var o, n = "", s = [], r = [], a = 0, l = 0, c = !1;;) {
							if ("" === (o = t.charAt(l))) return e(), i(), r;
							if (c) {
								if ("*" === o && "/" === t[l + 1]) {
									c = !1, l += 2, e();
									continue
								}
								l += 1
							} else {
								if (d(o)) {
									if (t.charAt(l - 1) && d(t.charAt(l - 1)) || !n) {
										l += 1;
										continue
									}
									if (0 === a) {
										e(), l += 1;
										continue
									}
									o = " "
								} else if ("(" === o) a += 1;
								else if (")" === o) a -= 1;
								else {
									if ("," === o) {
										e(), i(), l += 1;
										continue
									}
									if ("/" === o && "*" === t.charAt(l + 1)) {
										c = !0, l += 2;
										continue
									}
								}
								n += o, l += 1
							}
						}
					}(t)).length, e = 0; e < o; e++)
					if (s = (n = i[e])[n.length - 1], a = s, l.test(a) && 0 <= parseFloat(a) || c.test(a) || "0" === a || "-0" === a || "+0" === a) {
						if (r = s, n.pop(), 0 === n.length) return r;
						if (n = n.join(" "), A.matchesMedia(n)) return r
					}
				return "100vw"
			}(t));
			G[t] = e || X.width
		}
		return G[t]
	}, A.setRes = function (t) {
		var e;
		if (t)
			for (var i = 0, o = (e = A.parseSet(t)).length; i < o; i++) ct(e[i], t.sizes);
		return e
	}, A.setRes.res = ct, A.applySetCandidate = function (t, e) {
		if (t.length) {
			var i, o, n, s, r, a, l, c, d, u = e[A.ns],
				p = A.DPR;
			if (a = u.curSrc || e[B], (l = u.curCan || (w = e, x = a, !(T = t[0].set) && x && (T = (T = w[A.ns].sets) && T[T.length - 1]), (k = _(x, T)) && (x = A.makeUrl(x), w[A.ns].curSrc = x, (w[A.ns].curCan = k).res || ct(k, k.set.sizes)), k)) && l.set === t[0].set && ((d = q && !e.complete && l.res - .1 > p) || (l.cached = !0, l.res >= p && (r = l))), !r)
				for (t.sort(E), r = t[(s = t.length) - 1], o = 0; o < s; o++)
					if ((i = t[o]).res >= p) {
						r = t[n = o - 1] && (d || a !== A.makeUrl(i.url)) && (h = t[n].res, f = i.res, m = p, g = t[n].cached, y = b = v = void 0, "saveData" === H.algorithm ? 2.7 < h ? y = m + 1 : (b = (f - m) * (v = Math.pow(h - .6, 1.5)), g && (b += .1 * v), y = h + b) : y = 1 < m ? Math.sqrt(h * f) : h, m < y) ? t[n] : i;
						break
					}
			r && (c = A.makeUrl(r.url), u.curSrc = c, u.curCan = r, c !== a && A.setSrc(e, r), A.setSize(e))
		}
		var h, f, m, g, v, b, y, w, x, T, k
	}, A.setSrc = function (t, e) {
		var i;
		t.src = e.url, "image/svg+xml" === e.set.type && (i = t.style.width, t.style.width = t.offsetWidth + 1 + "px", t.offsetWidth + 1 && (t.style.width = i))
	}, A.getSet = function (t) {
		var e, i, o, n = !1,
			s = t[A.ns].sets;
		for (e = 0; e < s.length && !n; e++)
			if ((i = s[e]).srcset && A.matchesMedia(i.media) && (o = A.supportsType(i.type))) {
				"pending" === o && (i = o), n = i;
				break
			}
		return n
	}, A.parseSets = function (t, e, i) {
		var o, n, s, r, a = e && "PICTURE" === e.nodeName.toUpperCase(),
			l = t[A.ns];
		(l.src === c || i.src) && (l.src = O.call(t, "src"), l.src ? N.call(t, M, l.src) : L.call(t, M)), (l.srcset === c || i.srcset || !A.supSrcset || t.srcset) && (o = O.call(t, "srcset"), l.srcset = o, r = !0), l.sets = [], a && (l.pic = !0, function (t, e) {
			var i, o, n, s, r = t.getElementsByTagName("source");
			for (i = 0, o = r.length; i < o; i++)(n = r[i])[A.ns] = !0, (s = n.getAttribute("srcset")) && e.push({
				srcset: s,
				media: n.getAttribute("media"),
				type: n.getAttribute("type"),
				sizes: n.getAttribute("sizes")
			})
		}(e, l.sets)), l.srcset ? (n = {
			srcset: l.srcset,
			sizes: O.call(t, "sizes")
		}, l.sets.push(n), (s = (u || l.src) && W.test(l.srcset || "")) || !l.src || _(l.src, n) || n.has1x || (n.srcset += ", " + l.src, n.cands.push({
			url: l.src,
			d: 1,
			set: n
		}))) : l.src && l.sets.push({
			srcset: l.src,
			sizes: null
		}), l.curCan = null, l.curSrc = c, l.supported = !(a || n && !A.supSrcset || s && !A.supSizes), r && A.supSrcset && !l.supported && (o ? (N.call(t, z, o), t.srcset = "") : L.call(t, z)), l.supported && !l.srcset && (!l.src && t.src || t.src !== A.makeUrl(l.src)) && (null === l.src ? t.removeAttribute("src") : t.src = l.src), l.parsed = !0
	}, A.fillImg = function (t, e) {
		var i, o, n, s, r, a = e.reselect || e.reevaluate;
		t[A.ns] || (t[A.ns] = {}), i = t[A.ns], (a || i.evaled !== l) && ((!i.parsed || e.reevaluate) && A.parseSets(t, t.parentNode, e), i.supported ? i.evaled = l : (o = t, s = A.getSet(o), r = !1, "pending" !== s && (r = l, s && (n = A.setRes(s), A.applySetCandidate(n, o))), o[A.ns].evaled = r))
	}, A.setupRun = function () {
		(!Z || Q || K !== t.devicePixelRatio) && (Q = !1, K = t.devicePixelRatio, Y = {}, G = {}, A.DPR = K || 1, X.width = Math.max(t.innerWidth || 0, j.clientWidth), X.height = Math.max(t.innerHeight || 0, j.clientHeight), X.vw = X.width / 100, X.vh = X.height / 100, l = [X.height, X.width, K].join("-"), X.em = A.getEmValue(), X.rem = X.em)
	}, A.supPicture ? (dt = D, A.fillImg = D) : (m = t.attachEvent ? /d$|^c/ : /d$|^c|^i/, g = function () {
		var t = s.readyState || "";
		v = setTimeout(g, "loading" === t ? 200 : 999), s.body && (A.fillImgs(), (e = e || m.test(t)) && clearTimeout(v))
	}, v = setTimeout(g, s.body ? 9 : 99), b = j.clientHeight, rt(t, "resize", (i = function () {
		Q = Math.max(t.innerWidth || 0, j.clientWidth) !== X.width || j.clientHeight !== b, b = j.clientHeight, Q && A.fillImgs()
	}, a = 99, f = function () {
		var t = new Date - h;
		t < a ? p = setTimeout(f, a - t) : (p = null, i())
	}, function () {
		h = new Date, p || (p = setTimeout(f, a))
	})), rt(s, "readystatechange", g)), A.picturefill = dt, A.fillImgs = dt, A.teardownRun = D, dt._ = A, t.picturefillCFG = {
		pf: A,
		push: function (t) {
			var e = t.shift();
			"function" == typeof A[e] ? A[e].apply(A, t) : (H[e] = t[0], Z && A.fillImgs({
				reselect: !0
			}))
		}
	};
	for (; U && U.length;) t.picturefillCFG.push(U.shift());
	t.picturefill = dt, "object" == typeof module && "object" == typeof module.exports ? module.exports = dt : "function" == typeof define && define.amd && define("picturefill", function () {
		return dt
	}), A.supPicture || (P["image/webp"] = (k = "image/webp", C = "data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA==", (S = new t.Image).onerror = function () {
		P[k] = !1, dt()
	}, S.onload = function () {
		P[k] = 1 === S.width, dt()
	}, S.src = C, "pending"))
}(window, document),
function (t, e) {
	"object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.Popper = e()
}(this, function () {
	"use strict";

	function r(t) {
		return t && "[object Function]" === {}.toString.call(t)
	}

	function b(t, e) {
		if (1 !== t.nodeType) return [];
		var i = window.getComputedStyle(t, null);
		return e ? i[e] : i
	}

	function y(t) {
		return "HTML" === t.nodeName ? t : t.parentNode || t.host
	}

	function w(t) {
		if (!t || -1 !== ["HTML", "BODY", "#document"].indexOf(t.nodeName)) return window.document.body;
		var e = b(t),
			i = e.overflow,
			o = e.overflowX,
			n = e.overflowY;
		return /(auto|scroll)/.test(i + n + o) ? t : w(y(t))
	}

	function x(t) {
		var e = t && t.offsetParent,
			i = e && e.nodeName;
		return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TD", "TABLE"].indexOf(e.nodeName) && "static" === b(e, "position") ? x(e) : e : window.document.documentElement
	}

	function d(t) {
		return null === t.parentNode ? t : d(t.parentNode)
	}

	function T(t, e) {
		if (!(t && t.nodeType && e && e.nodeType)) return window.document.documentElement;
		var i = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
			o = i ? t : e,
			n = i ? e : t,
			s = document.createRange();
		s.setStart(o, 0), s.setEnd(n, 0);
		var r, a, l = s.commonAncestorContainer;
		if (t !== l && e !== l || o.contains(n)) return "BODY" === (a = (r = l).nodeName) || "HTML" !== a && x(r.firstElementChild) !== r ? x(l) : l;
		var c = d(t);
		return c.host ? T(c.host, e) : T(t, d(e).host)
	}

	function k(t) {
		var e = "top" === (1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "top") ? "scrollTop" : "scrollLeft",
			i = t.nodeName;
		if ("BODY" === i || "HTML" === i) {
			var o = window.document.documentElement;
			return (window.document.scrollingElement || o)[e]
		}
		return t[e]
	}

	function u(t, e) {
		var i = "x" === e ? "Left" : "Top",
			o = "Left" == i ? "Right" : "Bottom";
		return +t["border" + i + "Width"].split("px")[0] + +t["border" + o + "Width"].split("px")[0]
	}

	function o(t, e, i, o) {
		return P(e["offset" + t], e["scroll" + t], i["client" + t], i["offset" + t], i["scroll" + t], B() ? i["offset" + t] + o["margin" + ("Height" === t ? "Top" : "Left")] + o["margin" + ("Height" === t ? "Bottom" : "Right")] : 0)
	}

	function E() {
		var t = window.document.body,
			e = window.document.documentElement,
			i = B() && window.getComputedStyle(e);
		return {
			height: o("Height", t, e, i),
			width: o("Width", t, e, i)
		}
	}

	function _(t) {
		return V({}, t, {
			right: t.left + t.width,
			bottom: t.top + t.height
		})
	}

	function C(t) {
		var e = {};
		if (B()) try {
			e = t.getBoundingClientRect();
			var i = k(t, "top"),
				o = k(t, "left");
			e.top += i, e.left += o, e.bottom += i, e.right += o
		} catch (t) {} else e = t.getBoundingClientRect();
		var n = {
				left: e.left,
				top: e.top,
				width: e.right - e.left,
				height: e.bottom - e.top
			},
			s = "HTML" === t.nodeName ? E() : {},
			r = s.width || t.clientWidth || n.right - n.left,
			a = s.height || t.clientHeight || n.bottom - n.top,
			l = t.offsetWidth - r,
			c = t.offsetHeight - a;
		if (l || c) {
			var d = b(t);
			l -= u(d, "x"), c -= u(d, "y"), n.width -= l, n.height -= c
		}
		return _(n)
	}

	function S(t, e) {
		var i = B(),
			o = "HTML" === e.nodeName,
			n = C(t),
			s = C(e),
			r = w(t),
			a = b(e),
			l = +a.borderTopWidth.split("px")[0],
			c = +a.borderLeftWidth.split("px")[0],
			d = _({
				top: n.top - s.top - l,
				left: n.left - s.left - c,
				width: n.width,
				height: n.height
			});
		if (d.marginTop = 0, d.marginLeft = 0, !i && o) {
			var u = +a.marginTop.split("px")[0],
				p = +a.marginLeft.split("px")[0];
			d.top -= l - u, d.bottom -= l - u, d.left -= c - p, d.right -= c - p, d.marginTop = u, d.marginLeft = p
		}
		return (i ? e.contains(r) : e === r && "BODY" !== r.nodeName) && (d = function (t, e) {
			var i = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
				o = k(e, "top"),
				n = k(e, "left"),
				s = i ? -1 : 1;
			return t.top += o * s, t.bottom += o * s, t.left += n * s, t.right += n * s, t
		}(d, e)), d
	}

	function p(t, e, i, o) {
		var n, s, r, a, l, c, d, u = {
				top: 0,
				left: 0
			},
			p = T(t, e);
		if ("viewport" === o) n = p, s = window.document.documentElement, r = S(n, s), a = P(s.clientWidth, window.innerWidth || 0), l = P(s.clientHeight, window.innerHeight || 0), c = k(s), d = k(s, "left"), u = _({
			top: c - r.top + r.marginTop,
			left: d - r.left + r.marginLeft,
			width: a,
			height: l
		});
		else {
			var h;
			"scrollParent" === o ? "BODY" === (h = w(y(t))).nodeName && (h = window.document.documentElement) : h = "window" === o ? window.document.documentElement : o;
			var f = S(h, p);
			if ("HTML" !== h.nodeName || function t(e) {
					var i = e.nodeName;
					return "BODY" !== i && "HTML" !== i && ("fixed" === b(e, "position") || t(y(e)))
				}(p)) u = f;
			else {
				var m = E(),
					g = m.height,
					v = m.width;
				u.top += f.top - f.marginTop, u.bottom = g + f.top, u.left += f.left - f.marginLeft, u.right = v + f.left
			}
		}
		return u.left += i, u.top += i, u.right -= i, u.bottom -= i, u
	}

	function a(t, e, o, i, n) {
		var s = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
		if (-1 === t.indexOf("auto")) return t;
		var r = p(o, i, s, n),
			a = {
				top: {
					width: r.width,
					height: e.top - r.top
				},
				right: {
					width: r.right - e.right,
					height: r.height
				},
				bottom: {
					width: r.width,
					height: r.bottom - e.bottom
				},
				left: {
					width: e.left - r.left,
					height: r.height
				}
			},
			l = Object.keys(a).map(function (t) {
				return V({
					key: t
				}, a[t], {
					area: (e = a[t], e.width * e.height)
				});
				var e
			}).sort(function (t, e) {
				return e.area - t.area
			}),
			c = l.filter(function (t) {
				var e = t.width,
					i = t.height;
				return e >= o.clientWidth && i >= o.clientHeight
			}),
			d = 0 < c.length ? c[0].key : l[0].key,
			u = t.split("-")[1];
		return d + (u ? "-" + u : "")
	}

	function l(t, e, i) {
		return S(i, T(e, i))
	}

	function v(t) {
		var e = window.getComputedStyle(t),
			i = parseFloat(e.marginTop) + parseFloat(e.marginBottom),
			o = parseFloat(e.marginLeft) + parseFloat(e.marginRight);
		return {
			width: t.offsetWidth + o,
			height: t.offsetHeight + i
		}
	}

	function A(t) {
		var e = {
			left: "right",
			right: "left",
			bottom: "top",
			top: "bottom"
		};
		return t.replace(/left|right|bottom|top/g, function (t) {
			return e[t]
		})
	}

	function $(t, e, i) {
		i = i.split("-")[0];
		var o = v(t),
			n = {
				width: o.width,
				height: o.height
			},
			s = -1 !== ["right", "left"].indexOf(i),
			r = s ? "top" : "left",
			a = s ? "left" : "top",
			l = s ? "height" : "width",
			c = s ? "width" : "height";
		return n[r] = e[r] + e[l] / 2 - o[l] / 2, n[a] = i === a ? e[a] - o[c] : e[A(a)], n
	}

	function D(t, e) {
		return Array.prototype.find ? t.find(e) : t.filter(e)[0]
	}

	function I(t, i, e) {
		return (void 0 === e ? t : t.slice(0, function (t, e, i) {
			if (Array.prototype.findIndex) return t.findIndex(function (t) {
				return t[e] === i
			});
			var o = D(t, function (t) {
				return t[e] === i
			});
			return t.indexOf(o)
		}(t, "name", e))).forEach(function (t) {
			t.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
			var e = t.function || t.fn;
			t.enabled && r(e) && (i.offsets.popper = _(i.offsets.popper), i.offsets.reference = _(i.offsets.reference), i = e(i, t))
		}), i
	}

	function t(t, i) {
		return t.some(function (t) {
			var e = t.name;
			return t.enabled && e === i
		})
	}

	function O(t) {
		for (var e = [!1, "ms", "Webkit", "Moz", "O"], i = t.charAt(0).toUpperCase() + t.slice(1), o = 0; o < e.length - 1; o++) {
			var n = e[o],
				s = n ? "" + n + i : t;
			if (void 0 !== window.document.body.style[s]) return s
		}
		return null
	}

	function e(t, e, i, o) {
		i.updateBound = o, window.addEventListener("resize", i.updateBound, {
			passive: !0
		});
		var n = w(t);
		return function t(e, i, o, n) {
			var s = "BODY" === e.nodeName,
				r = s ? window : e;
			r.addEventListener(i, o, {
				passive: !0
			}), s || t(w(r.parentNode), i, o, n), n.push(r)
		}(n, "scroll", i.updateBound, i.scrollParents), i.scrollElement = n, i.eventsEnabled = !0, i
	}

	function i() {
		var e;
		this.state.eventsEnabled && (window.cancelAnimationFrame(this.scheduleUpdate), this.state = (this.reference, e = this.state, window.removeEventListener("resize", e.updateBound), e.scrollParents.forEach(function (t) {
			t.removeEventListener("scroll", e.updateBound)
		}), e.updateBound = null, e.scrollParents = [], e.scrollElement = null, e.eventsEnabled = !1, e))
	}

	function h(t) {
		return "" !== t && !isNaN(parseFloat(t)) && isFinite(t)
	}

	function c(i, o) {
		Object.keys(o).forEach(function (t) {
			var e = ""; - 1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(t) && h(o[t]) && (e = "px"), i.style[t] = o[t] + e
		})
	}

	function N(t, e, i) {
		var o = D(t, function (t) {
				return t.name === e
			}),
			n = !!o && t.some(function (t) {
				return t.name === i && t.enabled && t.order < o.order
			});
		if (!n) {
			var s = "`" + e + "`";
			console.warn("`" + i + "` modifier is required by " + s + " modifier in order to work, be sure to include it before " + s + "!")
		}
		return n
	}

	function n(t) {
		var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
			i = Y.indexOf(t),
			o = Y.slice(i + 1).concat(Y.slice(0, i));
		return e ? o.reverse() : o
	}

	function f(t, n, s, e) {
		var r = [0, 0],
			a = -1 !== ["right", "left"].indexOf(e),
			i = t.split(/(\+|\-)/).map(function (t) {
				return t.trim()
			}),
			o = i.indexOf(D(i, function (t) {
				return -1 !== t.search(/,|\s/)
			}));
		i[o] && -1 === i[o].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
		var l = /\s*,\s*|\s+/,
			c = -1 === o ? [i] : [i.slice(0, o).concat([i[o].split(l)[0]]), [i[o].split(l)[1]].concat(i.slice(o + 1))];
		return (c = c.map(function (t, e) {
			var i = (1 === e ? !a : a) ? "height" : "width",
				o = !1;
			return t.reduce(function (t, e) {
				return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? (t[t.length - 1] = e, o = !0, t) : o ? (t[t.length - 1] += e, o = !1, t) : t.concat(e)
			}, []).map(function (t) {
				return function (t, e, i, o) {
					var n = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
						s = +n[1],
						r = n[2];
					if (!s) return t;
					if (0 === r.indexOf("%")) {
						var a;
						switch (r) {
							case "%p":
								a = i;
								break;
							case "%":
							case "%r":
							default:
								a = o
						}
						return _(a)[e] / 100 * s
					}
					return "vh" === r || "vw" === r ? ("vh" === r ? P(document.documentElement.clientHeight, window.innerHeight || 0) : P(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * s : s
				}(t, i, n, s)
			})
		})).forEach(function (i, o) {
			i.forEach(function (t, e) {
				h(t) && (r[o] += t * ("-" === i[e - 1] ? -1 : 1))
			})
		}), r
	}
	for (var L = Math.min, j = Math.floor, P = Math.max, s = ["native code", "[object MutationObserverConstructor]"], m = "undefined" != typeof window, g = ["Edge", "Trident", "Firefox"], H = 0, M = 0; M < g.length; M += 1)
		if (m && 0 <= navigator.userAgent.indexOf(g[M])) {
			H = 1;
			break
		}
	var z, R, q = m && (R = window.MutationObserver, s.some(function (t) {
			return -1 < (R || "").toString().indexOf(t)
		})) ? function (t) {
			var e = !1,
				i = 0,
				o = document.createElement("span");
			return new MutationObserver(function () {
					t(), e = !1
				}).observe(o, {
					attributes: !0
				}),
				function () {
					e || (e = !0, o.setAttribute("x-index", i), ++i)
				}
		} : function (t) {
			var e = !1;
			return function () {
				e || (e = !0, setTimeout(function () {
					e = !1, t()
				}, H))
			}
		},
		B = function () {
			return null == z && (z = -1 !== navigator.appVersion.indexOf("MSIE 10")), z
		},
		W = function (t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
		},
		F = function () {
			function o(t, e) {
				for (var i, o = 0; o < e.length; o++)(i = e[o]).enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
			}
			return function (t, e, i) {
				return e && o(t.prototype, e), i && o(t, i), t
			}
		}(),
		U = function (t, e, i) {
			return e in t ? Object.defineProperty(t, e, {
				value: i,
				enumerable: !0,
				configurable: !0,
				writable: !0
			}) : t[e] = i, t
		},
		V = Object.assign || function (t) {
			for (var e, i = 1; i < arguments.length; i++)
				for (var o in e = arguments[i]) Object.prototype.hasOwnProperty.call(e, o) && (t[o] = e[o]);
			return t
		},
		Q = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
		Y = Q.slice(3),
		G = "flip",
		K = "clockwise",
		X = "counterclockwise",
		J = function () {
			function s(t, e) {
				var i = this,
					o = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
				W(this, s), this.scheduleUpdate = function () {
					return requestAnimationFrame(i.update)
				}, this.update = q(this.update.bind(this)), this.options = V({}, s.Defaults, o), this.state = {
					isDestroyed: !1,
					isCreated: !1,
					scrollParents: []
				}, this.reference = t.jquery ? t[0] : t, this.popper = e.jquery ? e[0] : e, this.options.modifiers = {}, Object.keys(V({}, s.Defaults.modifiers, o.modifiers)).forEach(function (t) {
					i.options.modifiers[t] = V({}, s.Defaults.modifiers[t] || {}, o.modifiers ? o.modifiers[t] : {})
				}), this.modifiers = Object.keys(this.options.modifiers).map(function (t) {
					return V({
						name: t
					}, i.options.modifiers[t])
				}).sort(function (t, e) {
					return t.order - e.order
				}), this.modifiers.forEach(function (t) {
					t.enabled && r(t.onLoad) && t.onLoad(i.reference, i.popper, i.options, t, i.state)
				}), this.update();
				var n = this.options.eventsEnabled;
				n && this.enableEventListeners(), this.state.eventsEnabled = n
			}
			return F(s, [{
				key: "update",
				value: function () {
					return function () {
						if (!this.state.isDestroyed) {
							var t = {
								instance: this,
								styles: {},
								arrowStyles: {},
								attributes: {},
								flipped: !1,
								offsets: {}
							};
							t.offsets.reference = l(this.state, this.popper, this.reference), t.placement = a(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), t.originalPlacement = t.placement, t.offsets.popper = $(this.popper, t.offsets.reference, t.placement), t.offsets.popper.position = "absolute", t = I(this.modifiers, t), this.state.isCreated ? this.options.onUpdate(t) : (this.state.isCreated = !0, this.options.onCreate(t))
						}
					}.call(this)
				}
			}, {
				key: "destroy",
				value: function () {
					return function () {
						return this.state.isDestroyed = !0, t(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.left = "", this.popper.style.position = "", this.popper.style.top = "", this.popper.style[O("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
					}.call(this)
				}
			}, {
				key: "enableEventListeners",
				value: function () {
					return function () {
						this.state.eventsEnabled || (this.state = e(this.reference, this.options, this.state, this.scheduleUpdate))
					}.call(this)
				}
			}, {
				key: "disableEventListeners",
				value: function () {
					return i.call(this)
				}
			}]), s
		}();
	return J.Utils = ("undefined" == typeof window ? global : window).PopperUtils, J.placements = Q, J.Defaults = {
		placement: "bottom",
		eventsEnabled: !0,
		removeOnDestroy: !1,
		onCreate: function () {},
		onUpdate: function () {},
		modifiers: {
			shift: {
				order: 100,
				enabled: !0,
				fn: function (t) {
					var e = t.placement,
						i = e.split("-")[0],
						o = e.split("-")[1];
					if (o) {
						var n = t.offsets,
							s = n.reference,
							r = n.popper,
							a = -1 !== ["bottom", "top"].indexOf(i),
							l = a ? "left" : "top",
							c = a ? "width" : "height",
							d = {
								start: U({}, l, s[l]),
								end: U({}, l, s[l] + s[c] - r[c])
							};
						t.offsets.popper = V({}, r, d[o])
					}
					return t
				}
			},
			offset: {
				order: 200,
				enabled: !0,
				fn: function (t, e) {
					var i, o = e.offset,
						n = t.placement,
						s = t.offsets,
						r = s.popper,
						a = s.reference,
						l = n.split("-")[0];
					return i = h(+o) ? [+o, 0] : f(o, r, a, l), "left" === l ? (r.top += i[0], r.left -= i[1]) : "right" === l ? (r.top += i[0], r.left += i[1]) : "top" === l ? (r.left += i[0], r.top -= i[1]) : "bottom" === l && (r.left += i[0], r.top += i[1]), t.popper = r, t
				},
				offset: 0
			},
			preventOverflow: {
				order: 300,
				enabled: !0,
				fn: function (t, o) {
					var e = o.boundariesElement || x(t.instance.popper);
					t.instance.reference === e && (e = x(e));
					var n = p(t.instance.popper, t.instance.reference, o.padding, e);
					o.boundaries = n;
					var i = o.priority,
						s = t.offsets.popper,
						r = {
							primary: function (t) {
								var e = s[t];
								return s[t] < n[t] && !o.escapeWithReference && (e = P(s[t], n[t])), U({}, t, e)
							},
							secondary: function (t) {
								var e = "right" === t ? "left" : "top",
									i = s[e];
								return s[t] > n[t] && !o.escapeWithReference && (i = L(s[e], n[t] - ("right" === t ? s.width : s.height))), U({}, e, i)
							}
						};
					return i.forEach(function (t) {
						var e = -1 === ["left", "top"].indexOf(t) ? "secondary" : "primary";
						s = V({}, s, r[e](t))
					}), t.offsets.popper = s, t
				},
				priority: ["left", "right", "top", "bottom"],
				padding: 5,
				boundariesElement: "scrollParent"
			},
			keepTogether: {
				order: 400,
				enabled: !0,
				fn: function (t) {
					var e = t.offsets,
						i = e.popper,
						o = e.reference,
						n = t.placement.split("-")[0],
						s = j,
						r = -1 !== ["top", "bottom"].indexOf(n),
						a = r ? "right" : "bottom",
						l = r ? "left" : "top",
						c = r ? "width" : "height";
					return i[a] < s(o[l]) && (t.offsets.popper[l] = s(o[l]) - i[c]), i[l] > s(o[a]) && (t.offsets.popper[l] = s(o[a])), t
				}
			},
			arrow: {
				order: 500,
				enabled: !0,
				fn: function (t, e) {
					if (!N(t.instance.modifiers, "arrow", "keepTogether")) return t;
					var i = e.element;
					if ("string" == typeof i) {
						if (!(i = t.instance.popper.querySelector(i))) return t
					} else if (!t.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;
					var o = t.placement.split("-")[0],
						n = t.offsets,
						s = n.popper,
						r = n.reference,
						a = -1 !== ["left", "right"].indexOf(o),
						l = a ? "height" : "width",
						c = a ? "Top" : "Left",
						d = c.toLowerCase(),
						u = a ? "left" : "top",
						p = a ? "bottom" : "right",
						h = v(i)[l];
					r[p] - h < s[d] && (t.offsets.popper[d] -= s[d] - (r[p] - h)), r[d] + h > s[p] && (t.offsets.popper[d] += r[d] + h - s[p]);
					var f = r[d] + r[l] / 2 - h / 2,
						m = b(t.instance.popper, "margin" + c).replace("px", ""),
						g = f - _(t.offsets.popper)[d] - m;
					return g = P(L(s[l] - h, g), 0), t.arrowElement = i, t.offsets.arrow = {}, t.offsets.arrow[d] = Math.round(g), t.offsets.arrow[u] = "", t
				},
				element: "[x-arrow]"
			},
			flip: {
				order: 600,
				enabled: !0,
				fn: function (f, m) {
					if (t(f.instance.modifiers, "inner")) return f;
					if (f.flipped && f.placement === f.originalPlacement) return f;
					var g = p(f.instance.popper, f.instance.reference, m.padding, m.boundariesElement),
						v = f.placement.split("-")[0],
						b = A(v),
						y = f.placement.split("-")[1] || "",
						w = [];
					switch (m.behavior) {
						case G:
							w = [v, b];
							break;
						case K:
							w = n(v);
							break;
						case X:
							w = n(v, !0);
							break;
						default:
							w = m.behavior
					}
					return w.forEach(function (t, e) {
						if (v !== t || w.length === e + 1) return f;
						v = f.placement.split("-")[0], b = A(v);
						var i, o = f.offsets.popper,
							n = f.offsets.reference,
							s = j,
							r = "left" === v && s(o.right) > s(n.left) || "right" === v && s(o.left) < s(n.right) || "top" === v && s(o.bottom) > s(n.top) || "bottom" === v && s(o.top) < s(n.bottom),
							a = s(o.left) < s(g.left),
							l = s(o.right) > s(g.right),
							c = s(o.top) < s(g.top),
							d = s(o.bottom) > s(g.bottom),
							u = "left" === v && a || "right" === v && l || "top" === v && c || "bottom" === v && d,
							p = -1 !== ["top", "bottom"].indexOf(v),
							h = !!m.flipVariations && (p && "start" === y && a || p && "end" === y && l || !p && "start" === y && c || !p && "end" === y && d);
						(r || u || h) && (f.flipped = !0, (r || u) && (v = w[e + 1]), h && (y = "end" === (i = y) ? "start" : "start" === i ? "end" : i), f.placement = v + (y ? "-" + y : ""), f.offsets.popper = V({}, f.offsets.popper, $(f.instance.popper, f.offsets.reference, f.placement)), f = I(f.instance.modifiers, f, "flip"))
					}), f
				},
				behavior: "flip",
				padding: 5,
				boundariesElement: "viewport"
			},
			inner: {
				order: 700,
				enabled: !1,
				fn: function (t) {
					var e = t.placement,
						i = e.split("-")[0],
						o = t.offsets,
						n = o.popper,
						s = o.reference,
						r = -1 !== ["left", "right"].indexOf(i),
						a = -1 === ["top", "left"].indexOf(i);
					return n[r ? "left" : "top"] = s[i] - (a ? n[r ? "width" : "height"] : 0), t.placement = A(e), t.offsets.popper = _(n), t
				}
			},
			hide: {
				order: 800,
				enabled: !0,
				fn: function (t) {
					if (!N(t.instance.modifiers, "hide", "preventOverflow")) return t;
					var e = t.offsets.reference,
						i = D(t.instance.modifiers, function (t) {
							return "preventOverflow" === t.name
						}).boundaries;
					if (e.bottom < i.top || e.left > i.right || e.top > i.bottom || e.right < i.left) {
						if (!0 === t.hide) return t;
						t.hide = !0, t.attributes["x-out-of-boundaries"] = ""
					} else {
						if (!1 === t.hide) return t;
						t.hide = !1, t.attributes["x-out-of-boundaries"] = !1
					}
					return t
				}
			},
			computeStyle: {
				order: 850,
				enabled: !0,
				fn: function (t, e) {
					var i = e.x,
						o = e.y,
						n = t.offsets.popper,
						s = D(t.instance.modifiers, function (t) {
							return "applyStyle" === t.name
						}).gpuAcceleration;
					void 0 !== s && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
					var r, a, l = void 0 === s ? e.gpuAcceleration : s,
						c = C(x(t.instance.popper)),
						d = {
							position: n.position
						},
						u = {
							left: j(n.left),
							top: j(n.top),
							bottom: j(n.bottom),
							right: j(n.right)
						},
						p = "bottom" === i ? "top" : "bottom",
						h = "right" === o ? "left" : "right",
						f = O("transform");
					if (a = "bottom" == p ? -c.height + u.bottom : u.top, r = "right" == h ? -c.width + u.right : u.left, l && f) d[f] = "translate3d(" + r + "px, " + a + "px, 0)", d[p] = 0, d[h] = 0, d.willChange = "transform";
					else {
						var m = "bottom" == p ? -1 : 1,
							g = "right" == h ? -1 : 1;
						d[p] = a * m, d[h] = r * g, d.willChange = p + ", " + h
					}
					var v = {
						"x-placement": t.placement
					};
					return t.attributes = V({}, v, t.attributes), t.styles = V({}, d, t.styles), t.arrowStyles = V({}, t.offsets.arrow, t.arrowStyles), t
				},
				gpuAcceleration: !0,
				x: "bottom",
				y: "right"
			},
			applyStyle: {
				order: 900,
				enabled: !0,
				fn: function (t) {
					return c(t.instance.popper, t.styles), e = t.instance.popper, i = t.attributes, Object.keys(i).forEach(function (t) {
						!1 === i[t] ? e.removeAttribute(t) : e.setAttribute(t, i[t])
					}), t.arrowElement && Object.keys(t.arrowStyles).length && c(t.arrowElement, t.arrowStyles), t;
					var e, i
				},
				onLoad: function (t, e, i, o, n) {
					var s = l(0, e, t),
						r = a(i.placement, s, e, t, i.modifiers.flip.boundariesElement, i.modifiers.flip.padding);
					return e.setAttribute("x-placement", r), c(e, {
						position: "absolute"
					}), i
				},
				gpuAcceleration: void 0
			}
		}
	}, J
}),
function () {
	"use strict";

	function e(t) {
		return void 0 === this || Object.getPrototypeOf(this) !== e.prototype ? new e(t) : ((C = this).version = "3.4.0", C.tools = new o, C.isSupported() ? (C.tools.extend(C.defaults, t || {}), C.defaults.container = x(C.defaults), C.store = {
			elements: {},
			containers: []
		}, C.sequences = {}, C.history = [], C.uid = 0, C.initialized = !1) : "undefined" != typeof console && console, C)
	}

	function x(t) {
		if (t && t.container) {
			if ("string" == typeof t.container) return window.document.documentElement.querySelector(t.container);
			if (C.tools.isNode(t.container)) return t.container
		}
		return C.defaults.container
	}

	function T() {
		return ++C.uid
	}

	function k(t, e) {
		var i = t.config;
		return "-webkit-transition: " + t.styles.computed.transition + "-webkit-transform " + i.duration / 1e3 + "s " + i.easing + " " + e / 1e3 + "s, opacity " + i.duration / 1e3 + "s " + i.easing + " " + e / 1e3 + "s; transition: " + t.styles.computed.transition + "transform " + i.duration / 1e3 + "s " + i.easing + " " + e / 1e3 + "s, opacity " + i.duration / 1e3 + "s " + i.easing + " " + e / 1e3 + "s; "
	}

	function E(t) {
		var e, i = t.config,
			o = t.styles.transform;
		e = "top" === i.origin || "left" === i.origin ? /^-/.test(i.distance) ? i.distance.substr(1) : "-" + i.distance : i.distance, parseInt(i.distance) && (o.initial += " translate" + i.axis + "(" + e + ")", o.target += " translate" + i.axis + "(0)"), i.scale && (o.initial += " scale(" + i.scale + ")", o.target += " scale(1)"), i.rotate.x && (o.initial += " rotateX(" + i.rotate.x + "deg)", o.target += " rotateX(0)"), i.rotate.y && (o.initial += " rotateY(" + i.rotate.y + "deg)", o.target += " rotateY(0)"), i.rotate.z && (o.initial += " rotateZ(" + i.rotate.z + "deg)", o.target += " rotateZ(0)"), o.initial += "; opacity: " + i.opacity + ";", o.target += "; opacity: " + t.styles.computed.opacity + ";"
	}

	function _() {
		if (C.isSupported()) {
			i();
			for (var t = 0; t < C.store.containers.length; t++) C.store.containers[t].addEventListener("scroll", d), C.store.containers[t].addEventListener("resize", d);
			C.initialized || (window.addEventListener("scroll", d), window.addEventListener("resize", d), C.initialized = !0)
		}
		return C
	}

	function d() {
		t(i)
	}

	function i() {
		var l, c, i, o, n;
		C.tools.forOwn(C.sequences, function (t) {
			n = C.sequences[t], i = !1;
			for (var e = 0; e < n.elemIds.length; e++) o = n.elemIds[e], p(C.store.elements[o]) && !i && (i = !0);
			n.active = i
		}), C.tools.forOwn(C.store.elements, function (t) {
			var e, i, o, n, s, r, a;
			c = C.store.elements[t], a = (r = c).config.useDelay, l = "always" === a || "onload" === a && !C.initialized || "once" === a && !r.seen,
				function (t) {
					if (t.sequence) {
						var e = C.sequences[t.sequence.id];
						return e.active && !e.blocked && !t.revealing && !t.disabled
					}
					return p(t) && !t.revealing && !t.disabled
				}(c) ? (c.config.beforeReveal(c.domEl), l ? c.domEl.setAttribute("style", c.styles.inline + c.styles.transform.target + c.styles.transition.delayed) : c.domEl.setAttribute("style", c.styles.inline + c.styles.transform.target + c.styles.transition.instant), u("reveal", c, l), c.revealing = !0, c.seen = !0, c.sequence && (e = c, i = l, n = o = 0, (s = C.sequences[e.sequence.id]).blocked = !0, i && "onload" === e.config.useDelay && (n = e.config.delay), e.sequence.timer && (o = Math.abs(e.sequence.timer.started - new Date), window.clearTimeout(e.sequence.timer)), e.sequence.timer = {
					started: new Date
				}, e.sequence.timer.clock = window.setTimeout(function () {
					s.blocked = !1, e.sequence.timer = null, d()
				}, Math.abs(s.interval) + n - o))) : function (t) {
					if (t.sequence) {
						var e = C.sequences[t.sequence.id];
						return !e.active && t.config.reset && t.revealing && !t.disabled
					}
					return !p(t) && t.config.reset && t.revealing && !t.disabled
				}(c) && (c.config.beforeReset(c.domEl), c.domEl.setAttribute("style", c.styles.inline + c.styles.transform.initial + c.styles.transition.instant), u("reset", c), c.revealing = !1)
		})
	}

	function u(t, e, i) {
		var o = 0,
			n = 0,
			s = "after";
		switch (t) {
			case "reveal":
				n = e.config.duration, i && (n += e.config.delay), s += "Reveal";
				break;
			case "reset":
				n = e.config.duration, s += "Reset"
		}
		e.timer && (o = Math.abs(e.timer.started - new Date), window.clearTimeout(e.timer.clock)), e.timer = {
			started: new Date
		}, e.timer.clock = window.setTimeout(function () {
			e.config[s](e.domEl), e.timer = null
		}, n - o)
	}

	function b(t) {
		for (var e = 0, i = 0, o = t.offsetHeight, n = t.offsetWidth; isNaN(t.offsetTop) || (e += t.offsetTop), isNaN(t.offsetLeft) || (i += t.offsetLeft), t = t.offsetParent;);
		return {
			top: e,
			left: i,
			height: o,
			width: n
		}
	}

	function p(t) {
		var e, i, o, n, s, r, a, l, c, d = b(t.domEl),
			u = {
				width: (e = t.config.container).clientWidth,
				height: e.clientHeight
			},
			p = function (t) {
				if (t && t !== window.document.documentElement) {
					var e = b(t);
					return {
						x: t.scrollLeft + e.left,
						y: t.scrollTop + e.top
					}
				}
				return {
					x: window.pageXOffset,
					y: window.pageYOffset
				}
			}(t.config.container),
			h = t.config.viewFactor,
			f = d.height,
			m = d.width,
			g = d.top,
			v = d.left;
		return i = g + f * h, o = v + m * h, n = g + f - f * h, s = v + m - m * h, r = p.y + t.config.viewOffset.top, a = p.x + t.config.viewOffset.left, l = p.y - t.config.viewOffset.bottom + u.height, c = p.x - t.config.viewOffset.right + u.width, i < l && r < n && o < c && a < s || "fixed" === window.getComputedStyle(t.domEl).position
	}

	function o() {}
	var C, t;
	e.prototype.defaults = {
		origin: "bottom",
		distance: "20px",
		duration: 500,
		delay: 0,
		rotate: {
			x: 0,
			y: 0,
			z: 0
		},
		opacity: 0,
		scale: .9,
		easing: "cubic-bezier(0.6, 0.2, 0.1, 1)",
		container: window.document.documentElement,
		mobile: !0,
		reset: !1,
		useDelay: "always",
		viewFactor: .2,
		viewOffset: {
			top: 0,
			right: 0,
			bottom: 0,
			left: 0
		},
		beforeReveal: function (t) {},
		beforeReset: function (t) {},
		afterReveal: function (t) {},
		afterReset: function (t) {}
	}, e.prototype.isSupported = function () {
		var t = document.documentElement.style;
		return "WebkitTransition" in t && "WebkitTransform" in t || "transition" in t && "transform" in t
	}, e.prototype.reveal = function (t, e, i, o) {
		var n, s, r, a, l, c, d, u, p, h, f, m, g, v, b, y;
		if (void 0 !== e && "number" == typeof e ? (i = e, e = {}) : null != e || (e = {}), n = x(e), u = n, !(s = "string" == typeof (d = t) ? Array.prototype.slice.call(u.querySelectorAll(d)) : C.tools.isNode(d) ? [d] : C.tools.isNodeList(d) ? Array.prototype.slice.call(d) : Array.isArray(d) ? d.filter(C.tools.isNode) : []).length) return C;
		i && "number" == typeof i && (c = T(), l = C.sequences[c] = {
			id: c,
			interval: i,
			elemIds: [],
			active: !1
		});
		for (var w = 0; w < s.length; w++)(a = s[w].getAttribute("data-sr-id")) ? r = C.store.elements[a] : (r = {
			id: T(),
			domEl: s[w],
			seen: !1,
			revealing: !1
		}).domEl.setAttribute("data-sr-id", r.id), l && (r.sequence = {
			id: l.id,
			index: l.elemIds.length
		}, l.elemIds.push(r.id)), g = r, b = n, (v = e).container && (v.container = b), g.config ? g.config = C.tools.extendClone(g.config, v) : g.config = C.tools.extendClone(C.defaults, v), "top" === g.config.origin || "bottom" === g.config.origin ? g.config.axis = "Y" : g.config.axis = "X", f = r, void 0, m = window.getComputedStyle(f.domEl), f.styles || (f.styles = {
			transition: {},
			transform: {},
			computed: {}
		}, f.styles.inline = f.domEl.getAttribute("style") || "", f.styles.inline += "; visibility: visible; ", f.styles.computed.opacity = m.opacity, m.transition && "all 0s ease 0s" !== m.transition ? f.styles.computed.transition = m.transition + ", " : f.styles.computed.transition = ""), f.styles.transition.instant = k(f, 0), f.styles.transition.delayed = k(f, f.config.delay), f.styles.transform.initial = " -webkit-transform:", f.styles.transform.target = " -webkit-transform:", E(f), f.styles.transform.initial += "transform:", f.styles.transform.target += "transform:", E(f), void 0, (h = (p = r).config.container) && -1 === C.store.containers.indexOf(h) && C.store.containers.push(p.config.container), C.store.elements[p.id] = p, C.tools.isMobile() && !r.config.mobile || !C.isSupported() ? (r.domEl.setAttribute("style", r.styles.inline), r.disabled = !0) : r.revealing || r.domEl.setAttribute("style", r.styles.inline + r.styles.transform.initial);
		return !o && C.isSupported() && (y = {
			target: t,
			config: e,
			interval: i
		}, C.history.push(y), C.initTimeout && window.clearTimeout(C.initTimeout), C.initTimeout = window.setTimeout(_, 0)), C
	}, e.prototype.sync = function () {
		if (C.history.length && C.isSupported()) {
			for (var t = 0; t < C.history.length; t++) {
				var e = C.history[t];
				C.reveal(e.target, e.config, e.interval, !0)
			}
			_()
		}
		return C
	}, o.prototype.isObject = function (t) {
		return null !== t && "object" == typeof t && t.constructor === Object
	}, o.prototype.isNode = function (t) {
		return "object" == typeof window.Node ? t instanceof window.Node : t && "object" == typeof t && "number" == typeof t.nodeType && "string" == typeof t.nodeName
	}, o.prototype.isNodeList = function (t) {
		var e = Object.prototype.toString.call(t);
		return "object" == typeof window.NodeList ? t instanceof window.NodeList : t && "object" == typeof t && /^\[object (HTMLCollection|NodeList|Object)\]$/.test(e) && "number" == typeof t.length && (0 === t.length || this.isNode(t[0]))
	}, o.prototype.forOwn = function (t, e) {
		if (!this.isObject(t)) throw new TypeError('Expected "object", but received "' + typeof t + '".');
		for (var i in t) t.hasOwnProperty(i) && e(i)
	}, o.prototype.extend = function (e, i) {
		return this.forOwn(i, function (t) {
			this.isObject(i[t]) ? (e[t] && this.isObject(e[t]) || (e[t] = {}), this.extend(e[t], i[t])) : e[t] = i[t]
		}.bind(this)), e
	}, o.prototype.extendClone = function (t, e) {
		return this.extend(this.extend({}, t), e)
	}, o.prototype.isMobile = function () {
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	}, t = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (t) {
		window.setTimeout(t, 1e3 / 60)
	}, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () {
		return e
	}) : "undefined" != typeof module && module.exports ? module.exports = e : window.ScrollReveal = e
}(),
function (t) {
	"use strict";
	"function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
}(function (c) {
	"use strict";
	var n, s = window.Slick || {};
	n = 0, (s = function (t, e) {
		var i, o = this;
		o.defaults = {
			accessibility: !0,
			adaptiveHeight: !1,
			appendArrows: c(t),
			appendDots: c(t),
			arrows: !0,
			asNavFor: null,
			prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
			nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
			autoplay: !1,
			autoplaySpeed: 3e3,
			centerMode: !1,
			centerPadding: "50px",
			cssEase: "ease",
			customPaging: function (t, e) {
				return c('<button type="button" />').text(e + 1)
			},
			dots: !1,
			dotsClass: "slick-dots",
			draggable: !0,
			easing: "linear",
			edgeFriction: .35,
			fade: !1,
			focusOnSelect: !1,
			focusOnChange: !1,
			infinite: !0,
			initialSlide: 0,
			lazyLoad: "ondemand",
			mobileFirst: !1,
			pauseOnHover: !0,
			pauseOnFocus: !0,
			pauseOnDotsHover: !1,
			respondTo: "window",
			responsive: null,
			rows: 1,
			rtl: !1,
			slide: "",
			slidesPerRow: 1,
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 500,
			swipe: !0,
			swipeToSlide: !1,
			touchMove: !0,
			touchThreshold: 5,
			useCSS: !0,
			useTransform: !0,
			variableWidth: !1,
			vertical: !1,
			verticalSwiping: !1,
			waitForAnimate: !0,
			zIndex: 1e3
		}, o.initials = {
			animating: !1,
			dragging: !1,
			autoPlayTimer: null,
			currentDirection: 0,
			currentLeft: null,
			currentSlide: 0,
			direction: 1,
			$dots: null,
			listWidth: null,
			listHeight: null,
			loadIndex: 0,
			$nextArrow: null,
			$prevArrow: null,
			scrolling: !1,
			slideCount: null,
			slideWidth: null,
			$slideTrack: null,
			$slides: null,
			sliding: !1,
			slideOffset: 0,
			swipeLeft: null,
			swiping: !1,
			$list: null,
			touchObject: {},
			transformsEnabled: !1,
			unslicked: !1
		}, c.extend(o, o.initials), o.activeBreakpoint = null, o.animType = null, o.animProp = null, o.breakpoints = [], o.breakpointSettings = [], o.cssTransitions = !1, o.focussed = !1, o.interrupted = !1, o.hidden = "hidden", o.paused = !0, o.positionProp = null, o.respondTo = null, o.rowCount = 1, o.shouldClick = !0, o.$slider = c(t), o.$slidesCache = null, o.transformType = null, o.transitionType = null, o.visibilityChange = "visibilitychange", o.windowWidth = 0, o.windowTimer = null, i = c(t).data("slick") || {}, o.options = c.extend({}, o.defaults, e, i), o.currentSlide = o.options.initialSlide, o.originalSettings = o.options, void 0 !== document.mozHidden ? (o.hidden = "mozHidden", o.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (o.hidden = "webkitHidden", o.visibilityChange = "webkitvisibilitychange"), o.autoPlay = c.proxy(o.autoPlay, o), o.autoPlayClear = c.proxy(o.autoPlayClear, o), o.autoPlayIterator = c.proxy(o.autoPlayIterator, o), o.changeSlide = c.proxy(o.changeSlide, o), o.clickHandler = c.proxy(o.clickHandler, o), o.selectHandler = c.proxy(o.selectHandler, o), o.setPosition = c.proxy(o.setPosition, o), o.swipeHandler = c.proxy(o.swipeHandler, o), o.dragHandler = c.proxy(o.dragHandler, o), o.keyHandler = c.proxy(o.keyHandler, o), o.instanceUid = n++, o.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, o.registerBreakpoints(), o.init(!0)
	}).prototype.activateADA = function () {
		this.$slideTrack.find(".slick-active").attr({
			"aria-hidden": "false"
		}).find("a, input, button, select").attr({
			tabindex: "0"
		})
	}, s.prototype.addSlide = s.prototype.slickAdd = function (t, e, i) {
		var o = this;
		if ("boolean" == typeof e) i = e, e = null;
		else if (e < 0 || e >= o.slideCount) return !1;
		o.unload(), "number" == typeof e ? 0 === e && 0 === o.$slides.length ? c(t).appendTo(o.$slideTrack) : i ? c(t).insertBefore(o.$slides.eq(e)) : c(t).insertAfter(o.$slides.eq(e)) : !0 === i ? c(t).prependTo(o.$slideTrack) : c(t).appendTo(o.$slideTrack), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slides.each(function (t, e) {
			c(e).attr("data-slick-index", t)
		}), o.$slidesCache = o.$slides, o.reinit()
	}, s.prototype.animateHeight = function () {
		var t = this;
		if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
			var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
			t.$list.animate({
				height: e
			}, t.options.speed)
		}
	}, s.prototype.animateSlide = function (t, e) {
		var i = {},
			o = this;
		o.animateHeight(), !0 === o.options.rtl && !1 === o.options.vertical && (t = -t), !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
			left: t
		}, o.options.speed, o.options.easing, e) : o.$slideTrack.animate({
			top: t
		}, o.options.speed, o.options.easing, e) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft), c({
			animStart: o.currentLeft
		}).animate({
			animStart: t
		}, {
			duration: o.options.speed,
			easing: o.options.easing,
			step: function (t) {
				t = Math.ceil(t), !1 === o.options.vertical ? i[o.animType] = "translate(" + t + "px, 0px)" : i[o.animType] = "translate(0px," + t + "px)", o.$slideTrack.css(i)
			},
			complete: function () {
				e && e.call()
			}
		})) : (o.applyTransition(), t = Math.ceil(t), !1 === o.options.vertical ? i[o.animType] = "translate3d(" + t + "px, 0px, 0px)" : i[o.animType] = "translate3d(0px," + t + "px, 0px)", o.$slideTrack.css(i), e && setTimeout(function () {
			o.disableTransition(), e.call()
		}, o.options.speed))
	}, s.prototype.getNavTarget = function () {
		var t = this.options.asNavFor;
		return t && null !== t && (t = c(t).not(this.$slider)), t
	}, s.prototype.asNavFor = function (e) {
		var t = this.getNavTarget();
		null !== t && "object" == typeof t && t.each(function () {
			var t = c(this).slick("getSlick");
			t.unslicked || t.slideHandler(e, !0)
		})
	}, s.prototype.applyTransition = function (t) {
		var e = this,
			i = {};
		!1 === e.options.fade ? i[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : i[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i)
	}, s.prototype.autoPlay = function () {
		var t = this;
		t.autoPlayClear(), t.slideCount > t.options.slidesToShow && (t.autoPlayTimer = setInterval(t.autoPlayIterator, t.options.autoplaySpeed))
	}, s.prototype.autoPlayClear = function () {
		this.autoPlayTimer && clearInterval(this.autoPlayTimer)
	}, s.prototype.autoPlayIterator = function () {
		var t = this,
			e = t.currentSlide + t.options.slidesToScroll;
		t.paused || t.interrupted || t.focussed || (!1 === t.options.infinite && (1 === t.direction && t.currentSlide + 1 === t.slideCount - 1 ? t.direction = 0 : 0 === t.direction && (e = t.currentSlide - t.options.slidesToScroll, t.currentSlide - 1 == 0 && (t.direction = 1))), t.slideHandler(e))
	}, s.prototype.buildArrows = function () {
		var t = this;
		!0 === t.options.arrows && (t.$prevArrow = c(t.options.prevArrow).addClass("slick-arrow"), t.$nextArrow = c(t.options.nextArrow).addClass("slick-arrow"), t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows), !0 !== t.options.infinite && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
			"aria-disabled": "true",
			tabindex: "-1"
		}))
	}, s.prototype.buildDots = function () {
		var t, e, i = this;
		if (!0 === i.options.dots && i.slideCount > i.options.slidesToShow) {
			for (i.$slider.addClass("slick-dotted"), e = c("<ul />").addClass(i.options.dotsClass), t = 0; t <= i.getDotCount(); t += 1) e.append(c("<li />").append(i.options.customPaging.call(this, i, t)));
			i.$dots = e.appendTo(i.options.appendDots), i.$dots.find("li").first().addClass("slick-active")
		}
	}, s.prototype.buildOut = function () {
		var t = this;
		t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), t.slideCount = t.$slides.length, t.$slides.each(function (t, e) {
			c(e).attr("data-slick-index", t).data("originalStyling", c(e).attr("style") || "")
		}), t.$slider.addClass("slick-slider"), t.$slideTrack = 0 === t.slideCount ? c('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent(), t.$list = t.$slideTrack.wrap('<div class="slick-list"/>').parent(), t.$slideTrack.css("opacity", 0), !0 !== t.options.centerMode && !0 !== t.options.swipeToSlide || (t.options.slidesToScroll = 1), c("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"), t.setupInfinite(), t.buildArrows(), t.buildDots(), t.updateDots(), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), !0 === t.options.draggable && t.$list.addClass("draggable")
	}, s.prototype.buildRows = function () {
		var t, e, i, o, n, s, r, a = this;
		if (o = document.createDocumentFragment(), s = a.$slider.children(), 0 < a.options.rows) {
			for (r = a.options.slidesPerRow * a.options.rows, n = Math.ceil(s.length / r), t = 0; t < n; t++) {
				var l = document.createElement("div");
				for (e = 0; e < a.options.rows; e++) {
					var c = document.createElement("div");
					for (i = 0; i < a.options.slidesPerRow; i++) {
						var d = t * r + (e * a.options.slidesPerRow + i);
						s.get(d) && c.appendChild(s.get(d))
					}
					l.appendChild(c)
				}
				o.appendChild(l)
			}
			a.$slider.empty().append(o), a.$slider.children().children().children().css({
				width: 100 / a.options.slidesPerRow + "%",
				display: "inline-block"
			})
		}
	}, s.prototype.checkResponsive = function (t, e) {
		var i, o, n, s = this,
			r = !1,
			a = s.$slider.width(),
			l = window.innerWidth || c(window).width();
		if ("window" === s.respondTo ? n = l : "slider" === s.respondTo ? n = a : "min" === s.respondTo && (n = Math.min(l, a)), s.options.responsive && s.options.responsive.length && null !== s.options.responsive) {
			for (i in o = null, s.breakpoints) s.breakpoints.hasOwnProperty(i) && (!1 === s.originalSettings.mobileFirst ? n < s.breakpoints[i] && (o = s.breakpoints[i]) : n > s.breakpoints[i] && (o = s.breakpoints[i]));
			null !== o ? null !== s.activeBreakpoint ? (o !== s.activeBreakpoint || e) && (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = c.extend({}, s.originalSettings, s.breakpointSettings[o]), !0 === t && (s.currentSlide = s.options.initialSlide), s.refresh(t)), r = o) : (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = c.extend({}, s.originalSettings, s.breakpointSettings[o]), !0 === t && (s.currentSlide = s.options.initialSlide), s.refresh(t)), r = o) : null !== s.activeBreakpoint && (s.activeBreakpoint = null, s.options = s.originalSettings, !0 === t && (s.currentSlide = s.options.initialSlide), s.refresh(t), r = o), t || !1 === r || s.$slider.trigger("breakpoint", [s, r])
		}
	}, s.prototype.changeSlide = function (t, e) {
		var i, o, n = this,
			s = c(t.currentTarget);
		switch (s.is("a") && t.preventDefault(), s.is("li") || (s = s.closest("li")), i = n.slideCount % n.options.slidesToScroll != 0 ? 0 : (n.slideCount - n.currentSlide) % n.options.slidesToScroll, t.data.message) {
			case "previous":
				o = 0 === i ? n.options.slidesToScroll : n.options.slidesToShow - i, n.slideCount > n.options.slidesToShow && n.slideHandler(n.currentSlide - o, !1, e);
				break;
			case "next":
				o = 0 === i ? n.options.slidesToScroll : i, n.slideCount > n.options.slidesToShow && n.slideHandler(n.currentSlide + o, !1, e);
				break;
			case "index":
				var r = 0 === t.data.index ? 0 : t.data.index || s.index() * n.options.slidesToScroll;
				n.slideHandler(n.checkNavigable(r), !1, e), s.children().trigger("focus");
				break;
			default:
				return
		}
	}, s.prototype.checkNavigable = function (t) {
		var e, i;
		if (i = 0, t > (e = this.getNavigableIndexes())[e.length - 1]) t = e[e.length - 1];
		else
			for (var o in e) {
				if (t < e[o]) {
					t = i;
					break
				}
				i = e[o]
			}
		return t
	}, s.prototype.cleanUpEvents = function () {
		var t = this;
		t.options.dots && null !== t.$dots && (c("li", t.$dots).off("click.slick", t.changeSlide).off("mouseenter.slick", c.proxy(t.interrupt, t, !0)).off("mouseleave.slick", c.proxy(t.interrupt, t, !1)), !0 === t.options.accessibility && t.$dots.off("keydown.slick", t.keyHandler)), t.$slider.off("focus.slick blur.slick"), !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide), t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide), !0 === t.options.accessibility && (t.$prevArrow && t.$prevArrow.off("keydown.slick", t.keyHandler), t.$nextArrow && t.$nextArrow.off("keydown.slick", t.keyHandler))), t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler), t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler), t.$list.off("touchend.slick mouseup.slick", t.swipeHandler), t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler), t.$list.off("click.slick", t.clickHandler), c(document).off(t.visibilityChange, t.visibility), t.cleanUpSlideEvents(), !0 === t.options.accessibility && t.$list.off("keydown.slick", t.keyHandler), !0 === t.options.focusOnSelect && c(t.$slideTrack).children().off("click.slick", t.selectHandler), c(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange), c(window).off("resize.slick.slick-" + t.instanceUid, t.resize), c("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault), c(window).off("load.slick.slick-" + t.instanceUid, t.setPosition)
	}, s.prototype.cleanUpSlideEvents = function () {
		var t = this;
		t.$list.off("mouseenter.slick", c.proxy(t.interrupt, t, !0)), t.$list.off("mouseleave.slick", c.proxy(t.interrupt, t, !1))
	}, s.prototype.cleanUpRows = function () {
		var t;
		0 < this.options.rows && ((t = this.$slides.children().children()).removeAttr("style"), this.$slider.empty().append(t))
	}, s.prototype.clickHandler = function (t) {
		!1 === this.shouldClick && (t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault())
	}, s.prototype.destroy = function (t) {
		var e = this;
		e.autoPlayClear(), e.touchObject = {}, e.cleanUpEvents(), c(".slick-cloned", e.$slider).detach(), e.$dots && e.$dots.remove(), e.$prevArrow && e.$prevArrow.length && (e.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove()), e.$nextArrow && e.$nextArrow.length && (e.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove()), e.$slides && (e.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
			c(this).attr("style", c(this).data("originalStyling"))
		}), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.detach(), e.$list.detach(), e.$slider.append(e.$slides)), e.cleanUpRows(), e.$slider.removeClass("slick-slider"), e.$slider.removeClass("slick-initialized"), e.$slider.removeClass("slick-dotted"), e.unslicked = !0, t || e.$slider.trigger("destroy", [e])
	}, s.prototype.disableTransition = function (t) {
		var e = {};
		e[this.transitionType] = "", !1 === this.options.fade ? this.$slideTrack.css(e) : this.$slides.eq(t).css(e)
	}, s.prototype.fadeSlide = function (t, e) {
		var i = this;
		!1 === i.cssTransitions ? (i.$slides.eq(t).css({
			zIndex: i.options.zIndex
		}), i.$slides.eq(t).animate({
			opacity: 1
		}, i.options.speed, i.options.easing, e)) : (i.applyTransition(t), i.$slides.eq(t).css({
			opacity: 1,
			zIndex: i.options.zIndex
		}), e && setTimeout(function () {
			i.disableTransition(t), e.call()
		}, i.options.speed))
	}, s.prototype.fadeSlideOut = function (t) {
		var e = this;
		!1 === e.cssTransitions ? e.$slides.eq(t).animate({
			opacity: 0,
			zIndex: e.options.zIndex - 2
		}, e.options.speed, e.options.easing) : (e.applyTransition(t), e.$slides.eq(t).css({
			opacity: 0,
			zIndex: e.options.zIndex - 2
		}))
	}, s.prototype.filterSlides = s.prototype.slickFilter = function (t) {
		var e = this;
		null !== t && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(t).appendTo(e.$slideTrack), e.reinit())
	}, s.prototype.focusHandler = function () {
		var i = this;
		i.$slider.off("focus.slick blur.slick").on("focus.slick", "*", function (t) {
			var e = c(this);
			setTimeout(function () {
				i.options.pauseOnFocus && e.is(":focus") && (i.focussed = !0, i.autoPlay())
			}, 0)
		}).on("blur.slick", "*", function (t) {
			c(this), i.options.pauseOnFocus && (i.focussed = !1, i.autoPlay())
		})
	}, s.prototype.getCurrent = s.prototype.slickCurrentSlide = function () {
		return this.currentSlide
	}, s.prototype.getDotCount = function () {
		var t = this,
			e = 0,
			i = 0,
			o = 0;
		if (!0 === t.options.infinite)
			if (t.slideCount <= t.options.slidesToShow) ++o;
			else
				for (; e < t.slideCount;) ++o, e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
		else if (!0 === t.options.centerMode) o = t.slideCount;
		else if (t.options.asNavFor)
			for (; e < t.slideCount;) ++o, e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
		else o = 1 + Math.ceil((t.slideCount - t.options.slidesToShow) / t.options.slidesToScroll);
		return o - 1
	}, s.prototype.getLeft = function (t) {
		var e, i, o, n, s = this,
			r = 0;
		return s.slideOffset = 0, i = s.$slides.first().outerHeight(!0), !0 === s.options.infinite ? (s.slideCount > s.options.slidesToShow && (s.slideOffset = s.slideWidth * s.options.slidesToShow * -1, n = -1, !0 === s.options.vertical && !0 === s.options.centerMode && (2 === s.options.slidesToShow ? n = -1.5 : 1 === s.options.slidesToShow && (n = -2)), r = i * s.options.slidesToShow * n), s.slideCount % s.options.slidesToScroll != 0 && t + s.options.slidesToScroll > s.slideCount && s.slideCount > s.options.slidesToShow && (t > s.slideCount ? (s.slideOffset = (s.options.slidesToShow - (t - s.slideCount)) * s.slideWidth * -1, r = (s.options.slidesToShow - (t - s.slideCount)) * i * -1) : (s.slideOffset = s.slideCount % s.options.slidesToScroll * s.slideWidth * -1, r = s.slideCount % s.options.slidesToScroll * i * -1))) : t + s.options.slidesToShow > s.slideCount && (s.slideOffset = (t + s.options.slidesToShow - s.slideCount) * s.slideWidth, r = (t + s.options.slidesToShow - s.slideCount) * i), s.slideCount <= s.options.slidesToShow && (r = s.slideOffset = 0), !0 === s.options.centerMode && s.slideCount <= s.options.slidesToShow ? s.slideOffset = s.slideWidth * Math.floor(s.options.slidesToShow) / 2 - s.slideWidth * s.slideCount / 2 : !0 === s.options.centerMode && !0 === s.options.infinite ? s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2) - s.slideWidth : !0 === s.options.centerMode && (s.slideOffset = 0, s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2)), e = !1 === s.options.vertical ? t * s.slideWidth * -1 + s.slideOffset : t * i * -1 + r, !0 === s.options.variableWidth && (o = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(t) : s.$slideTrack.children(".slick-slide").eq(t + s.options.slidesToShow), e = !0 === s.options.rtl ? o[0] ? -1 * (s.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, !0 === s.options.centerMode && (o = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(t) : s.$slideTrack.children(".slick-slide").eq(t + s.options.slidesToShow + 1), e = !0 === s.options.rtl ? o[0] ? -1 * (s.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, e += (s.$list.width() - o.outerWidth()) / 2)), e
	}, s.prototype.getOption = s.prototype.slickGetOption = function (t) {
		return this.options[t]
	}, s.prototype.getNavigableIndexes = function () {
		var t, e = this,
			i = 0,
			o = 0,
			n = [];
		for (!1 === e.options.infinite ? t = e.slideCount : (i = -1 * e.options.slidesToScroll, o = -1 * e.options.slidesToScroll, t = 2 * e.slideCount); i < t;) n.push(i), i = o + e.options.slidesToScroll, o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
		return n
	}, s.prototype.getSlick = function () {
		return this
	}, s.prototype.getSlideCount = function () {
		var n, s, t, r = this;
		return t = !0 === r.options.centerMode ? Math.floor(r.$list.width() / 2) : 0, s = -1 * r.swipeLeft + t, !0 === r.options.swipeToSlide ? (r.$slideTrack.find(".slick-slide").each(function (t, e) {
			var i, o;
			if (i = c(e).outerWidth(), o = e.offsetLeft, !0 !== r.options.centerMode && (o += i / 2), s < o + i) return n = e, !1
		}), Math.abs(c(n).attr("data-slick-index") - r.currentSlide) || 1) : r.options.slidesToScroll
	}, s.prototype.goTo = s.prototype.slickGoTo = function (t, e) {
		this.changeSlide({
			data: {
				message: "index",
				index: parseInt(t)
			}
		}, e)
	}, s.prototype.init = function (t) {
		var e = this;
		c(e.$slider).hasClass("slick-initialized") || (c(e.$slider).addClass("slick-initialized"), e.buildRows(), e.buildOut(), e.setProps(), e.startLoad(), e.loadSlider(), e.initializeEvents(), e.updateArrows(), e.updateDots(), e.checkResponsive(!0), e.focusHandler()), t && e.$slider.trigger("init", [e]), !0 === e.options.accessibility && e.initADA(), e.options.autoplay && (e.paused = !1, e.autoPlay())
	}, s.prototype.initADA = function () {
		var o = this,
			i = Math.ceil(o.slideCount / o.options.slidesToShow),
			n = o.getNavigableIndexes().filter(function (t) {
				return 0 <= t && t < o.slideCount
			});
		o.$slides.add(o.$slideTrack.find(".slick-cloned")).attr({
			"aria-hidden": "true",
			tabindex: "-1"
		}).find("a, input, button, select").attr({
			tabindex: "-1"
		}), null !== o.$dots && (o.$slides.not(o.$slideTrack.find(".slick-cloned")).each(function (t) {
			var e = n.indexOf(t);
			if (c(this).attr({
					role: "tabpanel",
					id: "slick-slide" + o.instanceUid + t,
					tabindex: -1
				}), -1 !== e) {
				var i = "slick-slide-control" + o.instanceUid + e;
				c("#" + i).length && c(this).attr({
					"aria-describedby": i
				})
			}
		}), o.$dots.attr("role", "tablist").find("li").each(function (t) {
			var e = n[t];
			c(this).attr({
				role: "presentation"
			}), c(this).find("button").first().attr({
				role: "tab",
				id: "slick-slide-control" + o.instanceUid + t,
				"aria-controls": "slick-slide" + o.instanceUid + e,
				"aria-label": t + 1 + " of " + i,
				"aria-selected": null,
				tabindex: "-1"
			})
		}).eq(o.currentSlide).find("button").attr({
			"aria-selected": "true",
			tabindex: "0"
		}).end());
		for (var t = o.currentSlide, e = t + o.options.slidesToShow; t < e; t++) o.options.focusOnChange ? o.$slides.eq(t).attr({
			tabindex: "0"
		}) : o.$slides.eq(t).removeAttr("tabindex");
		o.activateADA()
	}, s.prototype.initArrowEvents = function () {
		var t = this;
		!0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.off("click.slick").on("click.slick", {
			message: "previous"
		}, t.changeSlide), t.$nextArrow.off("click.slick").on("click.slick", {
			message: "next"
		}, t.changeSlide), !0 === t.options.accessibility && (t.$prevArrow.on("keydown.slick", t.keyHandler), t.$nextArrow.on("keydown.slick", t.keyHandler)))
	}, s.prototype.initDotEvents = function () {
		var t = this;
		!0 === t.options.dots && t.slideCount > t.options.slidesToShow && (c("li", t.$dots).on("click.slick", {
			message: "index"
		}, t.changeSlide), !0 === t.options.accessibility && t.$dots.on("keydown.slick", t.keyHandler)), !0 === t.options.dots && !0 === t.options.pauseOnDotsHover && t.slideCount > t.options.slidesToShow && c("li", t.$dots).on("mouseenter.slick", c.proxy(t.interrupt, t, !0)).on("mouseleave.slick", c.proxy(t.interrupt, t, !1))
	}, s.prototype.initSlideEvents = function () {
		var t = this;
		t.options.pauseOnHover && (t.$list.on("mouseenter.slick", c.proxy(t.interrupt, t, !0)), t.$list.on("mouseleave.slick", c.proxy(t.interrupt, t, !1)))
	}, s.prototype.initializeEvents = function () {
		var t = this;
		t.initArrowEvents(), t.initDotEvents(), t.initSlideEvents(), t.$list.on("touchstart.slick mousedown.slick", {
			action: "start"
		}, t.swipeHandler), t.$list.on("touchmove.slick mousemove.slick", {
			action: "move"
		}, t.swipeHandler), t.$list.on("touchend.slick mouseup.slick", {
			action: "end"
		}, t.swipeHandler), t.$list.on("touchcancel.slick mouseleave.slick", {
			action: "end"
		}, t.swipeHandler), t.$list.on("click.slick", t.clickHandler), c(document).on(t.visibilityChange, c.proxy(t.visibility, t)), !0 === t.options.accessibility && t.$list.on("keydown.slick", t.keyHandler), !0 === t.options.focusOnSelect && c(t.$slideTrack).children().on("click.slick", t.selectHandler), c(window).on("orientationchange.slick.slick-" + t.instanceUid, c.proxy(t.orientationChange, t)), c(window).on("resize.slick.slick-" + t.instanceUid, c.proxy(t.resize, t)), c("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault), c(window).on("load.slick.slick-" + t.instanceUid, t.setPosition), c(t.setPosition)
	}, s.prototype.initUI = function () {
		var t = this;
		!0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.show(), t.$nextArrow.show()), !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.show()
	}, s.prototype.keyHandler = function (t) {
		var e = this;
		t.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === t.keyCode && !0 === e.options.accessibility ? e.changeSlide({
			data: {
				message: !0 === e.options.rtl ? "next" : "previous"
			}
		}) : 39 === t.keyCode && !0 === e.options.accessibility && e.changeSlide({
			data: {
				message: !0 === e.options.rtl ? "previous" : "next"
			}
		}))
	}, s.prototype.lazyLoad = function () {
		function t(t) {
			c("img[data-lazy]", t).each(function () {
				var t = c(this),
					e = c(this).attr("data-lazy"),
					i = c(this).attr("data-srcset"),
					o = c(this).attr("data-sizes") || s.$slider.attr("data-sizes"),
					n = document.createElement("img");
				n.onload = function () {
					t.animate({
						opacity: 0
					}, 100, function () {
						i && (t.attr("srcset", i), o && t.attr("sizes", o)), t.attr("src", e).animate({
							opacity: 1
						}, 200, function () {
							t.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
						}), s.$slider.trigger("lazyLoaded", [s, t, e])
					})
				}, n.onerror = function () {
					t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, t, e])
				}, n.src = e
			})
		}
		var e, i, o, s = this;
		if (!0 === s.options.centerMode ? !0 === s.options.infinite ? o = (i = s.currentSlide + (s.options.slidesToShow / 2 + 1)) + s.options.slidesToShow + 2 : (i = Math.max(0, s.currentSlide - (s.options.slidesToShow / 2 + 1)), o = s.options.slidesToShow / 2 + 1 + 2 + s.currentSlide) : (i = s.options.infinite ? s.options.slidesToShow + s.currentSlide : s.currentSlide, o = Math.ceil(i + s.options.slidesToShow), !0 === s.options.fade && (0 < i && i--, o <= s.slideCount && o++)), e = s.$slider.find(".slick-slide").slice(i, o), "anticipated" === s.options.lazyLoad)
			for (var n = i - 1, r = o, a = s.$slider.find(".slick-slide"), l = 0; l < s.options.slidesToScroll; l++) n < 0 && (n = s.slideCount - 1), e = (e = e.add(a.eq(n))).add(a.eq(r)), n--, r++;
		t(e), s.slideCount <= s.options.slidesToShow ? t(s.$slider.find(".slick-slide")) : s.currentSlide >= s.slideCount - s.options.slidesToShow ? t(s.$slider.find(".slick-cloned").slice(0, s.options.slidesToShow)) : 0 === s.currentSlide && t(s.$slider.find(".slick-cloned").slice(-1 * s.options.slidesToShow))
	}, s.prototype.loadSlider = function () {
		var t = this;
		t.setPosition(), t.$slideTrack.css({
			opacity: 1
		}), t.$slider.removeClass("slick-loading"), t.initUI(), "progressive" === t.options.lazyLoad && t.progressiveLazyLoad()
	}, s.prototype.next = s.prototype.slickNext = function () {
		this.changeSlide({
			data: {
				message: "next"
			}
		})
	}, s.prototype.orientationChange = function () {
		this.checkResponsive(), this.setPosition()
	}, s.prototype.pause = s.prototype.slickPause = function () {
		this.autoPlayClear(), this.paused = !0
	}, s.prototype.play = s.prototype.slickPlay = function () {
		var t = this;
		t.autoPlay(), t.options.autoplay = !0, t.paused = !1, t.focussed = !1, t.interrupted = !1
	}, s.prototype.postSlide = function (t) {
		var e = this;
		!e.unslicked && (e.$slider.trigger("afterChange", [e, t]), e.animating = !1, e.slideCount > e.options.slidesToShow && e.setPosition(), e.swipeLeft = null, e.options.autoplay && e.autoPlay(), !0 === e.options.accessibility && (e.initADA(), e.options.focusOnChange)) && c(e.$slides.get(e.currentSlide)).attr("tabindex", 0).focus()
	}, s.prototype.prev = s.prototype.slickPrev = function () {
		this.changeSlide({
			data: {
				message: "previous"
			}
		})
	}, s.prototype.preventDefault = function (t) {
		t.preventDefault()
	}, s.prototype.progressiveLazyLoad = function (t) {
		t = t || 1;
		var e, i, o, n, s, r = this,
			a = c("img[data-lazy]", r.$slider);
		a.length ? (e = a.first(), i = e.attr("data-lazy"), o = e.attr("data-srcset"), n = e.attr("data-sizes") || r.$slider.attr("data-sizes"), (s = document.createElement("img")).onload = function () {
			o && (e.attr("srcset", o), n && e.attr("sizes", n)), e.attr("src", i).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === r.options.adaptiveHeight && r.setPosition(), r.$slider.trigger("lazyLoaded", [r, e, i]), r.progressiveLazyLoad()
		}, s.onerror = function () {
			t < 3 ? setTimeout(function () {
				r.progressiveLazyLoad(t + 1)
			}, 500) : (e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), r.$slider.trigger("lazyLoadError", [r, e, i]), r.progressiveLazyLoad())
		}, s.src = i) : r.$slider.trigger("allImagesLoaded", [r])
	}, s.prototype.refresh = function (t) {
		var e, i, o = this;
		i = o.slideCount - o.options.slidesToShow, !o.options.infinite && o.currentSlide > i && (o.currentSlide = i), o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0), e = o.currentSlide, o.destroy(!0), c.extend(o, o.initials, {
			currentSlide: e
		}), o.init(), t || o.changeSlide({
			data: {
				message: "index",
				index: e
			}
		}, !1)
	}, s.prototype.registerBreakpoints = function () {
		var t, e, i, o = this,
			n = o.options.responsive || null;
		if ("array" === c.type(n) && n.length) {
			for (t in o.respondTo = o.options.respondTo || "window", n)
				if (i = o.breakpoints.length - 1, n.hasOwnProperty(t)) {
					for (e = n[t].breakpoint; 0 <= i;) o.breakpoints[i] && o.breakpoints[i] === e && o.breakpoints.splice(i, 1), i--;
					o.breakpoints.push(e), o.breakpointSettings[e] = n[t].settings
				}
			o.breakpoints.sort(function (t, e) {
				return o.options.mobileFirst ? t - e : e - t
			})
		}
	}, s.prototype.reinit = function () {
		var t = this;
		t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide"), t.slideCount = t.$slides.length, t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll), t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0), t.registerBreakpoints(), t.setProps(), t.setupInfinite(), t.buildArrows(), t.updateArrows(), t.initArrowEvents(), t.buildDots(), t.updateDots(), t.initDotEvents(), t.cleanUpSlideEvents(), t.initSlideEvents(), t.checkResponsive(!1, !0), !0 === t.options.focusOnSelect && c(t.$slideTrack).children().on("click.slick", t.selectHandler), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), t.setPosition(), t.focusHandler(), t.paused = !t.options.autoplay, t.autoPlay(), t.$slider.trigger("reInit", [t])
	}, s.prototype.resize = function () {
		var t = this;
		c(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay), t.windowDelay = window.setTimeout(function () {
			t.windowWidth = c(window).width(), t.checkResponsive(), t.unslicked || t.setPosition()
		}, 50))
	}, s.prototype.removeSlide = s.prototype.slickRemove = function (t, e, i) {
		var o = this;
		return "boolean" == typeof t ? t = !0 === (e = t) ? 0 : o.slideCount - 1 : t = !0 === e ? --t : t, !(o.slideCount < 1 || t < 0 || t > o.slideCount - 1) && (o.unload(), !0 === i ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(t).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, void o.reinit())
	}, s.prototype.setCSS = function (t) {
		var e, i, o = this,
			n = {};
		!0 === o.options.rtl && (t = -t), e = "left" == o.positionProp ? Math.ceil(t) + "px" : "0px", i = "top" == o.positionProp ? Math.ceil(t) + "px" : "0px", n[o.positionProp] = t, !1 === o.transformsEnabled || (!(n = {}) === o.cssTransitions ? n[o.animType] = "translate(" + e + ", " + i + ")" : n[o.animType] = "translate3d(" + e + ", " + i + ", 0px)"), o.$slideTrack.css(n)
	}, s.prototype.setDimensions = function () {
		var t = this;
		!1 === t.options.vertical ? !0 === t.options.centerMode && t.$list.css({
			padding: "0px " + t.options.centerPadding
		}) : (t.$list.height(t.$slides.first().outerHeight(!0) * t.options.slidesToShow), !0 === t.options.centerMode && t.$list.css({
			padding: t.options.centerPadding + " 0px"
		})), t.listWidth = t.$list.width(), t.listHeight = t.$list.height(), !1 === t.options.vertical && !1 === t.options.variableWidth ? (t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow), t.$slideTrack.width(Math.ceil(t.slideWidth * t.$slideTrack.children(".slick-slide").length))) : !0 === t.options.variableWidth ? t.$slideTrack.width(5e3 * t.slideCount) : (t.slideWidth = Math.ceil(t.listWidth), t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0) * t.$slideTrack.children(".slick-slide").length)));
		var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
		!1 === t.options.variableWidth && t.$slideTrack.children(".slick-slide").width(t.slideWidth - e)
	}, s.prototype.setFade = function () {
		var i, o = this;
		o.$slides.each(function (t, e) {
			i = o.slideWidth * t * -1, !0 === o.options.rtl ? c(e).css({
				position: "relative",
				right: i,
				top: 0,
				zIndex: o.options.zIndex - 2,
				opacity: 0
			}) : c(e).css({
				position: "relative",
				left: i,
				top: 0,
				zIndex: o.options.zIndex - 2,
				opacity: 0
			})
		}), o.$slides.eq(o.currentSlide).css({
			zIndex: o.options.zIndex - 1,
			opacity: 1
		})
	}, s.prototype.setHeight = function () {
		if (1 === this.options.slidesToShow && !0 === this.options.adaptiveHeight && !1 === this.options.vertical) {
			var t = this.$slides.eq(this.currentSlide).outerHeight(!0);
			this.$list.css("height", t)
		}
	}, s.prototype.setOption = s.prototype.slickSetOption = function () {
		var t, e, i, o, n, s = this,
			r = !1;
		if ("object" === c.type(arguments[0]) ? (i = arguments[0], r = arguments[1], n = "multiple") : "string" === c.type(arguments[0]) && (i = arguments[0], o = arguments[1], r = arguments[2], "responsive" === arguments[0] && "array" === c.type(arguments[1]) ? n = "responsive" : void 0 !== arguments[1] && (n = "single")), "single" === n) s.options[i] = o;
		else if ("multiple" === n) c.each(i, function (t, e) {
			s.options[t] = e
		});
		else if ("responsive" === n)
			for (e in o)
				if ("array" !== c.type(s.options.responsive)) s.options.responsive = [o[e]];
				else {
					for (t = s.options.responsive.length - 1; 0 <= t;) s.options.responsive[t].breakpoint === o[e].breakpoint && s.options.responsive.splice(t, 1), t--;
					s.options.responsive.push(o[e])
				}
		r && (s.unload(), s.reinit())
	}, s.prototype.setPosition = function () {
		this.setDimensions(), this.setHeight(), !1 === this.options.fade ? this.setCSS(this.getLeft(this.currentSlide)) : this.setFade(), this.$slider.trigger("setPosition", [this])
	}, s.prototype.setProps = function () {
		var t = this,
			e = document.body.style;
		t.positionProp = !0 === t.options.vertical ? "top" : "left", "top" === t.positionProp ? t.$slider.addClass("slick-vertical") : t.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === t.options.useCSS && (t.cssTransitions = !0), t.options.fade && ("number" == typeof t.options.zIndex ? t.options.zIndex < 3 && (t.options.zIndex = 3) : t.options.zIndex = t.defaults.zIndex), void 0 !== e.OTransform && (t.animType = "OTransform", t.transformType = "-o-transform", t.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), void 0 !== e.MozTransform && (t.animType = "MozTransform", t.transformType = "-moz-transform", t.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (t.animType = !1)), void 0 !== e.webkitTransform && (t.animType = "webkitTransform", t.transformType = "-webkit-transform", t.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), void 0 !== e.msTransform && (t.animType = "msTransform", t.transformType = "-ms-transform", t.transitionType = "msTransition", void 0 === e.msTransform && (t.animType = !1)), void 0 !== e.transform && !1 !== t.animType && (t.animType = "transform", t.transformType = "transform", t.transitionType = "transition"), t.transformsEnabled = t.options.useTransform && null !== t.animType && !1 !== t.animType
	}, s.prototype.setSlideClasses = function (t) {
		var e, i, o, n, s = this;
		if (i = s.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), s.$slides.eq(t).addClass("slick-current"), !0 === s.options.centerMode) {
			var r = s.options.slidesToShow % 2 == 0 ? 1 : 0;
			e = Math.floor(s.options.slidesToShow / 2), !0 === s.options.infinite && (e <= t && t <= s.slideCount - 1 - e ? s.$slides.slice(t - e + r, t + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = s.options.slidesToShow + t, i.slice(o - e + 1 + r, o + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === t ? i.eq(i.length - 1 - s.options.slidesToShow).addClass("slick-center") : t === s.slideCount - 1 && i.eq(s.options.slidesToShow).addClass("slick-center")), s.$slides.eq(t).addClass("slick-center")
		} else 0 <= t && t <= s.slideCount - s.options.slidesToShow ? s.$slides.slice(t, t + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= s.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (n = s.slideCount % s.options.slidesToShow, o = !0 === s.options.infinite ? s.options.slidesToShow + t : t, s.options.slidesToShow == s.options.slidesToScroll && s.slideCount - t < s.options.slidesToShow ? i.slice(o - (s.options.slidesToShow - n), o + n).addClass("slick-active").attr("aria-hidden", "false") : i.slice(o, o + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
		"ondemand" !== s.options.lazyLoad && "anticipated" !== s.options.lazyLoad || s.lazyLoad()
	}, s.prototype.setupInfinite = function () {
		var t, e, i, o = this;
		if (!0 === o.options.fade && (o.options.centerMode = !1), !0 === o.options.infinite && !1 === o.options.fade && (e = null, o.slideCount > o.options.slidesToShow)) {
			for (i = !0 === o.options.centerMode ? o.options.slidesToShow + 1 : o.options.slidesToShow, t = o.slideCount; t > o.slideCount - i; t -= 1) e = t - 1, c(o.$slides[e]).clone(!0).attr("id", "").attr("data-slick-index", e - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
			for (t = 0; t < i + o.slideCount; t += 1) e = t, c(o.$slides[e]).clone(!0).attr("id", "").attr("data-slick-index", e + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
			o.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
				c(this).attr("id", "")
			})
		}
	}, s.prototype.interrupt = function (t) {
		t || this.autoPlay(), this.interrupted = t
	}, s.prototype.selectHandler = function (t) {
		var e = c(t.target).is(".slick-slide") ? c(t.target) : c(t.target).parents(".slick-slide"),
			i = parseInt(e.attr("data-slick-index"));
		return i || (i = 0), this.slideCount <= this.options.slidesToShow ? void this.slideHandler(i, !1, !0) : void this.slideHandler(i)
	}, s.prototype.slideHandler = function (t, e, i) {
		var o, n, s, r, a, l = null,
			c = this;
		if (e = e || !1, !(!0 === c.animating && !0 === c.options.waitForAnimate || !0 === c.options.fade && c.currentSlide === t)) return !1 === e && c.asNavFor(t), o = t, l = c.getLeft(o), r = c.getLeft(c.currentSlide), c.currentLeft = null === c.swipeLeft ? r : c.swipeLeft, !1 === c.options.infinite && !1 === c.options.centerMode && (t < 0 || t > c.getDotCount() * c.options.slidesToScroll) ? void(!1 === c.options.fade && (o = c.currentSlide, !0 !== i && c.slideCount > c.options.slidesToShow ? c.animateSlide(r, function () {
			c.postSlide(o)
		}) : c.postSlide(o))) : !1 === c.options.infinite && !0 === c.options.centerMode && (t < 0 || t > c.slideCount - c.options.slidesToScroll) ? void(!1 === c.options.fade && (o = c.currentSlide, !0 !== i && c.slideCount > c.options.slidesToShow ? c.animateSlide(r, function () {
			c.postSlide(o)
		}) : c.postSlide(o))) : (c.options.autoplay && clearInterval(c.autoPlayTimer), n = o < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + o : o >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : o - c.slideCount : o, c.animating = !0, c.$slider.trigger("beforeChange", [c, c.currentSlide, n]), s = c.currentSlide, c.currentSlide = n, c.setSlideClasses(c.currentSlide), c.options.asNavFor && ((a = (a = c.getNavTarget()).slick("getSlick")).slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide)), c.updateDots(), c.updateArrows(), !0 === c.options.fade ? (!0 !== i ? (c.fadeSlideOut(s), c.fadeSlide(n, function () {
			c.postSlide(n)
		})) : c.postSlide(n), void c.animateHeight()) : void(!0 !== i && c.slideCount > c.options.slidesToShow ? c.animateSlide(l, function () {
			c.postSlide(n)
		}) : c.postSlide(n)))
	}, s.prototype.startLoad = function () {
		var t = this;
		!0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.hide(), t.$nextArrow.hide()), !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.hide(), t.$slider.addClass("slick-loading")
	}, s.prototype.swipeDirection = function () {
		var t, e, i, o;
		return t = this.touchObject.startX - this.touchObject.curX, e = this.touchObject.startY - this.touchObject.curY, i = Math.atan2(e, t), (o = Math.round(180 * i / Math.PI)) < 0 && (o = 360 - Math.abs(o)), o <= 45 && 0 <= o ? !1 === this.options.rtl ? "left" : "right" : o <= 360 && 315 <= o ? !1 === this.options.rtl ? "left" : "right" : 135 <= o && o <= 225 ? !1 === this.options.rtl ? "right" : "left" : !0 === this.options.verticalSwiping ? 35 <= o && o <= 135 ? "down" : "up" : "vertical"
	}, s.prototype.swipeEnd = function (t) {
		var e, i, o = this;
		if (o.dragging = !1, o.swiping = !1, o.scrolling) return o.scrolling = !1;
		if (o.interrupted = !1, o.shouldClick = !(10 < o.touchObject.swipeLength), void 0 === o.touchObject.curX) return !1;
		if (!0 === o.touchObject.edgeHit && o.$slider.trigger("edge", [o, o.swipeDirection()]), o.touchObject.swipeLength >= o.touchObject.minSwipe) {
			switch (i = o.swipeDirection()) {
				case "left":
				case "down":
					e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), o.currentDirection = 0;
					break;
				case "right":
				case "up":
					e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), o.currentDirection = 1
			}
			"vertical" != i && (o.slideHandler(e), o.touchObject = {}, o.$slider.trigger("swipe", [o, i]))
		} else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), o.touchObject = {})
	}, s.prototype.swipeHandler = function (t) {
		var e = this;
		if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== t.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = t.originalEvent && void 0 !== t.originalEvent.touches ? t.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), t.data.action) {
			case "start":
				e.swipeStart(t);
				break;
			case "move":
				e.swipeMove(t);
				break;
			case "end":
				e.swipeEnd(t)
		}
	}, s.prototype.swipeMove = function (t) {
		var e, i, o, n, s, r, a = this;
		return s = void 0 !== t.originalEvent ? t.originalEvent.touches : null, !(!a.dragging || a.scrolling || s && 1 !== s.length) && (e = a.getLeft(a.currentSlide), a.touchObject.curX = void 0 !== s ? s[0].pageX : t.clientX, a.touchObject.curY = void 0 !== s ? s[0].pageY : t.clientY, a.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2))), !a.options.verticalSwiping && !a.swiping && 4 < r ? !(a.scrolling = !0) : (!0 === a.options.verticalSwiping && (a.touchObject.swipeLength = r), i = a.swipeDirection(), void 0 !== t.originalEvent && 4 < a.touchObject.swipeLength && (a.swiping = !0, t.preventDefault()), n = (!1 === a.options.rtl ? 1 : -1) * (a.touchObject.curX > a.touchObject.startX ? 1 : -1), !0 === a.options.verticalSwiping && (n = a.touchObject.curY > a.touchObject.startY ? 1 : -1), o = a.touchObject.swipeLength, (a.touchObject.edgeHit = !1) === a.options.infinite && (0 === a.currentSlide && "right" === i || a.currentSlide >= a.getDotCount() && "left" === i) && (o = a.touchObject.swipeLength * a.options.edgeFriction, a.touchObject.edgeHit = !0), !1 === a.options.vertical ? a.swipeLeft = e + o * n : a.swipeLeft = e + o * (a.$list.height() / a.listWidth) * n, !0 === a.options.verticalSwiping && (a.swipeLeft = e + o * n), !0 !== a.options.fade && !1 !== a.options.touchMove && (!0 === a.animating ? (a.swipeLeft = null, !1) : void a.setCSS(a.swipeLeft))))
	}, s.prototype.swipeStart = function (t) {
		var e, i = this;
		return i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow ? !(i.touchObject = {}) : (void 0 !== t.originalEvent && void 0 !== t.originalEvent.touches && (e = t.originalEvent.touches[0]), i.touchObject.startX = i.touchObject.curX = void 0 !== e ? e.pageX : t.clientX, i.touchObject.startY = i.touchObject.curY = void 0 !== e ? e.pageY : t.clientY, void(i.dragging = !0))
	}, s.prototype.unfilterSlides = s.prototype.slickUnfilter = function () {
		null !== this.$slidesCache && (this.unload(), this.$slideTrack.children(this.options.slide).detach(), this.$slidesCache.appendTo(this.$slideTrack), this.reinit())
	}, s.prototype.unload = function () {
		var t = this;
		c(".slick-cloned", t.$slider).remove(), t.$dots && t.$dots.remove(), t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove(), t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove(), t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
	}, s.prototype.unslick = function (t) {
		this.$slider.trigger("unslick", [this, t]), this.destroy()
	}, s.prototype.updateArrows = function () {
		var t = this;
		Math.floor(t.options.slidesToShow / 2), !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - t.options.slidesToShow && !1 === t.options.centerMode ? (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - 1 && !0 === t.options.centerMode && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
	}, s.prototype.updateDots = function () {
		null !== this.$dots && (this.$dots.find("li").removeClass("slick-active").end(), this.$dots.find("li").eq(Math.floor(this.currentSlide / this.options.slidesToScroll)).addClass("slick-active"))
	}, s.prototype.visibility = function () {
		this.options.autoplay && (document[this.hidden] ? this.interrupted = !0 : this.interrupted = !1)
	}, c.fn.slick = function () {
		var t, e, i = arguments[0],
			o = Array.prototype.slice.call(arguments, 1),
			n = this.length;
		for (t = 0; t < n; t++)
			if ("object" == typeof i || void 0 === i ? this[t].slick = new s(this[t], i) : e = this[t].slick[i].apply(this[t].slick, o), void 0 !== e) return e;
		return this
	}
}),
function (o, n, s, t) {
	o.navigation = function (e, t) {
		o(s).ready(function () {
			i(!0), o(n).resize(function () {
				i(!1)
			})
		});

		function i(t) {
			o(n).width() <= 992 ? (o(e).addClass("navigation__portrait"), o(e).removeClass("navigation__landscape"), o(".navigation-dropdown").css({
				display: "none"
			})) : t || (o("html").removeClass("navigation__portrait"), o(".navigation-dropdown").css({
				display: "block"
			})), 992 < o(n).width() ? (o(e).addClass("navigation__landscape"), o(e).removeClass("navigation__portrait"), o(e).removeClass("offcanvas__overlay"), o("body").removeClass("scroll-prevent"), o(".navigation-wrapper").removeClass("offcanvas__is-open")) : t || o(e).removeClass("navigation__landscape"), o(n).width() <= 1280 ? o(".navigation-dropdown .navigation-dropdown").addClass("algin-to-left") : o(".navigation-dropdown .navigation-dropdown").removeClass("algin-to-left")
		}
		o(e).find(".navigation-menu__link").on("mouseenter focusin", function () {
			o(e).find(".navigation-menu__link").parent().children(".nav-submenu").stop(!0, !0).delay(0).fadeIn(300)
		}).on("mouseleave focusout", function () {
			o(e).find(".navigation-menu__link").parent().children(".nav-submenu").stop(!0, !0).delay(0).fadeOut(300)
		}), o(".navigation__toggler").on("click", function () {
			o(".navigation-wrapper").addClass("offcanvas__is-open"), o(".navigation").addClass("offcanvas__overlay"), o("body").toggleClass("scroll-prevent")
		}), o("body, .offcanvas__close, .navigation-menu__item").on("click", function () {
			o(".navigation-wrapper").removeClass("offcanvas__is-open"), o(e).removeClass("offcanvas__overlay"), o("body").removeClass("scroll-prevent")
		}), o(".navigation__toggler").on("click", function (t) {
			t.stopPropagation()
		}), o("body").on("click", ".navigation-wrapper", function (t) {
			t.stopPropagation()
		}), o("body").find(".navigation-menu__link").on("click", ".submenu-icon", function (t) {
			0 < o(".navigation__portrait").length && (t.stopPropagation(), 0 < o(".navigation-dropdown").length && (o(this).parent("a").parent(".navigation-menu__item").children(".navigation-dropdown").slideToggle(), o(this).parent("a").toggleClass("highlight"), o(this).toggleClass("submenu-icon__caret--up")))
		}), o(e).find("li").each(function () {
			0 < o(this).children(".navigation-dropdown").length && o(this).children("a").append("")
		}), o("body").on("click", ".submenu-icon", function (t) {
			t.stopPropagation()
		}), o(n).on("scroll load", function () {
			100 < o(n).scrollTop() ? o(e).addClass("sticky-nav") : o(e).removeClass("sticky-nav")
		})
	}, o.fn.navigation = function (e) {
		return this.each(function () {
			if (void 0 === o(this).data("navigation")) {
				var t = new o.navigation(this, e);
				o(this).data("navigation", t)
			}
		})
	}
}(jQuery, window, document), $(document).on("scroll", onScroll);
var currentClass = "current-menu-item";

function onScroll(t) {
	var i = $(document).scrollTop();
	$(".navigation a").each(function () {
		var t = $(this);
		if ("#" != t.attr("href")) {
			var e = $(t.attr("href"));
			e.length && (e.position().top <= i + 4 && e.position().top + e.height() > i + 4 ? ($(".navigation li a").removeClass(currentClass), t.addClass(currentClass)) : t.removeClass(currentClass))
		}
	})
}

function loadHtml5LightBox(f) {
	var w, e, W;
	(w = jQuery).fn.html5lightbox = function (t) {
		var v = this;
		v.options = w.extend({
			freelink: "http://html5box.com/",
			defaultvideovolume: 1,
			autoclose: !1,
			autoclosedelay: 0,
			resizedelay: 100,
			insideiframe: !1,
			autoresizecontent: !0,
			defaultwidth: 960,
			defaultheight: 540,
			autoplay: !0,
			loopvideo: !1,
			html5player: !0,
			responsive: !0,
			nativehtml5controls: !1,
			videohidecontrols: !1,
			nativecontrolsonfirefox: !0,
			nativecontrolsonie: !0,
			nativecontrolsonandroid: !0,
			imagekeepratio: !0,
			maxheight: !1,
			elemautoheight: !1,
			useflashonie9: !0,
			useflashonie10: !0,
			useflashonie11: !1,
			useflashformp4onfirefox: !1,
			transition: "none",
			transitionduration: 400,
			enablepdfjs: !0,
			pdfjsengine: "",
			openpdfinnewtaboniphone: !1,
			openpdfinnewtabonipad: !1,
			googleanalyticsaccount: "",
			arrowloop: !0,
			showall: !1,
			userelforgroup: !0,
			shownavigation: !0,
			thumbwidth: 96,
			thumbheight: 72,
			thumbgap: 4,
			thumbtopmargin: 12,
			thumbbottommargin: 12,
			thumbborder: 1,
			thumbbordercolor: "transparent",
			thumbhighlightbordercolor: "#fff",
			thumbopacity: 1,
			navbuttonwidth: 32,
			navbgcolor: "rgba(0,0,0,0.2)",
			shownavcontrol: !0,
			navcontrolimage: "lightbox-navcontrol.png",
			hidenavdefault: !1,
			overlaybgcolor: "#000",
			overlayopacity: .9,
			bgcolor: "#fff",
			bordersize: 8,
			borderradius: 0,
			bordermargin: 16,
			bordertopmargin: 48,
			bordertopmarginsmall: 48,
			barautoheight: !0,
			barheight: 64,
			responsivebarheight: !1,
			smallscreenheight: 415,
			barheightonsmallheight: 64,
			notkeepratioonsmallheight: !1,
			loadingwidth: 64,
			loadingheight: 64,
			resizespeed: 400,
			fadespeed: 400,
			jsfolder: f,
			skinsfoldername: "assets/dist/layout/player-skins/default/",
			loadingimage: "lightbox-loading.gif",
			nextimage: "lightbox-next.png",
			previmage: "lightbox-prev.png",
			closeimage: "lightbox-close.png",
			playvideoimage: "lightbox-playvideo.png",
			titlebgimage: "lightbox-titlebg.png",
			navarrowsprevimage: "lightbox-navprev.png",
			navarrowsnextimage: "lightbox-navnext.png",
			navarrowsalwaysshowontouch: !0,
			navarrowsbottomscreenwidth: 479,
			closeonoverlay: !0,
			alwaysshownavarrows: !1,
			showplaybutton: !0,
			playimage: "lightbox-play.png",
			pauseimage: "lightbox-pause.png",
			fullscreenmode: !1,
			fullscreencloseimage: "lightbox-close-fullscreen.png",
			fullscreennextimage: "lightbox-next-fullscreen.png",
			fullscreenprevimage: "lightbox-prev-fullscreen.png",
			fullscreennomargin: !1,
			fullscreenmodeonsmallscreen: !1,
			fullscreennomarginonsmallscreen: !1,
			fullscreensmallscreenwidth: 736,
			fullscreenbgcolor: "rgba(0, 0, 0, 0.9)",
			fullscreennomargintextinside: !1,
			videobgcolor: "#000",
			html5videoposter: "",
			showtitle: !0,
			titlestyle: "bottom",
			titleinsidecss: "color:#fff; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 8px;",
			titlebottomcss: "color:#333; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left;",
			showdescription: !0,
			descriptioninsidecss: "color:#fff; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",
			descriptionbottomcss: "color:#333; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",
			fullscreentitlebottomcss: "color:#fff; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 8px 8px;",
			fullscreendescriptionbottomcss: "color:#fff; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",
			showsocialmedia: !0,
			socialmediaposition: "position:absolute;top:8px;right:8px;",
			showtitleprefix: !0,
			titleprefix: "%NUM / %TOTAL",
			autoslide: !1,
			slideinterval: 5e3,
			showtimer: !0,
			timerposition: "bottom",
			timerheight: 2,
			timercolor: "#dc572e",
			timeropacity: 1,
			initvimeo: !0,
			inityoutube: !0,
			swipepreventdefaultonandroid: !1,
			initsocial: !0,
			showsocial: !1,
			socialposition: "position:absolute;top:100%;right:0;",
			socialpositionsmallscreen: "position:absolute;top:100%;right:0;left:0;",
			socialdirection: "horizontal",
			socialbuttonsize: 32,
			socialbuttonfontsize: 18,
			socialrotateeffect: !0,
			showfacebook: !0,
			showtwitter: !0,
			showpinterest: !0,
			showemail: !1,
			imagepercentage: 75,
			sidetobottomscreenwidth: 479,
			errorwidth: 280,
			errorheight: 48,
			errorcss: "text-align:center; color:#ff0000; font-size:14px; font-family:Arial, sans-serif;",
			enabletouchswipe: !0,
			mobileresizeevent: !1,
			swipedistance: 0,
			bodynoscroll: !1,
			useabsolutepos: !1,
			useabsoluteposonmobile: !1,
			supportesckey: !0,
			supportarrowkeys: !0,
			version: "3.3",
			stamp: !0,
			freemark: "72,84,77,76,53,32,76,105,103,104,116,98,111,120,32,70,114,101,101,32,86,101,114,115,105,111,110",
			watermark: "",
			watermarklink: ""
		}, t), "undefined" != typeof html5lightbox_options && html5lightbox_options && w.extend(v.options, html5lightbox_options), w("div.html5lightbox_options").length && w.each(w("div.html5lightbox_options").data(), function (t, e) {
			v.options[t.toLowerCase()] = e
		}), w("div#html5lightbox_options").length && w.each(w("div#html5lightbox_options").data(), function (t, e) {
			v.options[t.toLowerCase()] = e
		}), w("div#html5lightbox_general_options").length && w.each(w("div#html5lightbox_general_options").data(), function (t, e) {
			v.options[t.toLowerCase()] = e
		});
		var b = v.options.defaultwidth,
			y = v.options.defaultheight;
		v.options.types = ["IMAGE", "FLASH", "VIDEO", "YOUTUBE", "VIMEO", "PDF", "MP3", "WEB", "FLV", "DAILYMOTION", "DIV", "WISTIA", "IFRAMEVIDEO"], v.options.htmlfolder = window.location.href.substr(0, window.location.href.lastIndexOf("/") + 1), v.options.skinsfolder = v.options.skinsfoldername, 0 < v.options.skinsfolder.length && "/" != v.options.skinsfolder[v.options.skinsfolder.length - 1] && (v.options.skinsfolder += "/"), "/" != v.options.skinsfolder.charAt(0) && "http:" != v.options.skinsfolder.substring(0, 5) && "https:" != v.options.skinsfolder.substring(0, 6) && (v.options.skinsfolder = v.options.jsfolder + v.options.skinsfolder);
		for (var e, i = ["loadingimage", "nextimage", "previmage", "closeimage", "playvideoimage", "titlebgimage", "navarrowsprevimage", "navarrowsnextimage", "navcontrolimage", "playimage", "pauseimage", "fullscreencloseimage", "fullscreennextimage", "fullscreenprevimage"], o = 0; o < i.length; o++) v.options[i[o]] && "http://" != v.options[i[o]].substring(0, 7).toLowerCase() && "https://" != v.options[i[o]].substring(0, 8).toLowerCase() && (v.options[i[o]] = v.options.skinsfolder + v.options[i[o]]);
		var n = "",
			s = v.options.freemark.split(",");
		for (o = 0; o < s.length; o++) n += String.fromCharCode(s[o]);
		v.options.freemark = n;
		var r = "hmtamgli5cboxh.iclolms";
		for (o = 1; o <= 5; o++) r = r.slice(0, o) + r.slice(o + 1);
		for (e = r.length, o = 0; o < 5; o++) r = r.slice(0, e - 9 + o) + r.slice(e - 8 + o); - 1 != v.options.htmlfolder.indexOf(r) && (v.options.stamp = !1), v.options.flashInstalled = !1;
		try {
			new ActiveXObject("ShockwaveFlash.ShockwaveFlash") && (v.options.flashInstalled = !0)
		} catch (t) {
			navigator.mimeTypes["application/x-shockwave-flash"] && (v.options.flashInstalled = !0)
		}
		if (v.options.html5VideoSupported = !!document.createElement("video").canPlayType, v.options.isChrome = null != navigator.userAgent.match(/Chrome/i), v.options.isFirefox = null != navigator.userAgent.match(/Firefox/i), v.options.isOpera = null != navigator.userAgent.match(/Opera/i) || null != navigator.userAgent.match(/OPR\//i), v.options.isSafari = null != navigator.userAgent.match(/Safari/i), v.options.isIE11 = null != navigator.userAgent.match(/Trident\/7/) && null != navigator.userAgent.match(/rv:11/), v.options.isIE = null != navigator.userAgent.match(/MSIE/i) && !v.options.isOpera, v.options.isIE10 = null != navigator.userAgent.match(/MSIE 10/i) && !this.options.isOpera, v.options.isIE9 = null != navigator.userAgent.match(/MSIE 9/i) && !v.options.isOpera, v.options.isIE8 = null != navigator.userAgent.match(/MSIE 8/i) && !v.options.isOpera, v.options.isIE7 = null != navigator.userAgent.match(/MSIE 7/i) && !v.options.isOpera, v.options.isIE6 = null != navigator.userAgent.match(/MSIE 6/i) && !v.options.isOpera, v.options.isIE678 = v.options.isIE6 || v.options.isIE7 || v.options.isIE8, v.options.isIE6789 = v.options.isIE6 || v.options.isIE7 || v.options.isIE8 || v.options.isIE9, v.options.isAndroid = null != navigator.userAgent.match(/Android/i), v.options.isIPad = null != navigator.userAgent.match(/iPad/i), v.options.isIPhone = null != navigator.userAgent.match(/iPod/i) || null != navigator.userAgent.match(/iPhone/i), v.options.isIOS = v.options.isIPad || v.options.isIPhone, v.options.isMobile = v.options.isAndroid || v.options.isIPad || v.options.isIPhone, v.options.isIOSLess5 = v.options.isIPad && v.options.isIPhone && (null != navigator.userAgent.match(/OS 4/i) || null != navigator.userAgent.match(/OS 3/i)), v.options.supportCSSPositionFixed = !v.options.isIE6 && !v.options.isIOSLess5, v.options.iequirksmode = v.options.isIE6789 && "CSS1Compat" != document.compatMode, v.options.isTouch = "ontouchstart" in window, v.options.isAndroid) {
			var a = navigator.userAgent.match(/Android\s([0-9\.]*)/i);
			v.options.androidVersion = a && 2 <= a.length ? parseInt(a[1], 10) : -1
		}
		var l, c, d = document.createElement("video");
		v.options.canplaymp4 = d && d.canPlayType && d.canPlayType("video/mp4").replace(/no/, ""), v.options.isMobile && (v.options.autoplay = !1), (v.options.isFirefox && v.options.nativecontrolsonfirefox || v.options.isAndroid && v.options.nativecontrolsonandroid) && (v.options.nativehtml5controls = !0), (v.options.isIE6789 || v.options.isIE10 || v.options.isIE11) && v.options.nativecontrolsonie && (v.options.nativehtml5controls = !0), v.options.navheight = 0, v.options.thumbgap += 2 * v.options.thumbborder, v.options.resizeTimeout = -1, v.slideTimeout = null, v.autosliding = !1, v.existingElem = -1, v.direction = -3, v.elemArray = new Array, v.options.curElem = -1, v.defaultoptions = w.extend({}, v.options), v.options.googleanalyticsaccount && !window._gaq && (window._gaq = window._gaq || [], window._gaq.push(["_setAccount", v.options.googleanalyticsaccount]), window._gaq.push(["_trackPageview"]), w.getScript("https://ssl.google-analytics.com/ga.js")), v.options.initvimeo && ((l = document.createElement("script")).src = v.options.jsfolder + "", (c = document.getElementsByTagName("script")[0]).parentNode.insertBefore(l, c)), v.options.inityoutube && ((l = document.createElement("script")).src = "https://www.youtube.com/iframe_api", (c = document.getElementsByTagName("script")[0]).parentNode.insertBefore(l, c)), v.options.initsocial && w("head").append('<link rel="stylesheet" href="' + v.options.jsfolder + '" type="text/css" />'), v.showing = !1, v.navvisible = !1, v.disableEscKey = function (t) {
			t ? v.disableesckeyinfullscreen = !0 : setTimeout(function () {
				v.disableesckeyinfullscreen = !1
			}, 1e3)
		}, v.supportKeyboard = function () {
			v.disableesckeyinfullscreen = !1, w(document).keyup(function (t) {
				v.showing && (!v.disableesckeyinfullscreen && v.options.supportesckey && 27 == t.keyCode ? v.finish() : v.options.supportarrowkeys && (39 == t.keyCode ? v.gotoSlide(-1) : 37 == t.keyCode && v.gotoSlide(-2)))
			}), v.options.supportesckey && (document.addEventListener("MSFullscreenChange", function () {
				v.disableEscKey(null != document.msFullscreenElement)
			}, !1), document.addEventListener("webkitfullscreenchange", function () {
				v.disableEscKey(document.webkitIsFullScreen)
			}, !1))
		}, v.supportKeyboard(), v.init = function () {
			v.showing = !1, v.readData(), v.createMarkup(), v.initSlide()
		}, v.readData = function () {
			v.each(function () {
				if ("a" == this.nodeName.toLowerCase() || "area" == this.nodeName.toLowerCase()) {
					var t = w(this),
						e = "mediatype" in t.data() ? t.data("mediatype") : v.checkType(t.attr("href"));
					if (!(e < 0)) {
						for (var i = t.data("title") ? t.data("title") : t.attr("title"), o = t.data("group") ? t.data("group") : v.options.userelforgroup ? t.attr("rel") : null, n = 0; n < v.elemArray.length; n++)
							if (t.attr("href") == v.elemArray[n][1]) return v.elemArray[n][2] = i, void(v.elemArray[n][3] = o);
						v.elemArray.push(new Array(e, t.attr("href"), i, o, t.data("width"), t.data("height"), t.data("webm"), t.data("ogg"), t.data("thumbnail"), t.data("description"), null, null, null, t.data("socialmedia")))
					}
				}
			})
		}, v.createMarkup = function () {
			w(window).width() <= v.options.fullscreensmallscreenwidth && (v.options.fullscreenmodeonsmallscreen && (v.options.fullscreenmode = !0, v.options.fullscreennomarginonsmallscreen && (v.options.fullscreennomargin = !0)), v.options.fullscreenmode && v.options.fullscreennomarginonsmallscreen && (v.options.fullscreennomargin = !0)), v.options.fullscreenmode && v.options.fullscreennomargin && (v.options.bgcolor = v.options.fullscreenbgcolor, v.options.bordersize = 0, v.options.bordermargin = 0, v.options.bordertopmargin = 0, v.options.bordertopmarginsmall = 0, v.options.fullscreennomargintextinside ? v.options.titlestyle = "inside" : (v.options.titlebottomcss = v.options.fullscreentitlebottomcss, v.options.descriptionbottomcss = v.options.fullscreendescriptionbottomcss)), v.options.barheightoriginal = v.options.barheight, v.options.responsivebarheight && w(window).height() <= v.options.smallscreenheight && (v.options.barheight = v.options.barheightonsmallheight), v.options.titlecss || (v.options.titlecss = "inside" == v.options.titlestyle ? v.options.titleinsidecss : v.options.titlebottomcss), v.options.descriptioncss || (v.options.descriptioncss = "inside" == v.options.titlestyle ? v.options.descriptioninsidecss : v.options.descriptionbottomcss), v.options.titlecss = w.trim(v.options.titlecss), 1 < v.options.titlecss.length && ("{" == v.options.titlecss.charAt(0) && (v.options.titlecss = v.options.titlecss.substring(1)), "}" == v.options.titlecss.charAt(v.options.titlecss.length - 1) && (v.options.titlecss = v.options.titlecss.substring(0, v.options.titlecss.length - 1))), v.options.descriptioncss = w.trim(v.options.descriptioncss), 1 < v.options.descriptioncss.length && ("{" == v.options.descriptioncss.charAt(0) && (v.options.descriptioncss = v.options.descriptioncss.substring(1)), "}" == v.options.descriptioncss.charAt(v.options.descriptioncss.length - 1) && (v.options.descriptioncss = v.options.descriptioncss.substring(0, v.options.descriptioncss.length - 1))), v.options.errorcss = w.trim(v.options.errorcss), 1 < v.options.errorcss.length && ("{" == v.options.errorcss.charAt(0) && (v.options.errorcss = v.options.errorcss.substring(1)), "}" == v.options.errorcss.charAt(v.options.errorcss.length - 1) && (v.options.errorcss = v.options.errorcss.substring(0, v.options.errorcss.length - 1)));
			var t = ".bodynoscroll {height:100%;overflow:hidden;}";
			t += ".html5-hide {display:none !important;} #html5box-html5-lightbox .html5-text {" + v.options.titlecss + "}", t += "#html5box-html5-lightbox .html5-description {" + v.options.descriptioncss + "}", t += "#html5box-html5-lightbox .html5-error {" + v.options.errorcss + "}", (v.options.navarrowsalwaysshowontouch || v.options.alwaysshownavarrows) && (t += "#html5box-html5-lightbox .html5-prev-touch {left:0px;top:50%;margin-top:-16px;margin-left:-32px;} #html5box-html5-lightbox .html5-next-touch {right:0px;top:50%;margin-top:-16px;margin-right:-32px;}", t += "@media (max-width: " + v.options.navarrowsbottomscreenwidth + "px) { #html5box-html5-lightbox .html5-prev-touch {top:100%;left:0;margin:0;} #html5box-html5-lightbox .html5-next-touch {top:100%;right:0;margin:0;} }"), t += "#html5box-html5-lightbox .html5-prev-fullscreen {display:block;} #html5box-html5-lightbox .html5-next-fullscreen {display:block;} #html5box-html5-lightbox .html5-prev-bottom-fullscreen {display:none;} #html5box-html5-lightbox .html5-next-bottom-fullscreen {display:none;}", t += "@media (max-width: " + v.options.navarrowsbottomscreenwidth + "px) {#html5box-html5-lightbox .html5-prev-fullscreen {display:none;} #html5box-html5-lightbox .html5-next-fullscreen {display:none;} #html5box-html5-lightbox .html5-prev-bottom-fullscreen {display:block;} #html5box-html5-lightbox .html5-next-bottom-fullscreen {display:block;} }", "right" == v.options.titlestyle ? (t += "#html5box-html5-lightbox .html5-elem-wrap {width:" + v.options.imagepercentage + "%;height:100%;} #html5box-html5-lightbox .html5-elem-data-box {min-height:100%;}", t += "@media (max-width: " + v.options.sidetobottomscreenwidth + "px) {#html5box-html5-lightbox .html5-elem-wrap {width:100%;height:auto;} #html5box-html5-lightbox .html5-elem-data-box {width:100%;height:auto;min-height:0;}}") : "left" == v.options.titlestyle && (t += "#html5box-html5-lightbox .html5-elem-wrap {height:100%;} #html5box-html5-lightbox .html5-elem-data-box {width:" + String(100 - v.options.imagepercentage) + "%;min-height:100%;}", t += "@media (max-width: " + v.options.sidetobottomscreenwidth + "px) {#html5box-html5-lightbox .html5-elem-wrap {width:100%;height:auto;} #html5box-html5-lightbox .html5-elem-data-box {width:100%;height:auto;min-height:0;}}"), t += ".html5-rotate { border-radius:50%; -webkit-transition:-webkit-transform .4s ease-in; transition: transform .4s ease-in; } .html5-rotate:hover { -webkit-transform: rotate(360deg); transform: rotate(360deg); }", t += "@media (max-width: " + v.options.navarrowsbottomscreenwidth + "px) {#html5-social {" + v.options.socialpositionsmallscreen + "}}", w("head").append("<style type='text/css' data-creator='html5box-html5-lightbox'>" + t + "</style>");
			var e = v.options.elemautoheight ? "auto" : "100%";
			v.$lightbox = w("<div id='html5box-html5-lightbox' style='display:none;top:0px;left:0px;width:100%;height:100%;z-index:9999998;text-align:center;'><div id='html5-lightbox-overlay' style='display:block;position:absolute;top:0px;left:0px;width:100%;min-height:100%;background-color:" + v.options.overlaybgcolor + ";opacity:" + v.options.overlayopacity + ";filter:alpha(opacity=" + Math.round(100 * v.options.overlayopacity) + ");'></div><div id='html5-lightbox-box' style='display:block;position:relative;margin:0px auto;'><div class='html5-elem-box' style='display:block;position:relative;width:100%;overflow-x:hidden;overflow-y:auto;height:" + e + ";margin:0px auto;text-align:center;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;'><div class='html5-elem-wrap' style='display:block;position:relative;margin:0px auto;text-align:center;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;background-color:" + v.options.bgcolor + ";'><div class='html5-loading' style='display:none;position:absolute;top:0px;left:0px;text-align:center;width:100%;height:100%;background:url(\"" + v.options.loadingimage + "\") no-repeat center center;'></div><div class='html5-error-box html5-error' style='display:none;position:absolute;padding:" + v.options.bordersize + "px;text-align:center;width:" + v.options.errorwidth + "px;height:" + v.options.errorheight + "px;'>The requested content cannot be loaded.<br />Please try again later.</div><div class='html5-image' style='display:none;position:relative;top:0px;left:0px;width:100%;height:100%;" + (v.options.iequirksmode ? "margin" : "padding") + ":" + v.options.bordersize + "px;text-align:center;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;'></div></div></div><div id='' style='display:none;position:absolute;left:" + String(v.options.bordersize + 2) + "px;top:" + String(v.options.bordersize + 2) + "px;'></div></div></div>"), v.options.positionFixed = v.options.supportCSSPositionFixed && v.options.responsive && !v.options.iequirksmode, (v.options.useabsolutepos || v.options.useabsoluteposonmobile && v.options.isMobile) && (v.options.positionFixed = !1), v.options.positionFixed || (v.options.bodynoscroll = !0), v.$lightbox.css({
				position: v.options.positionFixed ? "fixed" : "absolute"
			}), v.$lightbox.appendTo("body"), v.$lightboxBox = w("#html5-lightbox-box", v.$lightbox), v.$elem = w(".html5-elem-box", v.$lightbox), v.$elemWrap = w(".html5-elem-wrap", v.$lightbox), v.$loading = w(".html5-loading", v.$lightbox), v.$error = w(".html5-error-box", v.$lightbox), v.$image = w(".html5-image", v.$lightbox), v.options.fullscreenmode && v.options.fullscreennomargin && v.$elem.css({
				overflow: "hidden"
			});
			var i = "<div class='html5-elem-data-box' style='display:none;box-sizing:border-box;'></div>";
			if ("left" == v.options.titlestyle ? v.$elem.prepend(i) : v.$elem.append(i), v.$elemData = w(".html5-elem-data-box", v.$lightbox), v.$text = w(".html5-text", v.$lightbox), 0 < v.options.borderradius && (v.$elem.css({
					"border-radius": v.options.borderradius + "px",
					"-moz-border-radius": v.options.borderradius + "px",
					"-webkit-border-radius": v.options.borderradius + "px"
				}), "inside" == v.options.titlestyle ? v.$elemWrap.css({
					"border-radius": v.options.borderradius + "px",
					"-moz-border-radius": v.options.borderradius + "px",
					"-webkit-border-radius": v.options.borderradius + "px"
				}) : "bottom" == v.options.titlestyle && (v.$elemWrap.css({
					"border-top-left-radius": v.options.borderradius + "px",
					"-moz-top-left-border-radius": v.options.borderradius + "px",
					"-webkit-top-left-border-radius": v.options.borderradius + "px",
					"border-top-right-radius": v.options.borderradius + "px",
					"-moz-top-right-border-radius": v.options.borderradius + "px",
					"-webkit-top-right-border-radius": v.options.borderradius + "px"
				}), v.$elemData.css({
					"border-bottom-left-radius": v.options.borderradius + "px",
					"-moz-top-bottom-border-radius": v.options.borderradius + "px",
					"-webkit-bottom-left-border-radius": v.options.borderradius + "px",
					"border-bottom-right-radius": v.options.borderradius + "px",
					"-moz-bottom-right-border-radius": v.options.borderradius + "px",
					"-webkit-bottom-right-border-radius": v.options.borderradius + "px"
				}))), "right" == v.options.titlestyle || "left" == v.options.titlestyle ? (v.$lightboxBox.css({
					"background-color": v.options.bgcolor
				}), "right" == v.options.titlestyle ? (v.$elemWrap.css({
					position: "relative",
					float: "left"
				}), v.$elemData.css({
					position: "relative",
					overflow: "hidden",
					padding: v.options.bordersize + "px"
				})) : (v.$elemWrap.css({
					position: "relative",
					overflow: "hidden"
				}), v.$elemData.css({
					position: "relative",
					float: "left",
					padding: v.options.bordersize + "px"
				}))) : "inside" == v.options.titlestyle ? (v.$elemData.css({
					position: "absolute",
					margin: v.options.bordersize + "px",
					bottom: 0,
					left: 0,
					"background-color": "#333",
					"background-color": "rgba(51, 51, 51, 0.6)"
				}), v.$text.css({
					padding: v.options.bordersize + "px " + 2 * v.options.bordersize + "px"
				})) : (v.$elemData.css({
					position: "relative",
					width: "100%",
					height: v.options.barautoheight ? "auto" : v.options.barheight + "px",
					padding: "0 0 " + v.options.bordersize + "px 0",
					"background-color": v.options.bgcolor,
					"text-align": "left"
				}), v.options.fullscreenmode && v.options.fullscreennomargin || v.$text.css({
					margin: "0 " + v.options.bordersize + "px"
				})), v.options.showsocial) {
				var o = '<div id="html5-social" style="display:none;' + v.options.socialposition + '">',
					n = ("horizontal" == v.options.socialdirection ? "display:inline-block;" : "display:block;") + "margin:4px;",
					s = "display:table-cell;width:" + v.options.socialbuttonsize + "px;height:" + v.options.socialbuttonsize + "px;font-size:" + v.options.socialbuttonfontsize + "px;border-radius:50%;color:#fff;vertical-align:middle;text-align:center;cursor:pointer;padding:0;";
				v.options.showemail && (o += '<div class="html5-social-btn' + (v.options.socialrotateeffect ? " html5-rotate" : "") + ' html5-social-email" style="' + n + '"><div class="mh-icon-mail" style="' + s + 'background-color:#4d83ff;"></div></div>'), v.options.showfacebook && (o += '<div class="html5-social-btn' + (v.options.socialrotateeffect ? " html5-rotate" : "") + ' html5-social-facebook" style="' + n + '"><div class="mh-icon-facebook" style="' + s + 'background-color:#3b5998;"></div></div>'), v.options.showtwitter && (o += '<div class="html5-social-btn' + (v.options.socialrotateeffect ? " html5-rotate" : "") + ' html5-social-twitter" style="' + n + '"><div class="mh-icon-twitter" style="' + s + 'background-color:#03b3ee;"></div></div>'), v.options.showpinterest && (o += '<div class="html5-social-btn' + (v.options.socialrotateeffect ? " html5-rotate" : "") + ' html5-social-pinterest" style="' + n + '"><div class="mh-icon-pinterest" style="' + s + 'background-color:#c92228;"></div></div>'), o += '<div style="clear:both;"></div></div>', v.$lightboxBox.append(o), w(".html5-social-btn", v.$lightbox).click(function () {
					var t = window.location.href + (window.location.href.indexOf("?") < 0 ? "?" : "&") + "html5lightboxshare=" + encodeURIComponent(v.currentElem[1]),
						e = v.currentElem[2],
						i = v.currentElem[1];
					if (0 == v.currentElem[0]) i = v.absoluteUrl(v.currentElem[1]);
					else if (3 == v.currentElem[0]) i = "https://img.youtube.com/vi/" + v.getYoutubeId(v.currentElem[1]) + "/0.jpg";
					else {
						var o = w('.html5lightbox[href="' + v.currentElem[1] + '"]');
						if (0 < o.length)
							if (o.data("shareimage") && 0 < o.data("shareimage").length) i = v.absoluteUrl(o.data("shareimage"));
							else if (o.data("thumbnail") && 0 < o.data("thumbnail").length) i = v.absoluteUrl(o.data("thumbnail"));
						else {
							var n = w("img", o);
							0 < n.length && (i = v.absoluteUrl(n.attr("src")))
						}
					}
					var s = 2 == v.currentElem[0] || 3 == v.currentElem[0] || 4 == v.currentElem[0] || 8 == v.currentElem[0] || 9 == v.currentElem[0] || 11 == v.currentElem[0] || 12 == v.currentElem[0];
					return e = e ? v.html2Text(e) : "", w(this).hasClass("html5-social-facebook") ? window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(t) + "&t=" + encodeURIComponent(e), "_blank") : w(this).hasClass("html5-social-twitter") ? window.open("https://twitter.com/share?url=" + encodeURIComponent(t) + "&text=" + encodeURIComponent(e), "_blank") : w(this).hasClass("html5-social-pinterest") ? window.open("https://pinterest.com/pin/create/bookmarklet/?media=" + encodeURIComponent(i) + "&url=" + encodeURIComponent(t) + "&description=" + encodeURIComponent(e) + "&is_video=" + (s ? "true" : "false"), "_blank") : w(this).hasClass("html5-social-email") && window.open("mailto:?subject=" + encodeURIComponent(e) + "&body=Check out this: " + encodeURIComponent(t)), !1
				})
			}
			if (v.options.fullscreenmode ? (v.$lightbox.append("<div class='html5-next-fullscreen' style='cursor:pointer;position:absolute;right:" + v.options.bordersize + "px;top:50%;margin-top:-16px;'><img alt='' src='" + v.options.fullscreennextimage + "'></div><div class='html5-prev-fullscreen' style='cursor:pointer;position:absolute;left:" + v.options.bordersize + "px;top:50%;margin-top:-16px;'><img alt='' src='" + v.options.fullscreenprevimage + "'></div>"), v.$next = w(".html5-next-fullscreen", v.$lightbox), v.$prev = w(".html5-prev-fullscreen", v.$lightbox), v.$lightboxBox.append("<div class='html5-next-bottom-fullscreen' style='cursor:pointer;position:absolute;top:100%;right:0;margin-top:8px;'><img alt='' src='" + v.options.fullscreennextimage + "'></div><div class='html5-prev-bottom-fullscreen' style='cursor:pointer;position:absolute;top:100%;left:0;margin-top:8px;'><img alt='' src='" + v.options.fullscreenprevimage + "'></div>"), v.$nextbottom = w(".html5-next-bottom-fullscreen", v.$lightbox), v.$prevbottom = w(".html5-prev-bottom-fullscreen", v.$lightbox), v.$nextbottom.click(function () {
					v.nextArrowClicked()
				}), v.$prevbottom.click(function () {
					v.prevArrowClicked()
				}), v.$lightbox.append("<div id='html5-close-fullscreen' style='display:block;cursor:pointer;position:absolute;top:0;right:0;margin-top:0;margin-right:0;'><img alt='' src='" + v.options.fullscreencloseimage + "'></div>"), v.$close = w("#html5-close-fullscreen", v.$lightbox)) : (v.$lightboxBox.append("<div class='html5-next' style='display:none;cursor:pointer;position:absolute;right:" + v.options.bordersize + "px;top:50%;margin-top:-32px;'><img alt='' src='" + v.options.nextimage + "'></div><div class='html5-prev' style='display:none;cursor:pointer;position:absolute;left:" + v.options.bordersize + "px;top:50%;margin-top:-32px;'><img alt='' src='" + v.options.previmage + "'></div>"), v.$next = w(".html5-next", v.$lightbox), v.$prev = w(".html5-prev", v.$lightbox), (v.options.isTouch && v.options.navarrowsalwaysshowontouch || v.options.alwaysshownavarrows) && (v.$lightboxBox.append("<div class='html5-next-touch' style='display:block;cursor:pointer;position:absolute;'><img alt='' src='" + v.options.nextimage + "'></div><div class='html5-prev-touch' style='display:block;cursor:pointer;position:absolute;'><img alt='' src='" + v.options.previmage + "'></div>"), v.$nexttouch = w(".html5-next-touch", v.$lightbox), v.$prevtouch = w(".html5-prev-touch", v.$lightbox), v.$nexttouch.click(function () {
					v.nextArrowClicked()
				}), v.$prevtouch.click(function () {
					v.prevArrowClicked()
				})), v.$lightboxBox.append("<div id='html5-close' style='display:none;cursor:pointer;position:absolute;top:0;right:0;margin-top:-16px;margin-right:-16px;'><img alt='' src='" + v.options.closeimage + "'></div>"), v.$close = w("#html5-close", v.$lightbox)), v.$watermark = w("#html5-watermark", v.$lightbox), v.options.stamp) v.$watermark.html("<a href='" + v.options.freelink + "' style='text-decoration:none;' title='jQuery Lightbox'><div style='display:block;width:170px;height:20px;text-align:center;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#fff;color:#333;font:12px Arial,sans-serif;'><div style='line-height:20px;'>" + v.options.freemark + "</div></div></a>");
			else if (v.options.watermark) {
				var r = "<img alt='' src='" + v.options.watermark + "' style='border:none;' />";
				v.options.watermarklink && (r = "<a href='" + v.options.watermarklink + "' target='_blank'>" + r + "</a>"), v.$watermark.html(r)
			}
			v.options.closeonoverlay && w("#html5-lightbox-overlay", v.$lightbox).click(v.finish), v.$close.click(v.finish), v.$next.click(function () {
				v.nextArrowClicked()
			}), v.$prev.click(function () {
				v.prevArrowClicked()
			}), w(window).resize(function () {
				v.options.isIOS && !v.options.mobileresizeevent || (clearTimeout(v.options.resizeTimeout), v.options.resizeTimeout = setTimeout(function () {
					v.resizeWindow()
				}, v.options.resizedelay))
			}), w(window).scroll(function () {
				v.options.isIOS && !v.options.mobileresizeevent || v.scrollBox()
			}), w(window).on("orientationchange", function (t) {
				v.options.isMobile && v.resizeWindow()
			}), v.options.enabletouchswipe && v.enableSwipe()
		}, v.html2Text = function (t) {
			var e = document.createElement("div");
			return e.innerHTML = t, e.innerText
		}, v.slideTimer = function (t, e, i) {
			var o = this;
			o.timeout = t;
			var n = null,
				s = 0,
				r = !1,
				a = !1,
				l = !1;
			return this.pause = function () {
				a && (r = !0, clearInterval(n))
			}, this.resume = function (t) {
				l && !t || (l = !1, a && r && (r = !1, n = setInterval(function () {
					(s += 50) > o.timeout && (clearInterval(n), e && e()), i && i(s / o.timeout)
				}, 50)))
			}, this.stop = function () {
				clearInterval(n), i && i(-1), s = 0, a = r = !1
			}, this.start = function () {
				s = 0, a = !(r = !1), n = setInterval(function () {
					(s += 50) > o.timeout && (clearInterval(n), e && e()), i && i(s / o.timeout)
				}, 50)
			}, this.startandpause = function () {
				l = a = r = !(s = 0)
			}, this
		}, v.updateTimer = function (t) {
			var e = Math.round(100 * t);
			100 < e && (e = 100), e < 0 && (e = 0), w(".html5-timer", v.$lightbox).css({
				display: "block",
				width: e + "%"
			})
		}, v.initSlide = function () {
			v.autosliding = !1, v.slideTimeout = v.slideTimer(v.options.slideinterval, function () {
				v.gotoSlide(-1)
			}, v.options.showtimer ? function (t) {
				v.updateTimer(t)
			} : null), v.options.autoslide && (v.slideTimeout.stop(), v.autosliding = !0)
		}, v.nextArrowClicked = function () {
			v.options.nextElem <= v.options.curElem && v.options.onlastarrowclicked && window[v.options.onlastarrowclicked] && "function" == typeof window[v.options.onlastarrowclicked] && window[v.options.onlastarrowclicked](), v.gotoSlide(-1)
		}, v.prevArrowClicked = function () {
			v.options.prevElem >= v.options.curElem && v.options.onfirstarrowclicked && window[v.options.onfirstarrowclicked] && "function" == typeof window[v.options.onfirstarrowclicked] && window[v.options.onfirstarrowclicked](), v.gotoSlide(-2)
		}, v.calcNextPrevElem = function () {
			v.options.nextElem = -1, v.options.prevElem = -1, v.options.inGroup = !1, v.options.groupIndex = 0, v.options.groupCount = 0;
			for (var t = v.elemArray[v.options.curElem][3], e = 0; e < v.elemArray.length; e++) v.matchGroup(t, v.elemArray[e][3]) && (e == v.options.curElem && (v.options.groupIndex = v.options.groupCount), v.options.groupCount++);
			var i, o = v.elemArray[v.options.curElem][3];
			if (null != o && null != o) {
				for (i = v.options.curElem + 1; i < v.elemArray.length; i++)
					if (v.matchGroup(o, v.elemArray[i][3])) {
						v.options.nextElem = i;
						break
					}
				if (v.options.nextElem < 0)
					for (i = 0; i < v.options.curElem; i++)
						if (v.matchGroup(o, v.elemArray[i][3])) {
							v.options.nextElem = i;
							break
						}
				if (0 <= v.options.nextElem) {
					for (i = v.options.curElem - 1; 0 <= i; i--)
						if (v.matchGroup(o, v.elemArray[i][3])) {
							v.options.prevElem = i;
							break
						}
					if (v.options.prevElem < 0)
						for (i = v.elemArray.length - 1; i > v.options.curElem; i--)
							if (v.matchGroup(o, v.elemArray[i][3])) {
								v.options.prevElem = i;
								break
							}
				}
			}(0 <= v.options.nextElem || 0 <= v.options.prevElem) && (v.options.inGroup = !0)
		}, v.calcBoxPosition = function (t, e) {
			var i = t + 2 * v.options.bordersize,
				o = e + 2 * v.options.bordersize,
				n = v.options.shownavigation && v.navvisible ? v.options.navheight : 0,
				s = w(window).height(),
				r = Math.round((s - n) / 2 - o / 2);
			"bottom" == v.options.titlestyle && (r -= Math.round(v.options.barheight / 2));
			var a = w(window).width() < v.options.navarrowsbottomscreenwidth ? v.options.bordertopmarginsmall : v.options.bordertopmargin;
			if (r < a && (r = a), v.options.insideiframe && window.self != window.top && parent.window.jQuery && parent.window.jQuery("#" + v.options.iframeid).length) {
				var l = parent.window.jQuery("#" + v.options.iframeid).offset().top,
					c = parent.window.document.body.scrollTop;
				r = a, r += l < c ? c - l : 0
			}
			return [i, o, r]
		}, v.hideNavArrows = function () {
			var t = !1,
				e = !1;
			v.options.inGroup && ((v.options.arrowloop || !v.options.arrowloop && v.options.prevElem < v.options.curElem) && (t = !0), (v.options.arrowloop || !v.options.arrowloop && v.options.nextElem > v.options.curElem) && (e = !0)), t ? (v.$prev.removeClass("html5-hide"), v.$prevbottom && v.$prevbottom.removeClass("html5-hide"), v.$prevtouch && v.$prevtouch.removeClass("html5-hide")) : (v.$prev.addClass("html5-hide"), v.$prevbottom && v.$prevbottom.addClass("html5-hide"), v.$prevtouch && v.$prevtouch.addClass("html5-hide")), e ? (v.$next.removeClass("html5-hide"), v.$nextbottom && v.$nextbottom.removeClass("html5-hide"), v.$nexttouch && v.$nexttouch.removeClass("html5-hide")) : (v.$next.addClass("html5-hide"), v.$nextbottom && v.$nextbottom.addClass("html5-hide"), v.$nexttouch && v.$nexttouch.addClass("html5-hide"))
		}, v.clickHandler = function () {
			var t = w(this),
				i = {};
			if (w.each(t.data(), function (t, e) {
					i[t.toLowerCase()] = e
				}), v.options = w.extend(v.options, v.defaultoptions, i), w(window).trigger("html5lightbox.lightboxshow"), v.init(), v.elemArray.length <= 0) return !0;
			v.hideObjects();
			for (var e = 0; e < v.elemArray.length && v.elemArray[e][1] != t.attr("href"); e++);
			if (e == v.elemArray.length) return !0;
			v.options.curElem = e, v.calcNextPrevElem(), v.reset(), v.$lightbox.show();
			var o = v.calcBoxPosition(v.options.loadingwidth, v.options.loadingheight),
				n = o[0],
				s = o[1],
				r = o[2];
			return v.options.iequirksmode ? v.$lightboxBox.css({
				top: r
			}) : v.$lightboxBox.css({
				"margin-top": r
			}), "left" == v.options.titlestyle || "right" == v.options.titlestyle ? v.$lightboxBox.css({
				width: n,
				height: s
			}) : (v.$lightboxBox.css({
				width: n,
				height: "auto"
			}), v.$elemWrap.css({
				width: n,
				height: s
			})), v.loadCurElem(), !1
		}, v.loadThumbnail = function (e, i, o) {
			var t = new Image;
			w(t).on("load", function () {
				var t;
				t = this.width / this.height <= v.options.thumbwidth / v.options.thumbheight ? "width:100%;" : "height:100%;", w(".html5-nav-thumb").eq(i).html("<img alt='" + v.html2Text(o) + "' style='" + t + "' src='" + e + "' />")
			}), t.src = e
		}, v.matchGroup = function (t, e) {
			if (v.options.showall) return !0;
			if (!t || !e) return !1;
			var i = t.split(":"),
				o = e.split(":"),
				n = !1;
			for (var s in i)
				if (-1 < w.inArray(i[s], o)) {
					n = !0;
					break
				}
			return n
		}, v.showNavigation = function () {
			if (v.options.shownavigation && v.currentElem && v.currentElem[3]) {
				var t, e = !1,
					i = v.currentElem[3];
				for (t = 0; t < v.elemArray.length; t++)
					if (v.matchGroup(i, v.elemArray[t][3]) && v.elemArray[t][8] && 0 < v.elemArray[t][8].length) {
						e = !0;
						break
					}
				if (e && (v.options.navheight = v.options.thumbheight + v.options.thumbtopmargin + v.options.thumbbottommargin, !(0 < w(".html5-nav").length))) {
					var o = v.options.hidenavdefault ? "top:100%;bottom:auto;left:0;right:0;" : "top:auto;bottom:0;left:0;right:0;",
						n = v.options.positionFixed ? "fixed" : "absolute";
					w("body").append("<div class='html5-nav' style='display:block;position:" + n + ";" + o + "width:100%;height:" + v.options.navheight + "px;z-index:9999999;" + (v.options.navbgcolor ? "background-color:" + v.options.navbgcolor + ";" : "") + "'><div class='html5-nav-container' style='position:relative;margin:" + v.options.thumbtopmargin + "px auto " + v.options.thumbbottommargin + "px;'><div class='html5-nav-prev' style='display:block;position:absolute;cursor:pointer;width:" + v.options.navbuttonwidth + 'px;height:100%;left:0;top:0;background:url("' + v.options.navarrowsprevimage + "\") no-repeat left center;'></div><div class='html5-nav-mask' style='display:block;position:relative;margin:0 auto;overflow:hidden;'><div class='html5-nav-list'></div></div><div class='html5-nav-next' style='display:block;position:absolute;cursor:pointer;width:" + v.options.navbuttonwidth + 'px;height:100%;right:0;top:0;background:url("' + v.options.navarrowsnextimage + "\") no-repeat right center;'></div></div></div>"), v.navvisible = !v.options.hidenavdefault, v.options.shownavcontrol && (w(".html5-nav").append('<div class="html5-nav-showcontrol" style="position:absolute;display:block;cursor:pointer;bottom:100%;right:12px;margin:0;padding:0;"><img alt="" src="' + v.options.navcontrolimage + '"></div>'), w(".html5-nav-showcontrol").click(function () {
						var t = w(window).height(),
							e = w(".html5-nav").height();
						v.navvisible ? (v.navvisible = !1, w(".html5-nav").css({
							top: t - e + "px",
							bottom: "auto"
						}).animate({
							top: t + "px"
						}, function () {
							w(this).css({
								top: "100%",
								bottom: "auto"
							})
						})) : (v.navvisible = !0, e = w(".html5-nav").height(), w(".html5-nav").css({
							top: t + "px",
							bottom: "auto"
						}).animate({
							top: t - e + "px"
						}, function () {
							w(this).css({
								top: "auto",
								bottom: 0
							})
						})), v.resizeWindow()
					}));
					var s = 0;
					for (t = 0; t < v.elemArray.length; t++) v.matchGroup(i, v.elemArray[t][3]) && v.elemArray[t][8] && 0 < v.elemArray[t][8].length && (w(".html5-nav-list").append("<div class='html5-nav-thumb' data-arrayindex='" + t + "' style='float:left;overflow:hidden;cursor:pointer;opacity:" + v.options.thumbopacity + ";margin: 0 " + v.options.thumbgap / 2 + "px;width:" + v.options.thumbwidth + "px;height:" + v.options.thumbheight + "px;border:" + v.options.thumbborder + "px solid " + v.options.thumbbordercolor + ";'></div>"), this.loadThumbnail(v.elemArray[t][8], s, v.elemArray[t][2]), s++);
					w(".html5-nav-thumb").hover(function () {
						w(this).css({
							opacity: 1
						}), w(this).css({
							border: v.options.thumbborder + "px solid " + v.options.thumbhighlightbordercolor
						})
					}, function () {
						w(this).css({
							opacity: v.options.thumbopacity
						}), w(this).css({
							border: v.options.thumbborder + "px solid " + v.options.thumbbordercolor
						})
					}), w(".html5-nav-thumb").click(function () {
						var t = w(this).data("arrayindex");
						0 <= t && v.gotoSlide(t)
					}), v.options.totalwidth = s * (v.options.thumbgap + v.options.thumbwidth + 2 * v.options.thumbborder), w(".html5-nav-list").css({
						display: "block",
						position: "relative",
						"margin-left": 0,
						width: v.options.totalwidth + "px"
					}).append("<div style='clear:both;'></div>");
					var r = w(".html5-nav-mask"),
						a = w(".html5-nav-prev"),
						l = w(".html5-nav-next");
					a.click(function () {
						var t = w(".html5-nav-list"),
							e = w(".html5-nav-next"),
							i = w(window).width() - 2 * v.options.navbuttonwidth,
							o = parseInt(t.css("margin-left")) + i;
						0 <= o ? (o = 0, w(this).css({
							"background-position": "center left"
						})) : w(this).css({
							"background-position": "center right"
						}), o <= i - v.options.totalwidth ? e.css({
							"background-position": "center left"
						}) : e.css({
							"background-position": "center right"
						}), t.animate({
							"margin-left": o
						})
					}), l.click(function () {
						var t = w(".html5-nav-list"),
							e = w(".html5-nav-prev"),
							i = w(window).width() - 2 * v.options.navbuttonwidth,
							o = parseInt(t.css("margin-left")) - i;
						o <= i - v.options.totalwidth ? (o = i - v.options.totalwidth, w(this).css({
							"background-position": "center left"
						})) : w(this).css({
							"background-position": "center right"
						}), 0 <= o ? e.css({
							"background-position": "center left"
						}) : e.css({
							"background-position": "center right"
						}), t.animate({
							"margin-left": o
						})
					});
					var c = w(window).width();
					v.options.totalwidth <= c ? (r.css({
						width: v.options.totalwidth + "px"
					}), a.hide(), l.hide()) : (r.css({
						width: c - 2 * v.options.navbuttonwidth + "px"
					}), a.show(), l.show())
				}
			}
		}, v.loadElem = function (t) {
			if (v.currentElem = t, v.showing = !0, v.options.bodynoscroll && w("html,body").addClass("bodynoscroll"), v.options.showtitle && v.currentElem[2] && 0 < v.currentElem[2].length || v.options.showdescription && v.currentElem[9] && 0 < v.currentElem[9].length || v.options.inGroup && (v.options.showplaybutton || v.options.showtitleprefix) || (v.options.barheight = 0), v.showNavigation(), v.$elem.off("mouseenter").off("mouseleave").off("mousemove"), v.$loading.show(), v.options.onshowitem && window[v.options.onshowitem] && "function" == typeof window[v.options.onshowitem] && window[v.options.onshowitem](t), "none" != v.options.transition && 0 <= v.existingElem) {
				w(".html5-elem-box-previous").remove();
				var e = v.$elem.clone();
				e.insertAfter(v.$elem), v.$prevelem = v.$elem, v.$elem = e, v.$prevelem.addClass("html5-elem-box-previous"), v.$elem.addClass("html5-elem-box-current"), v.$elemWrap = w(".html5-elem-wrap", v.$elem), v.$loading = w(".html5-loading", v.$elem), v.$error = w(".html5-error-box", v.$elem), v.$image = w(".html5-image", v.$elem), v.$elemData = w(".html5-elem-data-box", v.$elem), v.$text = w(".html5-text", v.$elem), "slide" == v.options.transition && (v.$elem.css({
					position: "absolute",
					top: 0,
					left: -1 == v.direction ? "100%" : "-100%",
					opacity: 0,
					height: "auto"
				}), v.$prevelem.css({
					width: v.$prevelem.width() + "px",
					height: v.$prevelem.height() + "px"
				}))
			}
			switch (t[0]) {
				case 0:
					var i = new Image;
					w(i).on("load", function () {
						t[11] = i.width, t[12] = i.height, v.showImage(t, i.width, i.height)
					}), w(i).on("error", function () {
						v.showError()
					}), i.src = t[1];
					break;
				case 1:
					v.showSWF(t);
					break;
				case 2:
				case 8:
					v.showVideo(t);
					break;
				case 3:
				case 4:
				case 9:
				case 11:
				case 12:
					v.showYoutubeVimeo(t);
					break;
				case 5:
					v.showPDF(t);
					break;
				case 6:
					v.showMP3(t);
					break;
				case 7:
					v.showWeb(t);
					break;
				case 10:
					v.showDiv(t)
			}
			v.options.googleanalyticsaccount && window._gaq && window._gaq.push(["_trackEvent", "Lightbox", "Open", t[1]])
		}, v.loadCurElem = function () {
			v.loadElem(v.elemArray[v.options.curElem])
		}, v.showError = function () {
			v.$loading.hide(), v.resizeLightbox(v.options.errorwidth, v.options.errorheight, !0, function () {
				v.$loading.hide(), v.$error.show(), v.$elem.fadeIn(v.options.fadespeed, function () {
					v.showData()
				})
			})
		}, v.calcTextWidth = function (t) {
			return t - 36
		}, v.showTitle = function (t, e, i) {
			"inside" == v.options.titlestyle && v.$elemData.css({
				width: t + "px"
			});
			var o = "";
			v.options.showtitle && e && 0 < e.length && (o += e), v.options.inGroup && (v.options.showtitleprefix && (o = "<span class='html5-title-prefix'>" + v.options.titleprefix.replace("%NUM", v.options.groupIndex + 1).replace("%TOTAL", v.options.groupCount) + "</span> <span class='html5-title-caption'>" + o + "</span>"), v.options.showplaybutton && (o = "<div class='html5-playpause' style='display:inline-block;cursor:pointer;vertical-align:middle;'><div class='html5-play' style='display:block;'><img alt='' src='" + v.options.playimage + "'></div><div class='html5-pause' style='display:none;'><img alt='' src='" + v.options.pauseimage + "'></div></div> " + o)), 0 < o.length && (o = '<div class="html5-title">' + o + "</div>"), v.options.showdescription && i && 0 < i.length && (o += '<div class="html5-description">' + i + "</div>"), v.$text.html(o), v.options.inGroup && v.options.showplaybutton && (v.autosliding ? (w(".html5-play", v.$lightbox).hide(), w(".html5-pause", v.$lightbox).show()) : (w(".html5-play", v.$lightbox).show(), w(".html5-pause", v.$lightbox).hide()), w(".html5-play", v.$lightbox).click(function () {
				w(".html5-play", v.$lightbox).hide(), w(".html5-pause", v.$lightbox).show(), v.slideTimeout && (v.slideTimeout.stop(), v.slideTimeout.start(), v.autosliding = !0)
			}), w(".html5-pause", v.$lightbox).click(function () {
				w(".html5-play", v.$lightbox).show(), w(".html5-pause", v.$lightbox).hide(), v.slideTimeout && (v.slideTimeout.stop(), v.autosliding = !1)
			})), w("#html5-social", v.$lightbox).show(), v.options.showsocialmedia && (v.currentElem[13] ? 0 < w("#html5-socialmedia", v.$lightboxBox).length ? w("#html5-socialmedia", v.$lightboxBox).html(v.currentElem[13]) : v.$lightboxBox.append('<div id="html5-socialmedia" style="' + v.options.socialmediaposition + '">' + v.currentElem[13] + "</div>") : 0 < w("#html5-socialmedia", v.$lightboxBox).length && w("#html5-socialmedia", v.$lightboxBox).remove())
		}, v.showImage = function (e, t, i) {
			var o, n;
			e[4] ? o = e[4] : (o = t, e[4] = t), e[5] ? n = e[5] : (n = i, e[5] = i);
			var s = v.calcElemSize({
				w: o,
				h: n
			}, v.options.imagekeepratio);
			v.resizeLightbox(s.w, s.h, !0, function () {
				v.$loading.hide(), v.showTitle(s.w, e[2], e[9]);
				var t = v.options.showtimer && v.options.inGroup ? "<div class='html5-timer' style='display:none;position:absolute;" + v.options.timerposition + ":0;left:0;width:0;height:" + v.options.timerheight + "px;background-color:" + v.options.timercolor + ";opacity:" + v.options.timeropacity + ";'></div>" : "";
				v.$image.show(), v.$image.html("<div class='html5-image-container' style='display:block;position:relative;width:100%;height:100%;" + (v.options.imagekeepratio ? "overflow:hidden;" : "overflow:auto;") + "'><img alt='" + v.html2Text(e[2]) + "' src='" + e[1] + "' width='100%' height='" + (v.options.imagekeepratio ? "100%" : "auto") + "' />" + t + "</div>"), v.$elem.fadeIn(v.options.fadespeed, function () {
					v.showData()
				}), v.autosliding && (v.slideTimeout.stop(), v.slideTimeout.start())
			})
		}, v.showSWF = function (t) {
			var e = t[4] ? t[4] : b,
				i = t[5] ? t[5] : y,
				o = v.calcElemSize({
					w: e,
					h: i
				}, !0);
			e = o.w, i = o.h, v.resizeLightbox(e, i, !0, function () {
				v.$loading.hide(), v.showTitle(o.w, t[2], t[9]), v.$image.html("<div class='html5lightbox-swf' style='display:block;width:100%;height:100%;'></div>").show(), v.embedFlash(w(".html5lightbox-swf", v.$image), t[1], "window", {
					width: e,
					height: i
				}), v.$elem.show(), v.showData(), v.autosliding && (v.slideTimeout.stop(), v.slideTimeout.start())
			})
		}, v.showVideo = function (o) {
			v.slideTimeout.stop();
			var n = o[4] ? o[4] : b,
				s = o[5] ? o[5] : y,
				r = v.calcElemSize({
					w: n,
					h: s
				}, !0);
			n = r.w, s = r.h, v.resizeLightbox(n, s, !0, function () {
				v.$loading.hide(), v.showTitle(r.w, o[2], o[9]), v.$image.html("<div class='html5lightbox-video' style='display:block;width:100%;height:100%;overflow:hidden;background-color:" + v.options.videobgcolor + ";'></div>").show();
				var t = !1;
				if (v.options.isIE678 || 8 == o[0] || v.options.isIE9 && v.options.useflashonie9 || v.options.isIE10 && v.options.useflashonie10 || v.options.isIE11 && v.options.useflashonie11 ? t = !1 : v.options.isMobile ? t = !0 : !v.options.html5player && v.options.flashInstalled || !v.options.html5VideoSupported || (t = !0, (v.options.isFirefox || v.options.isOpera) && (o[6] || o[7] || v.options.canplaymp4 && !v.options.useflashformp4onfirefox || (t = !1))), t) {
					var e = o[1];
					(v.options.isFirefox || v.options.isOpera) && (o[6] ? e = o[6] : o[7] && (e = o[7])), v.embedHTML5Video(w(".html5lightbox-video", v.$image), e, v.options.autoplay, v.options.loopvideo)
				} else {
					var i = o[1];
					"/" != i.charAt(0) && "http:" != i.substring(0, 5) && "https:" != i.substring(0, 6) && (i = v.options.htmlfolder + i), v.embedFlash(w(".html5lightbox-video", v.$image), v.options.jsfolder + "html5boxplayer.swf", "transparent", {
						width: n,
						height: s,
						jsobjectname: "html5Lightbox",
						hidecontrols: v.options.videohidecontrols ? "1" : "0",
						hideplaybutton: "0",
						videofile: i,
						hdfile: "",
						ishd: "0",
						defaultvolume: v.options.defaultvideovolume,
						autoplay: v.options.autoplay ? "1" : "0",
						loop: v.options.loopvideo ? "1" : "0",
						errorcss: ".html5box-error" + v.options.errorcss,
						id: 0
					})
				}
				v.$elem.show(), v.showData()
			})
		}, v.loadNext = function () {
			w(window).trigger("html5lightbox.videofinished"), v.autosliding ? v.gotoSlide(-1) : v.options.autoclose && setTimeout(function () {
				v.finish()
			}, v.options.autoclosedelay)
		}, v.getYoutubeParams = function (t) {
			var e = {};
			if (t.indexOf("?") < 0) return e;
			for (var i = t.substring(t.indexOf("?") + 1).split("&"), o = 0; o < i.length; o++) {
				var n = i[o].split("=");
				n && 2 == n.length && "v" != n[0].toLowerCase() && (e[n[0].toLowerCase()] = n[1])
			}
			return e
		}, v.getYoutubeId = function (t) {
			var e = "",
				i = t.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\??v?=?))([^#\&\?]*).*/);
			return i && i[7] && 11 == i[7].length && (e = i[7]), e
		}, v.prepareYoutubeHref = function (t) {
			var e = "https://www.youtube.com/embed/" + v.getYoutubeId(t),
				i = this.getYoutubeParams(t),
				o = !0;
			for (var n in i) o ? (e += "?", o = !1) : e += "&", e += n + "=" + i[n];
			return e
		}, v.prepareDailymotionHref = function (t) {
			return t.match(/\:\/\/.*(dai\.ly)/i) && (t = "https://www.dailymotion.com/embed/video/" + t.match(/(dai\.ly\/)([a-zA-Z0-9\-\_]+)/)[2]), t
		}, v.showYoutubeVimeo = function (o) {
			v.slideTimeout.stop();
			var t = o[4] ? o[4] : b,
				e = o[5] ? o[5] : y,
				n = v.calcElemSize({
					w: t,
					h: e
				}, !0);
			t = n.w, e = n.h, v.resizeLightbox(t, e, !0, function () {
				v.$loading.hide(), v.showTitle(n.w, o[2], o[9]), v.$image.html("<div class='html5lightbox-video' style='display:block;width:100%;height:100%;overflow:hidden;'></div>").show();
				var t = o[1],
					e = "";
				if (3 == o[0] && (e = v.getYoutubeId(t), t = v.prepareYoutubeHref(t)), 9 == o[0] && (t = v.prepareDailymotionHref(t)), v.options.autoplay && (t += t.indexOf("?") < 0 ? "?" : "&", 11 == o[0] ? t += "autoPlay=true" : t += "autoplay=1"), v.options.loopvideo) switch (t += t.indexOf("?") < 0 ? "?" : "&", o[0]) {
					case 3:
						t += "loop=1&playlist=" + e;
						break;
					case 4:
					case 9:
						t += "loop=1";
						break;
					case 11:
						t += "endVideoBehavior=loop"
				}
				if (3 == o[0] ? (t.indexOf("?") < 0 ? t += "?wmode=transparent&rel=0" : t += "&wmode=transparent&rel=0", v.options.videohidecontrols && (t += "&controls=0&showinfo=0"), t += "&enablejsapi=1&origin=" + document.location.protocol + "//" + document.location.hostname) : 4 == o[0] && (t += t.indexOf("?") < 0 ? "?" : "&", t += "api=1&player_id=html5boxiframevideo" + v.options.curElem), w(".html5lightbox-video", v.$image).html("<iframe style='margin:0;padding:0;border:0;' class='html5boxiframevideo' id='html5boxiframevideo" + v.options.curElem + "' width='100%' height='100%' src='" + t + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>"), v.$elem.show(), v.showData(), 3 == o[0] && "object" == typeof YT && "function" == typeof YT.Player) v.ytplayer = new YT.Player("html5boxiframevideo" + v.options.curElem, {
					events: {
						onStateChange: function (t) {
							t.data == YT.PlayerState.ENDED && (w(window).trigger("html5lightbox.videofinished"), v.autosliding ? v.gotoSlide(-1) : v.options.autoclose && setTimeout(function () {
								v.finish()
							}, v.options.autoclosedelay))
						}
					}
				});
				else if (4 == o[0] && "function" == typeof $f) {
					var i = w("#html5boxiframevideo" + v.options.curElem)[0];
					v.vimeoPlayer = $f(i), v.vimeoPlayer.addEvent("ready", function () {
						v.vimeoPlayer.addEvent("finish", function (t) {
							w(window).trigger("html5lightbox.videofinished"), v.autosliding ? v.gotoSlide(-1) : v.options.autoclose && setTimeout(function () {
								v.finish()
							}, v.options.autoclosedelay)
						})
					})
				}
			})
		}, v.showPDF = function (t) {
			if (v.options.enablepdfjs) {
				if (v.options.isIPhone && v.options.openpdfinnewtaboniphone || v.options.isIPad && v.options.openpdfinnewtabonipad) return window.open(t[1], "_blank").focus(), void v.finish();
				v.options.pdfjsengine || (v.options.pdfjsengine = v.options.jsfolder + "pdfjs/web/viewer.html");
				var e = t[1];
				"http:" != e.substring(0, 5) && "https:" != e.substring(0, 6) && (e = v.absoluteUrl(e));
				var i = jQuery.extend(!0, {}, t);
				i[1] = v.options.pdfjsengine + "?file=" + encodeURIComponent(e), v.showWeb(i)
			} else {
				if (v.options.isIPhone || v.options.isIPad || v.options.isAndroid || v.options.isIE || v.options.isIE11) return window.open(t[1], "_blank").focus(), void v.finish();
				v.showWeb(t)
			}
		}, v.showMP3 = function (t) {}, v.showDiv = function (e) {
			var t = w(window).width(),
				i = w(window).height(),
				o = v.options.shownavigation && v.navvisible ? v.options.navheight : 0,
				n = e[4] ? e[4] : t,
				s = e[5] ? e[5] : i - o,
				r = v.calcElemSize({
					w: n,
					h: s
				}, !1);
			n = r.w, s = r.h, v.resizeLightbox(n, s, !0, function () {
				v.$loading.hide(), v.showTitle(r.w, e[2], e[9]), v.$image.html("<div class='html5lightbox-div' id='html5lightbox-div" + v.options.curElem + "' style='display:block;width:100%;height:" + (v.options.autoresizecontent ? "auto" : "100%") + ";" + (v.options.isIOS ? "-webkit-overflow-scrolling:touch;overflow-y:scroll;" : "overflow:auto;") + "'></div>").show();
				var t = e[1];
				0 < w(t).length ? w(t).children().appendTo(w("#html5lightbox-div" + v.options.curElem, v.$image)) : w("#html5lightbox-div" + v.options.curElem, v.$image).html("<div class='html5-error'>The specified div ID does not exist.</div>"), v.$elem.show(), v.showData(), v.options.autoresizecontent && v.resizeWindow(), v.autosliding && (v.slideTimeout.stop(), v.slideTimeout.start())
			})
		}, v.isSameDomain = function (t) {
			if ("http:" != t.substring(0, 5) && "https:" != t.substring(0, 6)) return !0;
			var e = document.createElement("a");
			e.setAttribute("href", t);
			var i = e.protocol == document.location.protocol && e.host == document.location.host && e.port == document.location.port;
			return e = null, i
		}, v.showWeb = function (t) {
			var e = w(window).width(),
				i = w(window).height(),
				o = v.options.shownavigation && v.navvisible ? v.options.navheight : 0,
				n = t[4] ? t[4] : e,
				s = t[5] ? t[5] : i - o,
				r = v.calcElemSize({
					w: n,
					h: s
				}, !1);
			n = r.w, s = r.h, v.resizeLightbox(n, s, !0, function () {
				v.$loading.hide(), v.showTitle(r.w, t[2], t[9]), v.$image.html("<div class='html5lightbox-web' style='display:block;width:100%;height:100%;" + (v.options.isIOS ? "-webkit-overflow-scrolling:touch;overflow-y:scroll;" : "") + "'></div>").show(), w(".html5lightbox-web", v.$image).html("<iframe style='margin:0;padding:0;border:0;' class='html5lightbox-web-iframe' width='100%' height='100%' src='" + t[1] + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>"), v.$elem.show(), v.showData(), v.options.autoresizecontent && v.isSameDomain(t[1]) && (w(".html5lightbox-web-iframe", v.$image).data("sameorigin", !0), w(".html5lightbox-web-iframe", v.$image).on("load", function () {
					w(this).data("sameoriginloaded", !0), v.resizeWindow()
				})), v.autosliding && (v.slideTimeout.stop(), v.slideTimeout.start())
			})
		}, v.scrollBox = function () {}, v.resizeWindow = function () {
			if (v.currentElem && v.options.responsive) {
				var t, e, i, o = w(window).width(),
					n = w(window).height();
				if (v.options.responsivebarheight && (n <= v.options.smallscreenheight ? v.options.barheight = v.options.barheightonsmallheight : v.options.barheight = v.options.barheightoriginal, "bottom" == v.options.titlestyle && "auto" != v.options.barautoheight && v.$elemData.css({
						height: v.options.barheight + "px",
						"max-height": v.options.barheight + "px"
					})), v.options.showtitle && v.currentElem[2] && 0 < v.currentElem[2].length || v.options.showdescription && v.currentElem[9] && 0 < v.currentElem[9].length || v.options.inGroup && (v.options.showplaybutton || v.options.showtitleprefix) || (v.options.barheight = 0), 5 == v.currentElem[0] || 7 == v.currentElem[0] || 10 == v.currentElem[0]) {
					var s = v.options.shownavigation && v.navvisible ? v.options.navheight : 0;
					t = v.currentElem[4] ? v.currentElem[4] : o, e = v.currentElem[5] ? v.currentElem[5] : n - s, i = !1
				} else t = v.currentElem[4] ? v.currentElem[4] : b, e = v.currentElem[5] ? v.currentElem[5] : y, i = 0 != v.currentElem[0] || v.options.imagekeepratio;
				var r = v.calcElemSize({
						w: t,
						h: e
					}, i),
					a = v.calcBoxPosition(r.w, r.h),
					l = a[0],
					c = a[1],
					d = a[2];
				if (v.$lightboxBox.css({
						"margin-top": d
					}), "left" == v.options.titlestyle || "right" == v.options.titlestyle ? v.$lightboxBox.css({
						width: l,
						height: c
					}) : (v.$lightboxBox.css({
						width: l,
						height: "auto"
					}), v.$elemWrap.css({
						width: l,
						height: c
					})), "inside" == v.options.titlestyle && v.$elemData.css({
						width: r.w + "px"
					}), v.options.autoresizecontent && (5 == v.currentElem[0] || 7 == v.currentElem[0] || 10 == v.currentElem[0])) {
					var u = !1;
					if (7 == v.currentElem[0] && 0 < w(".html5lightbox-web-iframe", v.$lightbox).length && w(".html5lightbox-web-iframe", v.$lightbox).data("sameoriginloaded")) {
						var p = w(".html5lightbox-web-iframe", v.$lightbox)[0];
						p && p.contentWindow && p.contentWindow.document && p.contentWindow.document.documentElement.offsetHeight && e > p.contentWindow.document.documentElement.offsetHeight && (e = p.contentWindow.document.documentElement.offsetHeight, u = !0)
					} else if (10 == v.currentElem[0] && 0 < w(".html5lightbox-div", v.$lightbox).length) {
						var h = w(".html5lightbox-div", v.$lightbox).height();
						h < e && (e = h, u = !0)
					}
					u && (r = v.calcElemSize({
						w: t,
						h: e
					}, i), l = (a = v.calcBoxPosition(r.w, r.h))[0], c = a[1], d = a[2], v.$lightboxBox.css({
						"margin-top": d
					}), "left" == v.options.titlestyle || "right" == v.options.titlestyle ? v.$lightboxBox.css({
						height: c
					}) : (v.$lightboxBox.css({
						height: "auto"
					}), v.$elemWrap.css({
						height: c
					})))
				}
				if (!(w(".html5-nav").length <= 0)) {
					w(".html5-nav-list").css({
						"margin-left": 0
					});
					var f = w(".html5-nav-mask"),
						m = w(".html5-nav-prev"),
						g = w(".html5-nav-next");
					o = w(window).width(), v.options.totalwidth <= o ? (f.css({
						width: v.options.totalwidth + "px"
					}), m.hide(), g.hide()) : (f.css({
						width: o - 2 * v.options.navbuttonwidth + "px"
					}), m.show(), g.show())
				}
			}
		}, v.calcElemSize = function (t, e) {
			if (!v.options.responsive) return t;
			var i = w(window).width();
			i = i || w(document).width();
			var o = w(window).height();
			o = o || w(document).height(), ("left" == v.options.titlestyle || "right" == v.options.titlestyle) && i > v.options.sidetobottomscreenwidth && (t.w = 100 * t.w / v.options.imagepercentage);
			var n = v.options.shownavigation && v.navvisible ? v.options.navheight : 0,
				s = w(window).width() < v.options.navarrowsbottomscreenwidth ? v.options.bordertopmarginsmall : v.options.bordertopmargin,
				r = o - n - 2 * v.options.bordersize - 2 * s;
			"bottom" == v.options.titlestyle && (r -= v.options.barheight);
			var a = i - 2 * v.options.bordersize - 2 * v.options.bordermargin;
			if ((v.options.fullscreenmode && i > v.options.navarrowsbottomscreenwidth || (v.options.isTouch && v.options.navarrowsalwaysshowontouch || v.options.alwaysshownavarrows) && i > v.options.navarrowsbottomscreenwidth) && (a -= 64), (("left" == v.options.titlestyle || "right" == v.options.titlestyle) && i <= v.options.sidetobottomscreenwidth || v.options.notkeepratioonsmallheight && o <= v.options.smallscreenheight) && (e = !1), 0 == v.currentElem[0] && e) {
				var l = v.currentElem[11] / v.currentElem[12];
				t.h = Math.round(t.w / l), t.h > r && (t.w = Math.round(l * r), t.h = r), t.w > a && (t.h = Math.round(a / l), t.w = a)
			} else t.h > r ? (e && (t.w = Math.round(t.w * r / t.h)), t.h = r) : v.options.maxheight && (t.h = r), t.w > a && (e && (t.h = Math.round(t.h * a / t.w)), t.w = a);
			return t
		}, v.showData = function () {
			0 < v.$text.text().length && v.$elemData.show(), "bottom" != v.options.titlestyle && "inside" != v.options.titlestyle || v.$lightboxBox.css({
				height: "auto"
			}), 0 < v.$text.text().length && "bottom" == v.options.titlestyle && v.$elemData.css({
				"max-height": v.options.barheight + "px"
			}), v.options.positionFixed ? w("#html5-lightbox-overlay", v.$lightbox).css({
				height: Math.max(w(window).height(), w(document).height())
			}) : w("#html5-lightbox-overlay", v.$lightbox).css({
				height: "100%"
			}), w(window).trigger("html5lightbox.lightboxopened")
		}, v.resizeLightbox = function (t, e, i, o) {
			v.hideNavArrows();
			var n = v.calcBoxPosition(t, e),
				s = n[0],
				r = n[1],
				a = n[2];
			v.$loading.hide(), v.$watermark.hide(), v.options.nextElem <= v.options.curElem && v.options.onlastitem && window[v.options.onlastitem] && "function" == typeof window[v.options.onlastitem] && window[v.options.onlastitem](v.currentElem), v.options.prevElem >= v.options.curElem && v.options.onfirstitem && window[v.options.onfirstitem] && "function" == typeof window[v.options.onfirstitem] && window[v.options.onfirstitem](v.currentElem), v.options.fullscreenmode || v.options.isTouch && v.options.navarrowsalwaysshowontouch || v.options.alwaysshownavarrows || (v.$lightboxBox.on("mouseenter mousemove", function () {
				(v.options.arrowloop && 0 <= v.options.prevElem || !v.options.arrowloop && 0 <= v.options.prevElem && v.options.prevElem < v.options.curElem) && v.$prev.fadeIn(), (v.options.arrowloop && 0 <= v.options.nextElem || !v.options.arrowloop && 0 <= v.options.nextElem && v.options.nextElem > v.options.curElem) && v.$next.fadeIn()
			}), v.$lightboxBox.on("mouseleave", function () {
				v.$next.fadeOut(), v.$prev.fadeOut()
			}));
			var l = parseInt(v.$lightboxBox.css("margin-top"));
			v.$lightboxBox.css({
				"margin-top": a
			});
			var c = i ? v.options.resizespeed : 0;
			"slide" == v.options.transition && 0 <= v.existingElem && (c = 0), "left" == v.options.titlestyle || "right" == v.options.titlestyle ? (s == v.$lightboxBox.width() && r == v.$lightboxBox.height() && (c = 0), v.$lightboxBox.animate({
				width: s
			}, c).animate({
				height: r
			}, c, function () {
				v.onAnimateFinish(o)
			})) : (s == v.$elemWrap.width() && r == v.$elemWrap.height() && (c = 0), v.$lightboxBox.css({
				width: s,
				height: "auto"
			}), "slide" == v.options.transition && 0 <= v.existingElem && 0 < w(".html5-elem-box-previous", v.$lightbox).length && w(".html5-elem-box-previous", v.$lightbox).css({
				"margin-left": String(s / 2 - w(".html5-elem-box-previous", v.$lightbox).width() / 2) + "px",
				top: String(l - a) + "px"
			}), v.$elemWrap.animate({
				width: s
			}, c).animate({
				height: r
			}, c, function () {
				v.onAnimateFinish(o)
			}))
		}, v.onAnimateFinish = function (t) {
			v.$loading.show(), v.$watermark.show(), v.$close.show(), v.$elem.css({
				"background-color": v.options.bgcolor
			}), t(), v.finishCallback()
		}, v.finishCallback = function () {
			"slide" == v.options.transition && 0 <= v.existingElem && (v.$prevelem.animate({
				left: -1 == v.direction ? "-100%" : "100%",
				opacity: 0
			}, {
				duration: v.options.transitionduration
			}), v.$elem.animate({
				left: 0,
				opacity: 1
			}, {
				duration: v.options.transitionduration,
				always: function () {
					v.$prevelem.remove(), v.$elem.removeClass("html5-elem-box-current").css({
						position: "relative",
						height: "100%"
					})
				}
			}))
		}, v.resetDiv = function (t) {
			if (0 < v.elemArray.length && 0 <= t && 10 == v.elemArray[t][0]) {
				var e = v.elemArray[t][1];
				0 < w(e).length && w("#html5lightbox-div" + t).children().appendTo(w(e))
			}
		}, v.reset = function () {
			v.options.stamp && v.$watermark.hide(), v.showing = !1, v.$image.empty(), v.$text.empty(), v.$error.hide(), v.$loading.hide(), v.$image.hide(), "bottom" != v.options.titlestyle && "inside" != v.options.titlestyle || v.$elemData.hide(), v.options.fullscreenmode || v.$close.hide(), v.$elem.css({
				"background-color": ""
			})
		}, v.resetNavigation = function () {
			v.options.navheight = 0, w(".html5-nav").remove(), v.navvisible = !1
		}, v.finish = function () {
			v.existingElem = -1, v.resetDiv(v.options.curElem), w(".html5-lightbox-video", v.$lightbox).length && w(".html5-lightbox-video", v.$lightbox).attr("src", ""), w("head").find("style").each(function () {
				"html5box-html5-lightbox" == w(this).data("creator") && w(this).remove()
			}), v.options.bodynoscroll && w("html,body").removeClass("bodynoscroll"), v.slideTimeout.stop(), v.reset(), v.resetNavigation(), v.$lightbox.remove(), w("#html5box-html5-lightbox").remove(), v.showObjects(), v.options.oncloselightbox && window[v.options.oncloselightbox] && "function" == typeof window[v.options.oncloselightbox] && window[v.options.oncloselightbox](v.currentElem), v.onLightboxClosed && "function" == typeof v.onLightboxClosed && v.onLightboxClosed(v.currentElem), w(window).trigger("html5lightbox.lightboxclosed")
		}, v.pauseSlide = function () {}, v.playSlide = function () {}, v.gotoSlide = function (t) {
			if (v.existingElem = v.options.curElem, v.direction = t, v.resetDiv(v.options.curElem), -1 == t) {
				if (v.options.nextElem < 0) return;
				v.options.curElem = v.options.nextElem
			} else if (-2 == t) {
				if (v.options.prevElem < 0) return;
				v.options.curElem = v.options.prevElem
			} else 0 <= t && (v.direction = t > v.options.curElem ? -1 : -2, v.options.curElem = t);
			v.autosliding && v.slideTimeout.stop(), v.calcNextPrevElem(), "none" == v.options.transition && v.reset(), v.loadCurElem()
		}, v.enableSwipe = function () {
			var t = !(!v.options.isAndroid || !(v.options.swipepreventdefaultonandroid || 0 <= v.options.androidVersion && v.options.androidVersion <= 5));
			v.$lightboxBox.html5lightboxTouchSwipe({
				preventWebBrowser: t,
				swipeDistance: v.options.swipedistance,
				swipeLeft: function () {
					v.gotoSlide(-1)
				},
				swipeRight: function () {
					v.gotoSlide(-2)
				}
			})
		}, v.hideObjects = function () {
			w("select, embed, object").css({
				visibility: "hidden"
			})
		}, v.showObjects = function () {
			w("select, embed, object").css({
				visibility: "visible"
			})
		}, v.embedHTML5Video = function (t, e, i, o) {
			t.html("<div style='display:block;width:100%;height:100%;position:relative;'><video class='html5-lightbox-video' width='100%' height='100%'" + (v.options.html5videoposter && 0 < v.options.html5videoposter.length ? "poster='" + v.options.html5videoposter + "'" : "") + (i ? " autoplay" : "") + (o ? " loop" : "") + (v.options.nativehtml5controls && !v.options.videohidecontrols ? " controls='controls'" : "") + " src='" + e + "'></div>"), v.options.nativehtml5controls || (w("video", t).data("src", e), w("video", t).lightboxHTML5VideoControls(v.options.skinsfolder, v, v.options.videohidecontrols, !1, v.options.defaultvideovolume)), w("video", t).off("ended").on("ended", function () {
				w(window).trigger("html5lightbox.videofinished"), v.autosliding ? v.gotoSlide(-1) : v.options.autoclose && setTimeout(function () {
					v.finish()
				}, v.options.autoclosedelay)
			})
		}, v.embedFlash = function (t, e, i, o) {
			if (v.options.flashInstalled) {
				var n = {
					pluginspage: "http://www.adobe.com/go/getflashplayer",
					quality: "high",
					allowFullScreen: "true",
					allowScriptAccess: "always",
					type: "application/x-shockwave-flash",
					width: "100%",
					height: "100%"
				};
				n.src = e, n.flashVars = w.param(o), n.wmode = i;
				var s = "";
				for (var r in n) s += r + "=" + n[r] + " ";
				t.html("<embed " + s + "/>")
			} else t.html("<div class='html5lightbox-flash-error' style='display:block; position:relative;text-align:center; width:100%; left:0px; top:40%;'><div class='html5-error'><div>The required Adobe Flash Player plugin is not installed</div><br /><div style='display:block;position:relative;text-align:center;width:112px;height:33px;margin:0px auto;'><a href='http://www.adobe.com/go/getflashplayer'><img src='http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' width='112' height='33'></img></a></div></div>")
		}, v.checkType = function (t) {
			return t ? t.match(/\.(jpg|gif|png|bmp|jpeg)(.*)?$/i) ? 0 : t.match(/[^\.]\.(swf)\s*$/i) ? 1 : t.match(/\.(mp4|m4v|ogv|ogg|webm)(.*)?$/i) ? 2 : t.match(/\:\/\/.*(youtube\.com)/i) || t.match(/\:\/\/.*(youtu\.be)/i) ? 3 : t.match(/\:\/\/.*(vimeo\.com)/i) ? 4 : t.match(/\:\/\/.*(dailymotion\.com)/i) || t.match(/\:\/\/.*(dai\.ly)/i) ? 9 : t.match(/[^\.]\.(pdf)\s*$/i) ? 5 : t.match(/[^\.]\.(mp3)\s*$/i) ? 6 : t.match(/[^\.]\.(flv)\s*$/i) ? 8 : t.match(/\#\w+/i) ? 10 : t.match(/\:\/\/.*(wistia)/i) ? 11 : 7 : -1
		}, v.getURLParams = function () {
			for (var t = {}, e = window.location.search.substring(1).split("&"), i = 0; i < e.length; i++) {
				var o = e[i].split("=");
				o && 2 == o.length && (t[o[0].toLowerCase()] = unescape(o[1]))
			}
			return t
		}, v.absoluteUrl = function (t) {
			var e = document.createElement("a");
			return e.href = t, e.protocol + "//" + e.host + e.pathname + e.search + e.hash
		}, v.showLightbox = function (t, e, i, o, n, s, r, a, l) {
			v.options = w.extend(v.options, v.defaultoptions), w(window).trigger("html5lightbox.lightboxshow"), v.init(), v.reset(), v.$lightbox.show();
			var c = v.calcBoxPosition(v.options.loadingwidth, v.options.loadingheight),
				d = c[0],
				u = c[1],
				p = c[2];
			v.$lightboxBox.css({
				"margin-top": p
			}), "left" == v.options.titlestyle || "right" == v.options.titlestyle ? v.$lightboxBox.css({
				width: d,
				height: u
			}) : (v.$lightboxBox.css({
				width: d,
				height: "auto"
			}), v.$elemWrap.css({
				width: d,
				height: u
			})), v.loadElem(new Array(t, e, i, null, o, n, s, r, a, l))
		}, v.addItem = function (t, e, i, o, n, s, r, a, l, c) {
			type = c && 0 <= c ? c : v.checkType(t), v.elemArray.push(new Array(type, t, e, i, o, n, s, r, a, l))
		}, v.showItem = function (t) {
			if (v.options = w.extend(v.options, v.defaultoptions), w(window).trigger("html5lightbox.lightboxshow"), v.init(), v.elemArray.length <= 0) return !0;
			v.hideObjects();
			for (var e = 0; e < v.elemArray.length && v.elemArray[e][1] != t; e++);
			if (e == v.elemArray.length) return !0;
			v.options.curElem = e, v.calcNextPrevElem(), v.reset(), v.$lightbox.show();
			var i = v.calcBoxPosition(v.options.loadingwidth, v.options.loadingheight),
				o = i[0],
				n = i[1],
				s = i[2];
			return v.$lightboxBox.css({
				"margin-top": s
			}), "left" == v.options.titlestyle || "right" == v.options.titlestyle ? v.$lightboxBox.css({
				width: o,
				height: n
			}) : (v.$lightboxBox.css({
				width: o,
				height: "auto"
			}), v.$elemWrap.css({
				width: o,
				height: n
			})), v.loadCurElem(), !1
		}, v.off("click").click(v.clickHandler), v.each(function () {
			var t = w(this),
				e = !1,
				i = 0;
			if ("undefined" != typeof html5lightbox_options && html5lightbox_options && ("autoopen" in html5lightbox_options && (e = html5lightbox_options.autoopen), "autoopendelay" in html5lightbox_options && (i = html5lightbox_options.autoopendelay)), e = t.data("autoopen") ? t.data("autoopen") : e, i = t.data("autoopendelay") ? t.data("autoopendelay") : i, e) return setTimeout(function () {
				t.click()
			}, i), !1
		});
		var u = v.getURLParams();
		if ("html5lightboxshare" in u) {
			var p = decodeURIComponent(u.html5lightboxshare),
				h = w('.html5lightbox[href="' + p + '"]');
			0 < h.length && h.click()
		}
		return v
	}, (e = jQuery).fn.html5lightboxTouchSwipe = function (t) {
		var a = {
			preventWebBrowser: !1,
			swipeDistance: 0,
			swipeLeft: null,
			swipeRight: null,
			swipeTop: null,
			swipeBottom: null
		};
		return t && e.extend(a, t), this.each(function () {
			var i = -1,
				o = -1,
				n = -1,
				s = -1;

			function r(t) {
				s = n = o = i = -1
			}
			try {
				e(this).on("touchstart", function (t) {
					var e = t.originalEvent;
					1 <= e.targetTouches.length ? (i = e.targetTouches[0].pageX, o = e.targetTouches[0].pageY) : r()
				}), e(this).on("touchmove", function (t) {
					a.preventWebBrowser && t.preventDefault();
					var e = t.originalEvent;
					1 <= e.targetTouches.length ? (n = e.targetTouches[0].pageX, s = e.targetTouches[0].pageY) : r()
				}), e(this).on("touchend", function (t) {
					(0 < n || 0 < s) && (Math.abs(n - i) > Math.abs(s - o) ? Math.abs(n - i) > a.swipeDistance && (i < n ? a.swipeRight && a.swipeRight.call() : a.swipeLeft && a.swipeLeft.call()) : Math.abs(s - o) > a.swipeDistance && (o < s ? a.swipeBottom && a.swipeBottom.call() : a.swipeTop && a.swipeTop.call())), r()
				}), e(this).on("touchcancel", r)
			} catch (t) {}
		})
	}, (W = jQuery).fn.lightboxHTML5VideoControls = function (t, e, i, o, n) {
		var s = "ontouchstart" in window,
			r = s ? "touchstart" : "mousedown",
			a = s ? "touchmove" : "mousemove",
			l = s ? "touchcancel" : "mouseup",
			c = "click",
			d = s ? 48 : 36,
			u = null,
			p = null,
			h = !1,
			f = !0,
			m = null != navigator.userAgent.match(/iPod/i) || null != navigator.userAgent.match(/iPhone/i),
			g = W(this).data("ishd"),
			v = W(this).data("hd"),
			b = W(this).data("src"),
			y = W(this);
		if (y.get(0).removeAttribute("controls"), m) {
			var w = y.height() - d;
			y.css({
				height: w
			})
		}
		var x = W("<div class='html5boxVideoPlay'></div>");
		m || (y.after(x), x.css({
			position: "absolute",
			top: "50%",
			left: "50%",
			display: "block",
			cursor: "pointer",
			width: 64,
			height: 64,
			"margin-left": -32,
			"margin-top": -32,
			"background-image": "url('" + t + "html5boxplayer_playvideo.png')",
			"background-position": "center center",
			"background-repeat": "no-repeat"
		}).on(c, function () {
			y.get(0).play()
		}));
		var T = W("<div class='html5boxVideoFullscreenBg'></div>"),
			k = W("<div class='html5boxVideoControls'><div class='html5boxVideoControlsBg'></div><div class='html5boxPlayPause'><div class='html5boxPlay'></div><div class='html5boxPause'></div></div><div class='html5boxTimeCurrent'>--:--</div><div class='html5boxFullscreen'></div><div class='html5boxHD'></div><div class='html5boxVolume'><div class='html5boxVolumeButton'></div><div class='html5boxVolumeBar'><div class='html5boxVolumeBarBg'><div class='html5boxVolumeBarActive'></div></div></div></div><div class='html5boxTimeTotal'>--:--</div><div class='html5boxSeeker'><div class='html5boxSeekerBuffer'></div><div class='html5boxSeekerPlay'></div><div class='html5boxSeekerHandler'></div></div><div style='clear:both;'></div></div>");
		y.after(k), y.after(T), T.css({
			display: "none",
			position: "fixed",
			left: 0,
			top: 0,
			bottom: 0,
			right: 0,
			"z-index": 2147483647
		}), k.css({
			display: "block",
			position: "absolute",
			width: "100%",
			height: d,
			left: 0,
			bottom: 0,
			right: 0,
			"max-width": "640px",
			margin: "0 auto"
		});
		var E = function () {
			f = !0
		};
		y.on(c, function () {
			f = !0
		}).hover(function () {
			f = !0
		}, function () {
			f = !1
		}), i || setInterval(function () {
			f && (k.show(), f = !1, clearTimeout(u), u = setTimeout(function () {
				y.get(0).paused || k.fadeOut()
			}, 5e3))
		}, 250), W(".html5boxVideoControlsBg", k).css({
			display: "block",
			position: "absolute",
			width: "100%",
			height: "100%",
			left: 0,
			top: 0,
			"background-color": "#000000",
			opacity: .7,
			filter: "alpha(opacity=70)"
		}), W(".html5boxPlayPause", k).css({
			display: "block",
			position: "relative",
			width: "32px",
			height: "32px",
			margin: Math.floor((d - 32) / 2),
			float: "left"
		});
		var _ = W(".html5boxPlay", k),
			C = W(".html5boxPause", k);
		_.css({
			display: "block",
			position: "absolute",
			top: 0,
			left: 0,
			width: "32px",
			height: "32px",
			cursor: "pointer",
			"background-image": "url('" + t + "html5boxplayer_playpause.png')",
			"background-position": "top left"
		}).hover(function () {
			W(this).css({
				"background-position": "bottom left"
			})
		}, function () {
			W(this).css({
				"background-position": "top left"
			})
		}).on(c, function () {
			y.get(0).play()
		}), C.css({
			display: "none",
			position: "absolute",
			top: 0,
			left: 0,
			width: "32px",
			height: "32px",
			cursor: "pointer",
			"background-image": "url('" + t + "html5boxplayer_playpause.png')",
			"background-position": "top right"
		}).hover(function () {
			W(this).css({
				"background-position": "bottom right"
			})
		}, function () {
			W(this).css({
				"background-position": "top right"
			})
		}).on(c, function () {
			y.get(0).pause()
		});
		var S = W(".html5boxTimeCurrent", k),
			A = W(".html5boxTimeTotal", k),
			$ = W(".html5boxSeeker", k),
			D = W(".html5boxSeekerPlay", k),
			I = W(".html5boxSeekerBuffer", k),
			O = W(".html5boxSeekerHandler", k);
		if (S.css({
				display: "block",
				position: "relative",
				float: "left",
				"line-height": d + "px",
				"font-weight": "normal",
				"font-size": "12px",
				margin: "0 8px",
				"font-family": "Arial, Helvetica, sans-serif",
				color: "#fff"
			}), A.css({
				display: "block",
				position: "relative",
				float: "right",
				"line-height": d + "px",
				"font-weight": "normal",
				"font-size": "12px",
				margin: "0 8px",
				"font-family": "Arial, Helvetica, sans-serif",
				color: "#fff"
			}), $.css({
				display: "block",
				cursor: "pointer",
				overflow: "hidden",
				position: "relative",
				height: "10px",
				"background-color": "#222",
				margin: Math.floor((d - 10) / 2) + "px 4px"
			}).on(r, function (t) {
				var e = (s ? t.originalEvent.touches[0] : t).pageX - $.offset().left;
				D.css({
					width: e
				}), y.get(0).currentTime = e * y.get(0).duration / $.width(), $.on(a, function (t) {
					var e = (s ? t.originalEvent.touches[0] : t).pageX - $.offset().left;
					D.css({
						width: e
					}), y.get(0).currentTime = e * y.get(0).duration / $.width()
				})
			}).on(l, function () {
				$.off(a)
			}), I.css({
				display: "block",
				position: "absolute",
				left: 0,
				top: 0,
				height: "100%",
				"background-color": "#444"
			}), D.css({
				display: "block",
				position: "absolute",
				left: 0,
				top: 0,
				height: "100%",
				"background-color": "#fcc500"
			}), !m && (y.get(0).requestFullscreen || y.get(0).webkitRequestFullScreen || y.get(0).mozRequestFullScreen || y.get(0).webkitEnterFullScreen || y.get(0).msRequestFullscreen)) {
			var N = function (t) {
				k.css({
					position: t ? "fixed" : "absolute"
				});
				var e = L.css("background-position") ? L.css("background-position").split(" ")[1] : L.css("background-position-y");
				L.css({
					"background-position": (t ? "right" : "left") + " " + e
				}), T.css({
					display: t ? "block" : "none"
				}), t ? (W(document).on("mousemove", E), k.css({
					"z-index": 2147483647
				})) : (W(document).off("mousemove", E), k.css({
					"z-index": ""
				}))
			};
			document.addEventListener("MSFullscreenChange", function () {
				h = null != document.msFullscreenElement;
				var t = null != navigator.userAgent.match(/Trident\/7/) && null != navigator.userAgent.match(/rv:11/),
					e = null != navigator.userAgent.match(/MSIE/i) && !this.options.isOpera;
				(t || e) && (h || y.get(0).removeAttribute("controls")), N(h)
			}, !1), document.addEventListener("fullscreenchange", function () {
				h = document.fullscreen, N(document.fullscreen)
			}, !1), document.addEventListener("mozfullscreenchange", function () {
				h = document.mozFullScreen, null != navigator.userAgent.match(/Firefox/i) && (h ? y.get(0).setAttribute("controls", "controls") : y.get(0).removeAttribute("controls")), N(document.mozFullScreen)
			}, !1), document.addEventListener("webkitfullscreenchange", function () {
				h = document.webkitIsFullScreen, null != navigator.userAgent.match(/Android/i) ? h ? y.get(0).setAttribute("controls", "controls") : (y.get(0).removeAttribute("controls"), N(document.webkitIsFullScreen)) : N(document.webkitIsFullScreen)
			}, !1), y.get(0).addEventListener("webkitbeginfullscreen", function () {
				h = !0
			}, !1), y.get(0).addEventListener("webkitendfullscreen", function () {
				h = !1
			}, !1), null == navigator.userAgent.match(/Android/i) && W("head").append("<style type='text/css'>video::-webkit-media-controls { display:none !important; }</style>");
			var L = W(".html5boxFullscreen", k);
			L.css({
				display: "block",
				position: "relative",
				float: "right",
				width: "32px",
				height: "32px",
				margin: Math.floor((d - 32) / 2),
				cursor: "pointer",
				"background-image": "url('" + t + "html5boxplayer_fullscreen.png')",
				"background-position": "left top"
			}).hover(function () {
				var t = W(this).css("background-position") ? W(this).css("background-position").split(" ")[0] : W(this).css("background-position-x");
				W(this).css({
					"background-position": t + " bottom"
				})
			}, function () {
				var t = W(this).css("background-position") ? W(this).css("background-position").split(" ")[0] : W(this).css("background-position-x");
				W(this).css({
					"background-position": t + " top"
				})
			}).on(c, function () {
				! function (t) {
					if (t) {
						var e = null != navigator.userAgent.match(/Trident\/7/) && null != navigator.userAgent.match(/rv:11/),
							i = null != navigator.userAgent.match(/MSIE/i) && !this.options.isOpera;
						(e || i) && y.get(0).setAttribute("controls", "controls"), y.get(0).requestFullscreen ? y.get(0).requestFullscreen() : y.get(0).webkitRequestFullScreen ? y.get(0).webkitRequestFullScreen() : y.get(0).mozRequestFullScreen ? y.get(0).mozRequestFullScreen() : y.get(0).webkitEnterFullScreen && y.get(0).webkitEnterFullScreen(), y.get(0).msRequestFullscreen && y.get(0).msRequestFullscreen()
					} else document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen ? document.webkitCancelFullScreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.msExitFullscreen && document.msExitFullscreen()
				}(h = !h)
			})
		}
		v && W(".html5boxHD", k).css({
			display: "block",
			position: "relative",
			float: "right",
			width: "32px",
			height: "32px",
			margin: Math.floor((d - 32) / 2),
			cursor: "pointer",
			"background-image": "url('" + t + "html5boxplayer_hd.png')",
			"background-position": (g ? "right" : "left") + " center"
		}).on(c, function () {
			g = !g, W(this).css({
				"background-position": (g ? "right" : "left") + " center"
			}), e.isHd = g;
			var t = y.get(0).isPaused;
			y.get(0).setAttribute("src", (g ? v : b) + "#t=" + y.get(0).currentTime), t ? m || y.get(0).pause() : y.get(0).play()
		});
		var j = 0 == (y.get(0).volume = n) ? 1 : n,
			P = y.get(0).volume;
		if (y.get(0).volume = P / 2 + .1, y.get(0).volume === P / 2 + .1) {
			y.get(0).volume = P;
			var H = W(".html5boxVolume", k),
				M = W(".html5boxVolumeButton", k),
				z = W(".html5boxVolumeBar", k),
				R = W(".html5boxVolumeBarBg", k),
				q = W(".html5boxVolumeBarActive", k);
			H.css({
				display: "block",
				position: "relative",
				float: "right",
				width: "32px",
				height: "32px",
				margin: Math.floor((d - 32) / 2)
			}).hover(function () {
				clearTimeout(p);
				var t = y.get(0).volume;
				q.css({
					height: Math.round(100 * t) + "%"
				}), z.show()
			}, function () {
				clearTimeout(p), p = setTimeout(function () {
					z.hide()
				}, 1e3)
			}), M.css({
				display: "block",
				position: "absolute",
				top: 0,
				left: 0,
				width: "32px",
				height: "32px",
				cursor: "pointer",
				"background-image": "url('" + t + "html5boxplayer_volume.png')",
				"background-position": "top " + (0 < P ? "left" : "right")
			}).hover(function () {
				var t = W(this).css("background-position") ? W(this).css("background-position").split(" ")[0] : W(this).css("background-position-x");
				W(this).css({
					"background-position": t + " bottom"
				})
			}, function () {
				var t = W(this).css("background-position") ? W(this).css("background-position").split(" ")[0] : W(this).css("background-position-x");
				W(this).css({
					"background-position": t + " top"
				})
			}).on(c, function () {
				var t = y.get(0).volume;
				0 < t ? (j = t, t = 0) : t = j;
				var e = W(this).css("background-position") ? W(this).css("background-position").split(" ")[1] : W(this).css("background-position-y");
				M.css({
					"background-position": (0 < t ? "left" : "right") + " " + e
				}), y.get(0).volume = t, q.css({
					height: Math.round(100 * t) + "%"
				})
			}), z.css({
				display: "none",
				position: "absolute",
				left: 4,
				bottom: "100%",
				width: 24,
				height: 80,
				"margin-bottom": Math.floor((d - 32) / 2),
				"background-color": "#000000",
				opacity: .7,
				filter: "alpha(opacity=70)"
			}), R.css({
				display: "block",
				position: "relative",
				width: 10,
				height: 68,
				margin: 7,
				cursor: "pointer",
				"background-color": "#222"
			}), q.css({
				display: "block",
				position: "absolute",
				bottom: 0,
				left: 0,
				width: "100%",
				height: "100%",
				"background-color": "#fcc500"
			}), R.on(r, function (t) {
				var e = 1 - ((s ? t.originalEvent.touches[0] : t).pageY - R.offset().top) / R.height();
				e = 1 < e ? 1 : e < 0 ? 0 : e, q.css({
					height: Math.round(100 * e) + "%"
				}), M.css({
					"background-position": "left " + (0 < e ? "top" : "bottom")
				}), y.get(0).volume = e, R.on(a, function (t) {
					var e = 1 - ((s ? t.originalEvent.touches[0] : t).pageY - R.offset().top) / R.height();
					e = 1 < e ? 1 : e < 0 ? 0 : e, q.css({
						height: Math.round(100 * e) + "%"
					}), M.css({
						"background-position": "left " + (0 < e ? "top" : "bottom")
					}), y.get(0).volume = e
				})
			}).on(l, function () {
				R.off(a)
			})
		}
		var B = function (t) {
			var e = Math.floor(t / 3600),
				i = e < 10 ? "0" + e : e,
				o = Math.floor((t - 3600 * e) / 60),
				n = o < 10 ? "0" + o : o,
				s = Math.floor(t - (3600 * e + 60 * o)),
				r = n + ":" + (s < 10 ? "0" + s : s);
			return 0 < e && (r = i + ":" + r), r
		};
		o && x.hide(), i && k.hide();
		try {
			y.on("play", function () {
				o || x.hide(), i || (_.hide(), C.show())
			}), y.on("pause", function () {
				o || x.show(), i || (k.show(), clearTimeout(u), _.show(), C.hide())
			}), y.on("ended", function () {
				W(window).trigger("html5lightbox.videoended"), o || x.show(), i || (k.show(), clearTimeout(u), _.show(), C.hide())
			}), y.on("timeupdate", function () {
				var t = y.get(0).currentTime;
				if (t) {
					S.text(B(t));
					var e = y.get(0).duration;
					if (e) {
						A.text(B(e));
						var i = $.width(),
							o = Math.round(i * t / e);
						D.css({
							width: o
						}), O.css({
							left: o
						})
					}
				}
			}), y.on("progress", function () {
				if (y.get(0).buffered && 0 < y.get(0).buffered.length && !isNaN(y.get(0).buffered.end(0)) && !isNaN(y.get(0).duration)) {
					var t = $.width();
					I.css({
						width: Math.round(t * y.get(0).buffered.end(0) / y.get(0).duration)
					})
				}
			})
		} catch (t) {}
	}, jQuery(document).ready(function () {
		"undefined" == typeof html5Lightbox && (html5Lightbox = jQuery(".html5lightbox").html5lightbox())
	})
}
$('.navigation-wrapper a[href^="#"]').on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
	  var $anchor = $(this);
	  $('html, body').stop().animate({
			scrollTop: $($anchor.attr('href')).offset().top - 100
		}, 1500,'easeInOutExpo');      
    }
  }), $(document).ready(function () {
		$(".navigation").navigation()
	}),
/*
$('.navigation-wrapper a[href^="#"]').on("click", function (t) {
		t.preventDefault(), $(document).off("scroll"), $("a").each(function () {
			$(this).removeClass(currentClass)
		}), $(this).addClass(currentClass);
		var e = this.hash;
		$target = $(e), $("html, body").stop().animate({
			scrollTop: $target.offset().top + 2
		}, 500, "swing", function () {
			window.location.hash = e, $(document).on("scroll", onScroll)
		})
	}), $(document).ready(function () {
		$(".navigation").navigation()
	}),*/
	
	function () {
		for (var t = document.getElementsByTagName("script"), e = "", i = 0; i < t.length; i++) t[i].src && t[i].src.match(/html5lightbox\.js/i) && (e = t[i].src.substr(0, t[i].src.lastIndexOf("/") + 1));
		var o = !1;
		if ("undefined" == typeof jQuery) o = !0;
		else {
			var n = jQuery.fn.jquery.split(".");
			(n[0] < 1 || 1 == n[0] && n[1] < 6) && (o = !0)
		}
		if (o) {
			var s = document.getElementsByTagName("head")[0],
				r = document.createElement("script");
			r.setAttribute("type", "text/javascript"), r.readyState ? r.onreadystatechange = function () {
				"loaded" != r.readyState && "complete" != r.readyState || (r.onreadystatechange = null, loadHtml5LightBox(e))
			} : r.onload = function () {
				loadHtml5LightBox(e)
			}, r.setAttribute("src", e + "jquery.js"), s.appendChild(r)
		} else loadHtml5LightBox(e)
	}(), $(function () {
		"use strict";
		jQuery("img.svg").each(function () {
			var i = jQuery(this),
				o = i.attr("id"),
				n = i.attr("class"),
				t = i.attr("src");
			jQuery.get(t, function (t) {
				var e = jQuery(t).find("svg");
				void 0 !== o && (e = e.attr("id", o)), void 0 !== n && (e = e.attr("class", n + " replaced-svg")), !(e = e.removeAttr("xmlns:a")).attr("viewBox") && e.attr("height") && e.attr("width") && e.attr("viewBox", "0 0 " + e.attr("height") + " " + e.attr("width")), i.replaceWith(e)
			}, "xml")
		})
	}),
	function (e) {
		"use strict";
		e(".switcher__toggler").on("click", function () {
			e(".switcher").toggleClass("switcher__is-open")
		}), e(".tab-color").on("click", function () {
			e(".switcher-gradient").removeClass("active"), e(".switcher-color").addClass("active"), e(".tab-gradient").removeClass("active"), e(this).addClass("active")
		}), e(".tab-gradient").on("click", function () {
			e(".switcher-color").removeClass("active"), e(".switcher-gradient").addClass("active"), e(".tab-color").removeClass("active"), e(this).addClass("active")
		}), e(".switcher-tab__color button").on("click", function () {
			var t = e(".tab-color-clone").find(".active");
			0 < t.length && e("body").removeClass(t.attr("data-theme")), e("body").addClass(e(this).attr("data-theme")), e(".switcher-tab__color button").removeClass("active"), e(this).addClass("active")
		}), e(".tab-full").on("click", function () {
			e(".switcher-boxed").removeClass("active"), e("body").removeClass("canvas-boxed"), e(".tab-boxed").removeClass("active"), e(this).addClass("active")
		}), e(".tab-boxed").on("click", function () {
			e(".switcher-boxed").addClass("active"), e("body").addClass("canvas-boxed"), e(".tab-full").removeClass("active"), e(this).addClass("active")
		}), e(".switcher-tab__pattern button").on("click", function () {
			var t = e(".switcher-boxed").find(".active");
			0 < t.length && e("body").removeClass(t.attr("data-theme")), e("body").addClass(e(this).attr("data-theme")), e(".switcher-tab__pattern button").removeClass("active"), e(this).addClass("active")
		})
	}(jQuery),
	function (e) {
		"use strict";
		jQuery.fn.vApp = function (t) {
			t.layout && e(this).addClass(t.layout), t.skin && e(this).addClass(t.skin), t.pattern && e(this).addClass(t.pattern)
		}
	}(jQuery),
	function (i) {
		"use strict";
		i(".media__content").each(function () {
			var t = i(this).attr("src");
			i(this).parent().css("background-image", "url(" + t + ")"), i(this).parent().css("background-repeat", "no-repeat"), i(this).hide()
		}), i(".testimonial-one .testimonial").slick({
			vertical: !0,
			verticalSwiping: !0,
			slidesToShow: 2,
			slidesToScroll: 2,
			prevArrow: '<div><button class="prevArrow arrowBtn"><i class="nc-icon nc-tail-left"></i></button></div>',
			nextArrow: '<div><button class="nextArrow arrowBtn"><i class="nc-icon nc-tail-right"></i></button></div>',
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					vertical: !1,
					verticalSwiping: !1
				}
			}]
		}), i(".testimonial-two .testimonial").slick({
			autoplay: !1,
			prevArrow: '<div><button class="prevArrow arrowBtn"><i class="nc-icon nc-tail-left"></i></button></div>',
			nextArrow: '<div><button class="nextArrow arrowBtn"><i class="nc-icon nc-tail-right"></i></button></div>',
			responsive: [{
				breakpoint: 992,
				settings: {
					arrows: !1
				}
			}]
		}), i(".testimonial-three .testimonial").slick({
			autoplay: !0,
			slidesToShow: 2,
			arrows: !1,
			dots: !1,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1
				}
			}]
		}), window.sr = ScrollReveal(), sr.reveal(".reveal, .faq-list, .feature__list", {
			duration: 1e3,
			delay: 0,
			scale: 1,
			opacity: .2,
			easing: "ease-in-out"
		}), i(".prcing-table").ready(function () {
			i(".prcing-table").parent().parent().css("align-items", "center")
		}), i(".tab__annual").click(function () {
			i(".pro .value").text("219"), i(".company .value").text("449")
		}), i(".tab__monthly").click(function () {
			i(".pro .value").text("19"), i(".company .value").text("39")
		}), i(document).on("click", ".pricing-tab-list__item", function () {
			i(".current").removeClass("current"), i(this).addClass("current")
		}), i(window).on("scroll", function () {
			250 <= i(window).scrollTop() ? i(".faq-sidebar").addClass("faq-sidebar--is-fixed") : i(".faq-sidebar").removeClass("faq-sidebar--is-fixed")
		}), i(".faq-sidebar-list__item a").on("click", function () {
			i(".faq-sidebar-list__item a").removeClass("active"), i(this).addClass("active")
		}), i(".faq-sidebar a").bind("click", function (t) {
			var e = i(this);
			i(".header").outerHeight(), i("html, body").stop().animate({
				scrollTop: i(e.attr("href")).offset().top - "150" + "px"
			}, 1200, "easeInOutExpo"), t.preventDefault()
		}), i.extend(i.easing, {
			easeInOutExpo: function (t, e, i, o, n) {
				return 0 == e ? i : e == n ? i + o : (e /= n / 2) < 1 ? o / 2 * Math.pow(2, 10 * (e - 1)) + i : o / 2 * (2 - Math.pow(2, -10 * --e)) + i
			}
		})
	}(jQuery);


